package cs444.codeGen;

import cs444.utils.Code;
import cs444.typeLinker.*;
import cs444.ast.*;
import java.util.*;
import java.io.*;

public class CodeGenerator
{
	protected Collection<JoosClass> m_jClassCollection;
	protected int m_numTypes;
	
	public CodeGenerator( CompilationEnvironment env )
	{
		m_jClassCollection = env.getClasses();
		m_numTypes = env.getClasses().size() + env.getInterfaces().size();
	}
	
	public void genCode() throws java.io.IOException
	{
		Appendable out;
		for( JoosClass jClass : m_jClassCollection ) {
			out = new StringBuffer();
			jClass.genCode( out, m_numTypes );
			try {
				// Create file 
				FileWriter fstream = new FileWriter( "output/" + jClass.fullName() + ".s");
				BufferedWriter file = new BufferedWriter(fstream);
				file.write( out.toString() );
				//Close the output stream
				file.close();
			} catch( IOException e ) {//Catch exception if any
				System.err.println("Error: " + e.getMessage());
			}
		}
		try {
			FileWriter fwriter = new FileWriter( "output/_core.s" );
			Code.genCode( fwriter );
			fwriter.close();
		} catch( IOException e ) {
			System.err.println( "Error: " + e.getMessage() );
		}
	}
}
