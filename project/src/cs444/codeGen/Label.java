package cs444.codeGen;

public class Label {
	private static int m_labelIndex;
	
	private Label(){
		m_labelIndex = 0;
	}
	
	public static int getLabelIndex(){
		int temp = m_labelIndex;
		m_labelIndex++;
		return temp;
	}
	
}
