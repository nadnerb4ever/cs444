package cs444.staticAnalyzer;

public class StatementUnreachableException  extends StaticAnalyzerException {

	private static final long serialVersionUID = 1L;

	public StatementUnreachableException()
	{
		super( "Statement not reachable." );
	}
	
	public StatementUnreachableException( String msg )
	{
		super( msg );
	}
	
	public StatementUnreachableException(String msg, Throwable cause)
	{
		super( msg, cause );
	}

	public StatementUnreachableException(Throwable cause)
	{
		super( cause );
	}
}
