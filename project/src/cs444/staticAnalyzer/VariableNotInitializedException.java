package cs444.staticAnalyzer;

public class VariableNotInitializedException extends StaticAnalyzerException {
	private static final long serialVersionUID = 1L;

	public VariableNotInitializedException()
	{
		super( "Local variable not initialized and used." );
	}
	
	public VariableNotInitializedException( String msg )
	{
		super( msg );
	}
	
	public VariableNotInitializedException(String msg, Throwable cause)
	{
		super( msg, cause );
	}

	public VariableNotInitializedException(Throwable cause)
	{
		super( cause );
	}
}