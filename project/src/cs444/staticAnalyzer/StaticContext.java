package cs444.staticAnalyzer;

import java.util.*;
import cs444.typeLinker.*;


public class StaticContext
{
	private boolean m_reachable;
	private Set<String> m_initialized;
	// Marks whether this context is for a block in a method or for an initializer
	private boolean m_forMethod;
	
	public StaticContext( boolean forMethod )
	{
		m_reachable = true;
		m_initialized = new TreeSet<String>();
		m_forMethod = forMethod;
	}
	
	public void reachable( boolean reachable )
	{
		m_reachable = reachable;
	}
	public boolean reachable()
	{
		return m_reachable;
	}
	public void init( String varname )
	{
		m_initialized.add( varname );
	}
	public void init( JoosVariable var )
	{
		init( var.toString() );
	}
	public boolean isInit( String varname )
	{
		return m_initialized.contains( varname );
	}
	public boolean isInit( JoosVariable var )
	{
		if( var.getOrigin().asJoosMethod() != null ) {
			// This is a method parameter (which are always initialized)
			return true;
		} else if( var.getOrigin().asJoosClass() != null && m_forMethod ) {
			// Then we are in a method block and this is a class variable (assume initialized)
			return true;
		}
		return isInit( var.toString() );
	}
	public StaticContext copy()
	{
		StaticContext ret = new StaticContext( m_forMethod );
		copyInto( ret );
		return ret;
	}
	public void copyInto( StaticContext other )
	{
		other.reachable( reachable() );
		other.m_initialized.addAll( m_initialized );
	}
	public StaticContext intersect( StaticContext other )
	{
		StaticContext ret = new StaticContext( m_forMethod );
		ret.reachable( reachable() || other.reachable() );
		//System.err.println( "computing intersection of Static Contexts:\n" + toString() + "\n" + other.toString() );
		for( String var : m_initialized ) {
			if( other.isInit( var ) ) {
				ret.init( var );
			}
		}
		return ret;
	}
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append( "<StaticContext:\n\treachable = " + reachable() + ",\n\tinitialized = {" );
		String sep = "";
		for( String s : m_initialized ) {
			builder.append( sep + "\n\t\t" + s );
			sep = ",";
		}
		builder.append( "\n\t} >" );
		return builder.toString();
	}
}




