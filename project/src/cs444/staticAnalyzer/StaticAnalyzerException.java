package cs444.staticAnalyzer;

public class StaticAnalyzerException extends Exception {

	private static final long serialVersionUID = 1L;

	public StaticAnalyzerException()
	{
		super( "Local variable not initialized and used." );
	}
	
	public StaticAnalyzerException( String msg )
	{
		super( msg );
	}
	
	public StaticAnalyzerException(String msg, Throwable cause)
	{
		super( msg, cause );
	}

	public StaticAnalyzerException(Throwable cause)
	{
		super( cause );
	}
}
