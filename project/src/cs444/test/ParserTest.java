package cs444.test;

import cs444.parser.*;
import cs444.shared.Kind;
import cs444.shared.Token;

import java.util.*;
import java.io.*;


public class ParserTest
{
	public static void main( String[] args )
	{
		if( args.length != 2 ) {
			System.err.println( "Invalid number of arguments: expected 2, got " + args.length );
			System.exit( 1 );
		}
		Parser parser = getParser( args[0] );
		if( parser == null ) {
			System.err.println( "Could not create parser" );
			System.exit( 2 );
		}
		Collection<Token> tokens = getTokens( args[1] );
		if( tokens == null ) {
			System.err.println( "Could not create token stream" );
			System.exit( 3 );
		}
		for( Token token : tokens ) {
			parser.enqueue( Symbol.wrap( token ) );
		}
		parser.markEnd();
		try {
			parser.run();
		} catch( ParserException pe ) {
			System.err.println( pe.toString() );
			System.exit( 4 );
		}
		if( parser.isComplete() ) {
			System.out.println( "Parser completed successfully" );
			System.out.println( parser.getParseTree().toString() );
		} else {
			System.out.println( "Parser did not complete successfully" );
		}
	}
	
	public static Parser getParser( String fileName )
	{
		return LR1Parser.create( fileName );
	}
	public static Collection<Token> getTokens( String fileName )
	{
		Collection<Token> ret = null;
		try {
			InputStream in = new FileInputStream( fileName );
			ret = getTokens( in );
			in.close();
		} catch( FileNotFoundException fnfe ) {
			System.err.println( "Error: Could not find " + fileName );
		} catch( IOException ioe ) {
			// Do nothing
		}
		return ret;
	}
	public static Collection<Token> getTokens( InputStream input )
	{
		ArrayList<Token> ret = new ArrayList<Token>();
		Scanner in = new Scanner( input );
		while( in.hasNextLine() ) {
			String line = in.nextLine().trim();
			if( line.equals( "" ) ) {
				continue;
			}
			int idx = line.indexOf( ' ' );
			String first = line.substring( 0, idx );
			String rest = line.substring( idx + 1 );
			Kind kind = Kind.valueOf( first );
			Token token = new Token( kind, rest );
			ret.add( token );
		}
		return ret;
	}
}


