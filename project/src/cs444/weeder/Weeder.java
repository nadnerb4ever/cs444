package cs444.weeder;

import cs444.parser.*;

import java.math.BigInteger;
import java.util.*;

public class Weeder
{
	private String m_error = null;
	
	private String m_filename;
	private String m_baseFileName;
	private Set<String> m_classModifiers = null;
	private Set<String> m_methodModifiers = null;
	private Set<String> m_fieldModifiers = null;
	private boolean m_isInterface = false;
	
	public Weeder( String filename )
	{
		m_filename = filename;
		int sepIdx = filename.lastIndexOf( '/' );
		m_baseFileName = filename.substring( sepIdx + 1, filename.length() - 5 );
	}

	public String getError()
	{
		return m_error;
	}
	public boolean check( ParseTree tree )
	{
		m_error = null;
		return check( tree.getRoot() );
	}
	
	public boolean checkChildren( ParseTreeNode node )
	{
		if( node.isLeaf() ) {
			return true;
		}
		for( ParseTreeNode child : node.getChildren() ) {
			if( !check( child ) ) {
				return false;
			}
		}
		return true;
	}


	
	private static final String[] UNARY_MINUS = { "MINUS", "Unary" };
	
	public boolean check( ParseTreeNode node )
	{
		ParseTreeNode[] children = node.getChildren();
		if( node.getType().equals( "ClassDeclaration" ) ) {
			String className = getBaseValue( children[2] );
			m_classModifiers = getModifiers( children[0] );
			if( m_classModifiers == null || !checkClassModifiers( m_classModifiers, className ) || !check( children[5] ) ) {
				return false;
			}
			m_classModifiers = null;
			return true;
		} else if( node.getType().equals( "InterfaceDeclaration" ) ) {
			String className = getBaseValue( children[2] );
			m_classModifiers = getModifiers( children[0] );
			m_isInterface = true;
			if( m_classModifiers == null || !checkClassModifiers( m_classModifiers, className ) || !check( children[4] ) ) {
				return false;
			}
			m_isInterface = false;
			m_classModifiers = null;
			return true;
		} else if( node.getType().equals( "AbstractMethodDeclaration" ) ) {
			m_methodModifiers = getModifiers( children[0] );
			if( m_methodModifiers == null || !checkMethodModifiers( m_methodModifiers, true ) ) {
				return false;
			}
			m_methodModifiers = null;
		} else if( node.getType().equals( "ConcreteMethodDeclaration" ) ) {
			m_methodModifiers = getModifiers( children[0] );
			if( m_methodModifiers == null || !checkMethodModifiers( m_methodModifiers, false ) ) {
				return false;
			}
			m_methodModifiers = null;
		} else if( node.getType().equals( "FieldDeclaration" ) ) {
			Set<String> modifiers = getModifiers( children[0] );
			if( modifiers == null || !checkFieldModifiers( modifiers, children[2] ) ) {
				return false;
			}
		} else if( node.getType().equals( "DECIMAL" ) ) {
			return checkIntRange( node.getValue(), false);
		} else if( node.matches( "Unary", UNARY_MINUS ) ) {
			return checkNegativeUnary( children[1] );
		} else if( node.getType().equals( "CastExpression" ) ) {
			if( !checkIsType( children[1] ) ) {
				return false;
			}
		}
		return checkChildren( node );
	}
	
	public boolean checkNegativeUnary( ParseTreeNode node )
	{
		ParseTreeNode[] children = node.getChildren();
		if( node.getType().equals( "DECIMAL" ) ) {
			return checkIntRange( node.getValue(), true );
		} else if( node.isLeaf() ) {
			return check( node );
		} else if( children.length == 1 ) {
			return checkNegativeUnary( children[0] );
		}
		return checkChildren( node );
	}
	public boolean checkIsType( ParseTreeNode node )
	{
		ParseTreeNode[] children = node.getChildren();
		if( node.getType().equals( "BasicType" ) || node.getType().equals( "QualifiedID" ) ) {
			return true;
		} else if( node.isLeaf() ) {
			m_error = node.getValue() + " is not a type";
			return false;
		} else if( children.length == 1 ) {
			return checkIsType( children[0] );
		}
		m_error = "An expression is not a type";
		return false;
	}

	
	// Methods for checking specific results
	public Set<String> getModifiers( ParseTreeNode node )
	{
		Set<String> ret = new TreeSet<String>();
		ParseTreeNode[] children = null;
		while( (children = node.getChildren()).length >= 1 ) {
			String type = getBaseType( children[0] );
			if( ret.contains( type ) ) {
				m_error = "Modifier '" + type + "' has been declared twice";
				return null;
			}
			ret.add( type );
			if(children.length == 1){
			    break;
			}
			node = children[1];
		}
		return ret;
	}
	
	public String getBaseType( ParseTreeNode node )
	{
		while( !node.isLeaf() ) {
			node = node.getChildren()[0];
		}
		return node.getType();
	}
	
	public String getBaseValue( ParseTreeNode node )
	{
		while( !node.isLeaf() ) {
			node = node.getChildren()[0];
		}
		return node.getValue();
	}
	
	public boolean checkClassModifiers( Set<String> modifiers, String className )
	{
		if( !modifiers.contains( "PUBLIC" ) ) {
			m_error = "Classes cannot be package private";
		} else if( !className.equals( m_baseFileName ) ) {
			m_error = "public class: '" + className + "' does not match file name";
		} else if( modifiers.contains( "ABSTRACT" ) && modifiers.contains( "FINAL" ) ) {
			m_error = "Class cannot be declared abstract and final";
		} else if( m_isInterface && (modifiers.contains( "ABSTRACT" ) || modifiers.contains( "FINAL" )) ) {
			m_error = "An interface cannot be declared abstract or final";
		}
		return m_error == null;
	}
	
	private static final String[] ACCESS_MODIFIERS = {"PUBLIC", "PRIVATE", "PROTECTED"};
	public boolean checkMethodModifiers( Set<String> modifiers, boolean abstractMethod )
	{
		int accessModifierCount = 0;
		for( String access : ACCESS_MODIFIERS ) {
			if( modifiers.contains( access ) ) {
				accessModifierCount += 1;
			}
		}
		if( accessModifierCount == 0 ) {
			m_error = "Package private method not allowed";
		} else if( accessModifierCount > 1 ) {
			m_error = "Methods can not have more than one access modifier";
		} else if( modifiers.contains( "ABSTRACT" ) ) {
			if( !abstractMethod ) {
				m_error = "An abstract method cannot have an implementation";
			} else if( !m_classModifiers.contains( "ABSTRACT" ) && !m_isInterface ) {
				m_error = "A class must be abstract in order to have an abstract method";
			} else if( modifiers.contains( "FINAL" ) ) {
				m_error = "An abstract method cannot be final";
			} else if( modifiers.contains( "STATIC" ) ) {
				m_error = "An abstract method cannot be static";
			}
		} else if( abstractMethod && !(m_isInterface || modifiers.contains( "NATIVE" )) ) {
			m_error = "A method must be declared abstract or native if it does not have an implementation";
		} else if( modifiers.contains( "NATIVE" ) && !modifiers.contains( "STATIC" ) ) {
			m_error = "A native method must be static";
		} else if( modifiers.contains( "STATIC" ) && modifiers.contains( "FINAL" ) ) {
			m_error = "A static method can not be final";
		}
		return m_error == null;
	}
	public boolean checkFieldModifiers( Set<String> modifiers, ParseTreeNode variableDeclarator )
	{
		ParseTreeNode[] children = variableDeclarator.getChildren();
		int accessModifierCount = 0;
		for( String access : ACCESS_MODIFIERS ) {
			if( modifiers.contains( access ) ) {
				accessModifierCount += 1;
			}
		}
		if( modifiers.contains( "NATIVE" ) ) {
			m_error = "A field can not be declared native";
		} else if( accessModifierCount == 0 ) {
			m_error = "Package private field not allowed";
		} else if( accessModifierCount > 1 ) {
			m_error = "Fields can not have more than one access modifier";
		} else if( modifiers.contains( "FINAL" ) ) {
			if( children.length != 3 ) {
				m_error = "A final field must have a variable assignment";
			}
		}
		return m_error == null;
	}
	    
	
	/**
	 * The largest decimal literal of type int is 2147483648 ( 2^31 ). 
	 * All decimal literals from 0 to 2147483647 may appear anywhere an int literal may appear,
	 * but the literal 2147483648 may appear only as the operand of the unary negation operator -
	 */
	public boolean checkIntRange(String num, boolean isNegative)
	{
		if ( new BigInteger( num, 10 ).bitLength() <= 31 
			|| ( num.equals("2147483648") && isNegative) ) {
			return true;
		}
		m_error = "Integer out of range: "+ num;
		return false;
	}
}




