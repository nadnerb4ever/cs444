package cs444.lex.rule_table;

import java.util.*;

public interface NFARuleTable
{
	public Collection<State> getNextState( State s, Character c );
	public Collection<State> getClosure( State s );
	// TODO remove this once we no longer have a broken version of getClosure
	public Collection<State> getRealClosure( State s );
	public Collection<Character> getTransitions( State s );
	public State getStart();
	public String toString();
};

