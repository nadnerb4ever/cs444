package cs444.lex.rule_table;

import cs444.shared.*;
import java.util.*;

public interface RuleTable
{
	public Collection<State> getNextState( State s, char c );
	public Collection<State> getClosure( State s );
	public State getStart();
	public State getEnd();
};

