package cs444.lex.rule_table;

import cs444.shared.*;
import java.util.*;
import cs444.parser.*;
import cs444.lex.*;

public class NFABuilder
{
	static String[] m_or = new String[] { "regex", "OR", "regex2" };
	static String[] m_and = new String[] { "regex", "regex2" };
	static String[] m_regex2 = new String[] { "regex2" };
	static String[] m_paren = new String[] { "LPAREN", "regex", "RPAREN" };
	static String[] m_range = new String[] { "LBRACE", "SYMBOL", "SYMBOL", "RBRACE" };
	static String[] m_symbol = new String[] { "SYMBOL" };
	static String[] m_star = new String[] { "regex2", "STAR" };
	static String[] m_epsilon = new String[] { "EPSILON" };
	
	ParseTree m_tree;
	int counter = 0;
	String m_prefix;
	
	public NFABuilder( ParseTree tree, String kind )
	{
		m_tree = tree;
		m_prefix = kind;
	}
	
	public NFARuleTableImpl build()
	{
		return build( m_tree.getRoot() );
	}
	
	private NFARuleTableImpl build( ParseTreeNode current )
	{
		NFARuleTableImpl ret = null;
		if( current.isLeaf() ){
			ret = new NFARuleTableImpl();
			String value = current.getValue();
			State start = getUniqueState( false, ret );
			State end = getUniqueState( true, ret );
			ret.setStart( start );
			ret.addTransition( start, end, Character.valueOf(value.charAt(0) ));
			return ret;
		}
		ParseTreeNode[] nodes = current.getChildren();
		
		if( current.matches( "regex2", m_range ) ) {
			char lo = nodes[1].getValue().charAt( 0 );
			char hi = nodes[2].getValue().charAt( 0 );
			ret = rangeNFA( lo, hi );
		} else if( current.matches( "regex", m_or ) ) {
			NFARuleTableImpl regex = build( nodes[0] );
			NFARuleTableImpl regex2 = build( nodes[2] );
			regex = NFARuleTableImpl.orMerge( regex, regex2 );
			ret = regex;
		} else if( current.matches( "regex", m_and ) ) {
			NFARuleTableImpl regex = build( nodes[0] );
			NFARuleTableImpl regex2 = build( nodes[1] );
			//System.err.println( "tableA:\n" + regex );
			//System.err.println( "tableB:\n" + regex2 );
			regex = NFARuleTableImpl.andMerge( regex, regex2 );
			//System.err.println( "tableC:\n" + regex );
			ret = regex;
		} else if( current.matches( "regex", m_regex2 ) ) {
			ret = build( nodes[0] );
		} else if( current.matches( "regex2", m_paren ) ) {
			ret = build( nodes[1] );
		} else if( current.matches( "regex2", m_symbol ) ) {
			ret = build( nodes[0] );
		} else if( current.matches( "regex2", m_star ) ) {
			NFARuleTableImpl regex = build( nodes[0] );
			regex = NFARuleTableImpl.star( regex );
			ret = regex;
		} else if( current.matches( "regex2", m_epsilon ) ) {
			ret = new NFARuleTableImpl();			
			State start = getUniqueState( false, ret );
			State end = getUniqueState( true, ret );
			ret.setStart( start );
			ret.addEpsilon( start, end );
		} else if( current.getType() == "SYMBOL" ){
			ret = new NFARuleTableImpl();
			State start = getUniqueState( false, ret );
			State end = getUniqueState( true, ret );
			ret.setStart( start );
			ret.addTransition( start, end, Character.valueOf( current.getValue().charAt(0) ) );
		}
		return ret;
	}
	
	
	private State getUniqueState( boolean accepting, NFARuleTableImpl dst )
	{
		State ret = getUniqueState( accepting );
		dst.addState( ret );
		return ret;
	}
	private State getUniqueState( boolean accepting )
	{
		State ret = new State( m_prefix + ":" + String.valueOf( counter++ ), accepting );
		if( accepting ) {
			ret.setKind( m_prefix );
		}
		return ret;
	}
	
	private NFARuleTableImpl rangeNFA( char lo, char hi )
	{
		NFARuleTableImpl nfa = new NFARuleTableImpl();
		State start = getUniqueState( false, nfa );
		State end = getUniqueState( true, nfa );
		nfa.setStart( start );
		while( lo <= hi ){
			nfa.addTransition( start, end, Character.valueOf( lo ) );
			lo++;
		}
		return nfa;
	}
	
	public static void main(String args[])
	{
		RegexParser regex = new RegexParser();
		ParseTree tree = regex.parse( args[0] );
		NFABuilder nfa = new NFABuilder( tree, "kind" );
	}
}

