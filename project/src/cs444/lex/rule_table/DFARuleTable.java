package cs444.lex.rule_table;

import java.util.*;

public interface DFARuleTable
{
	public Collection<State> getStates();
	public State getNextState( State s, char c );
	public Collection<Character> getTransitions( State s );
	public State getStart();
};

