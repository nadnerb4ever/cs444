package cs444.lex.rule_table;

import cs444.shared.*;
import java.util.*;
import cs444.lex.*;
import cs444.parser.*;

public class DFABuilder
{
	protected NFARuleTable nfa = null;
	protected Map<String, Integer> m_priorityMap;
	
	public void setNFARuleTable( NFARuleTable nfaRT, Map<String, Integer> priorityMap )
	{
		nfa = nfaRT;
	  m_priorityMap = priorityMap;
	}
	
	public DFARuleTable create()
	{
		DFARuleTableImpl dfa = new DFARuleTableImpl();
		
		Deque< TreeSet<State> > worklist = new LinkedList< TreeSet<State> >();
		
		TreeSet<State> start = new TreeSet<State>();
		Collection<State> closure = nfa.getRealClosure( nfa.getStart() );
		
		if( closure != null ){
			start.addAll( closure );
		}else{
			start.add( nfa.getStart() );
		}
		dfa.addState( getUniqueDFAState( start ) );
		worklist.add( start );
		
		while( !worklist.isEmpty() ) {
			TreeSet<State> currentSet = worklist.pollFirst();
			Set<Character> transitions = getTransitions( currentSet );
			State currentState = getUniqueDFAState( currentSet );
			
			for( Character c : transitions ) {
				TreeSet<State> newSet = getStates( currentSet, c );
				State dfaState = getUniqueDFAState( newSet );
				
				if( ! dfa.getStates().contains( dfaState ) ) {
					dfa.addState( dfaState );
					worklist.addLast( newSet );
				}
				
				dfa.addTransition( currentState, c, dfaState);
			}
		}
		return dfa;
	}
	
	private Set<Character> getTransitions(Set<State> states)
	{
		Set<Character> transitions = new HashSet<Character>();
		for( State state : states ) {
			Collection<Character> s = nfa.getTransitions( state );
			if( s!=null ){
				transitions.addAll( s );
			}
		}
		return transitions;
	}
	
	private TreeSet<State> getStates(Set<State> src, Character c)
	{
		TreeSet<State> dst = new TreeSet<State>();
		for( State state : src ) {
			Collection<State> states = nfa.getNextState( state, c );
			if( states != null ){
				dst.addAll( states );
			}
		}
		
		TreeSet<State> closure = new TreeSet<State>();
		for( State state : dst ) {
			Collection<State> eps = nfa.getRealClosure( state );
			if( eps != null ) {
				closure.addAll( eps );
			}
		}
		//dst.addAll( closure );
		return closure;
	}
	private State getUniqueDFAState( TreeSet<State> set)
	{
		StringBuilder name = new StringBuilder();
		boolean accepting = false;
		String kind = null;
		
		for( State state : set.descendingSet() ) {
			name.append( "$" ).append( state.name() );
			if( state.isAccepting() == true ) {
				if( kind == null || ( m_priorityMap.get( state.getKind() ) < m_priorityMap.get( kind ) ) ){
					kind = state.getKind();
				}
				accepting = true;
			}
		}
		State ret = new State( name.toString(), accepting );
		if( ret.isAccepting() ){
			ret.setKind( kind );
		}
		return ret;
	}
	
	public static void main( String[]args )
	{
		RegexParser regex = new RegexParser();
		ParseTree tree = regex.parse( args[0] );
		NFABuilder nfa = new NFABuilder( tree, "test" );
		
		Map<String, Integer> priorityMap = new TreeMap<String, Integer>();
		priorityMap.put( "test" , new Integer(1) );
		
		DFABuilder dfa = new DFABuilder();
		NFARuleTable t = nfa.build();
		dfa.setNFARuleTable( t, priorityMap );
		System.out.println( "RESULT" );
		System.out.println( dfa.create().toString() );
	}
}

