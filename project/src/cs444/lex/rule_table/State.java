package cs444.lex.rule_table;

import cs444.shared.*;
import java.util.*;

public class State implements Comparable<State>
{
	protected String name;
	protected boolean accepting;
	protected String kind;
	public State( String n, boolean a)
	{
		this.name = n;
		this.accepting = a;
	}
	
	public int compareTo( State state )
	{
		return name.compareTo( state.name );
	}
	
	public boolean equals( Object other )
	{
		if( other instanceof State ){
			State state = (State) other;
			return name.equals( state.name );
		}else{
			return false;
		}
	}
	
	public int hashCode()
	{
		return name.hashCode();
	}
	
	public boolean isAccepting()
	{
		return accepting;
	}
	
	public void notAccepting(){
		accepting = false;
	}
	
	public void accepting(){
		accepting = true;
	}
	
	public String name()
	{
		return name;
	}
	
	public void setKind( String k )
	{
		kind = k;
	}
	
	public String getKind()
	{
		return kind;
	}
	
	public String toString()
	{
		return name()  ;
	}
};

