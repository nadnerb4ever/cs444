package cs444.lex.rule_table;

import cs444.shared.*;
import java.util.*;

import java.io.*;
public class DFARuleTableImpl implements DFARuleTable
{
	State start = null;
	private Set<State> states;
	private HashMap<State, TreeMap<Character, State>> stateMap;
	
	public DFARuleTableImpl()
	{
		stateMap = new HashMap<State, TreeMap<Character, State>>();
		states = new TreeSet<State>();
	}
	
	private void setStart( State state )
	{
		start = state;
	}
	
	public Collection<State> getStates()
	{
		return states;
	}
	public State getNextState( State s, char c )
	{
		TreeMap<Character, State> sMap = stateMap.get( s );
		if( sMap == null ) {
			return null;
		}
		return sMap.get( c );
	}
	public Collection<Character> getTransitions( State s )
	{
		TreeMap<Character, State> sMap = stateMap.get( s );
		Set<Character> transitions = sMap.descendingKeySet();
		return transitions;
	}
	public State getStart()
	{
		return start;
	}
	
	public void addTransition( State start, Character c, State end )
	{
		// add epsilon transition
		TreeMap<Character, State> startMap = stateMap.get( start );
		if( startMap == null ) {
			startMap = new TreeMap<Character, State>();
			stateMap.put( start, startMap );
		}Set<State> states = stateMap.keySet();
		startMap.put( c, end );
	}
	
	public void addState ( State s)
	{
		if( states.isEmpty() ) {
			setStart( s );
		}
		states.add( s );
	}
	
	public String toString()
	{
		StringBuilder ret = new StringBuilder();
		Set<State> states = stateMap.keySet();
		Iterator<State> statesIt = states.iterator();
		Set<State> allStates = new TreeSet<State>();
		ret.append( "Start State:\n" + start.name() + "\n");
		
		ret.append( "Transitions:\n" );
		
		while( statesIt.hasNext() ){
			State s = statesIt.next();
				
			TreeMap<Character, State> transitions = stateMap.get( s );
			if( transitions != null ){
				Iterator<Character> transitionsIt = transitions.keySet().iterator();
				allStates.add( s );
				while( transitionsIt.hasNext() ) {
					Character c = transitionsIt.next();
					State next = transitions.get( c );
					ret.append(s.name() + " " + (int) c.charValue() + " " + next.name() + "\n" );
					allStates.add( next );
				}
			}
		}
		ret.append( "States:\n" );
		for( State state: allStates ){
			ret.append( state.toString() );
			if( state.isAccepting() ){
				ret.append(" " + state.getKind() );
			}
			ret.append( "\n" );
		}
		return ret.toString();
	}
	
	public void updateAccepting( HashMap<State, String> acceptingMap )
	{
		Set<State> states = stateMap.keySet();
		Iterator<State> statesIt = states.iterator();
		
		
		while( statesIt.hasNext() ){
			State s = statesIt.next();
				
			TreeMap<Character, State> transitions = stateMap.get( s );
			if( transitions != null ){
				Iterator<Character> transitionsIt = transitions.keySet().iterator();
				if( acceptingMap.containsKey( s ) ){
					s.accepting();
					s.setKind( acceptingMap.get(s) );
				}
				while( transitionsIt.hasNext() ) {
					Character c = transitionsIt.next();
					State next = transitions.get( c );
					if( acceptingMap.containsKey( next ) ){
						next.accepting();
						next.setKind( acceptingMap.get(next) );
					}
				}
			}
		}
	}
	
	public static DFARuleTableImpl createFromDFAFile( String fileName )
	{
		DFARuleTableImpl dfa = new DFARuleTableImpl();
		FileInputStream fReader;
		HashMap<State, String> acceptingMap = new HashMap<State, String>();
  	    try{
			fReader = new FileInputStream( fileName );
		} catch ( FileNotFoundException e ){
		    System.err.println( "Can't find a file: " + fileName );
			return null;
		}
		Scanner scan = new Scanner( fReader );
		
		while( scan.hasNextLine() ){
			String nextLine = scan.nextLine().trim();
			if( nextLine.equals( "Start State:" ) ){
				dfa.setStart( new State( scan.nextLine(), false ) );
			}else if( nextLine.equals( "Transitions:" ) ){
				while( scan.hasNextLine() ){
					nextLine = scan.nextLine().trim();
					if( nextLine.equals( "States:" ) ){
						break;
					}else{
						String[] parts = nextLine.split( " ", 3 );
						dfa.addTransition( new State(parts[0], false), (char) Integer.parseInt(parts[1]), new State(parts[2], false) );
					}
				}
				while( scan.hasNextLine() ){
					nextLine = scan.nextLine().trim();
					String[] split = nextLine.split( " ", 2 );
					if( split.length == 2 ){
						acceptingMap.put( new State( split[0] , true), split[1] );
					}
				}
			}
		}
		dfa.updateAccepting( acceptingMap  );
		return dfa;
	}
};

