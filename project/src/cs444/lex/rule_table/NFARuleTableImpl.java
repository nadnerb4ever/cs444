package cs444.lex.rule_table;

import cs444.shared.*;
import java.util.*;



public class NFARuleTableImpl implements NFARuleTable
{
	private HashSet< State > states = new HashSet< State >();
	private HashMap< State, TreeMap<Character, Set<State>> > stateMap = new HashMap< State, TreeMap<Character, Set<State>> >();
	private HashMap< State, Set<State> > epsilon = new HashMap< State, Set<State> >();
	private HashMap< State, Set<State> > closures = null;
	private	State start;
	
	public String toString()
	{
		StringBuilder ret = new StringBuilder();
		Set<State> states = stateMap.keySet();
		Iterator<State> statesIt = states.iterator();
		while( statesIt.hasNext() ){
			State s = statesIt.next();
			ret.append( s.name()+ " " + s.isAccepting()+ " " + s.getKind() + "\n" );
				
			TreeMap<Character, Set<State>> transitions = stateMap.get( s );
			if( transitions != null ){
				Iterator<Character> transitionsIt = transitions.keySet().iterator();
				while( transitionsIt.hasNext() ) {
					Character c = transitionsIt.next();
					Set<State> tStates = transitions.get( c );
					Iterator<State> tStatesIt = tStates.iterator();
					while( tStatesIt.hasNext() ){
						State n = tStatesIt.next();
						ret.append( c.toString() + " " + n.name() + "  " + n.isAccepting() + "\n" );
					}
				}
			}
		}
		ret.append( "epsilon: \n");
		Iterator<State> epsIt = epsilon.keySet().iterator();
		while( epsIt.hasNext() ){
			State e = epsIt.next();
			Iterator<State> transIt = epsilon.get( e ).iterator();
			while( transIt.hasNext() ){
				ret.append( e.name() + " " + transIt.next().name() + "\n" );
			}
		}
		return ret.toString();
	}
	
	public Set<State> getNextState( State s, Character c )
	{
		TreeMap<Character, Set<State>> sMap = stateMap.get( s );
		if( sMap == null ) {
			return null;
		}
		return sMap.get( c );
	}
	
	protected void addState( State state )
	{
		states.add( state );
	}
	protected void addStates( Set<State> states )
	{
		this.states.addAll( states );
	}
	public Set<State> getStates()
	{
		return states;
	}
	
	protected void addTransition( State start, State end, Character c )
	{
		assert( states.contains( start ) );
		assert( states.contains( end ) );
		
		if( stateMap == null ){
			stateMap = new HashMap< State, TreeMap<Character, Set<State>> >();
		}
		TreeMap<Character, Set<State>> startMap = stateMap.get( start );
		if( startMap == null ) {
			startMap = new TreeMap<Character, Set<State>>();
			stateMap.put( start, startMap );
		}
		Set<State> endSet = startMap.get( c );
		if( endSet == null ){
			endSet = new TreeSet<State>();
			startMap.put( c, endSet );
		}
		addSimpleClosure( start );
		addSimpleClosure( end );
		
		endSet.add( end );
	}
	
	public HashMap< State, TreeMap<Character, Set<State>> > getStateMap()
	{
		return stateMap;
	}
	
	protected void addToStateMap( HashMap< State, TreeMap<Character, Set<State>> > map )
	{
		stateMap.putAll( map );
	}
	
	public State getStart()
	{
		return start;
	}
	public void setStart( State s)
	{
		assert( states.contains( s ) );
		start = s;
	}
	
	/**
	 * TODO: Deprecated; phase this out in favour of getEpsilon.
	 * Then getClosure can be ued for the actual full closure of the state
	 */
	public Set<State> getClosure( State s )
	{
		return getEpsilon( s );
	}
	/**
	 * TODO: Rename this once the broken getClosure method has been phased out
	 */
	public Set<State> getRealClosure( State state )
	{
		generateClosures();
		return closures.get( state );
	}
	public Set<State> getEpsilon( State state )
	{
		return epsilon.get( state );
	}
	/**
	 * TODO: Phase this out in favour of addEpsilon
	 */
	public void addClosure( State s, Set<State> states )
	{
		addEpsilon( s, states );
	}
	public void addEpsilon( State s, Set<State> states )
	{
		for( State dst : states ) {
			addEpsilon( s, dst );
		}
	}
	public void addEpsilon( State src, State dst )
	{
		assert( states.contains( src ) );
		assert( states.contains( dst ) );
		
		// Remove our closures that we computed
		closures = null;
		
		Set<State> dsts = epsilon.get( src );
		if( dsts == null ) {
			dsts = new TreeSet<State>();
			epsilon.put( src, dsts );
		}
		dsts.add( dst );
	}
	
	public void generateClosures()
	{
		if( closures != null ) {
			return;
		}
		closures = new HashMap< State, Set<State> >();
		for( State state : epsilon.keySet() ) {
			generateClosure( state );
		}
	}
	private void generateClosure( State state )
	{
		Set<State> closure = new TreeSet<State>();
		NavigableSet<State> worklist = new TreeSet<State>();
		closure.add( state );
		worklist.add( state );
		while( worklist.size() > 0 ) {
			State current = worklist.pollFirst();
			Set<State> epsilon = getEpsilon( current );
			if( epsilon == null ) {
				continue;
			}
			for( State dst : epsilon ) {
				if( !closure.contains( dst ) ) {
					closure.add( dst );
					worklist.add( dst );
				}
			}
		}
		closures.put( state, closure );
	}
	
	private void addAllClosure( HashMap< State, Set<State> > map )
	{
		epsilon.putAll( map );
	}
	
	private HashMap< State, Set<State> > getAllClosure( )
	{
		return epsilon;
	}

	public Set<Character> getTransitions( State s )
	{
		if( stateMap.containsKey( s ) ){
			return stateMap.get( s ).keySet();
		}else{
			return null;
		}
	}
	
	static public NFARuleTableImpl orMerge( NFARuleTableImpl tableA, NFARuleTableImpl tableB)
	{
		tableA.addStates( tableB.getStates() );
		tableA.addToStateMap( tableB.getStateMap() );
		Set<State> startBSet = new TreeSet<State>();
		State startB = tableB.getStart();
		startBSet.add( startB );
		State startA = tableA.getStart();
		
		tableA.addEpsilon( startA, startBSet );
		
		tableA.addAllClosure( tableB.getAllClosure() );
		return tableA;
	}
	
	static public NFARuleTableImpl andMerge( NFARuleTableImpl tableA, NFARuleTableImpl tableB )
	{
		Set<State> endStates = tableA.getAccepting();
		Iterator<State> it = endStates.iterator();
		tableA.addStates( tableB.getStates() );
		
		State start = tableB.getStart();
		
		while( it.hasNext() ){
			State next = it.next();
			next.notAccepting();
			next.setKind( null );
			tableA.addEpsilon( next, start );
		}
		
		tableA.addToStateMap( tableB.getStateMap() );
		tableA.addAllClosure( tableB.getAllClosure() );
		return tableA;
	}
	
	static public NFARuleTableImpl star( NFARuleTableImpl table ) 
	{
		Set<State> accepting = table.getAccepting();
		State start = table.getStart();
		Iterator<State> acceptingIt = accepting.iterator();
		String kind = "";
		while( acceptingIt.hasNext() ){
			State next = acceptingIt.next();
			table.addEpsilon( next, start );
			kind = next.getKind();
		}
		start.accepting();
		start.setKind( kind );
		return table;
	}
	
	private Set<State> getAccepting()
	{
		Set<State> accepting = new TreeSet<State>();
		for( State state : states ) {
			if( state.isAccepting() ) {
				accepting.add( state );
			}
		}
		/*
		Set<State> states = stateMap.keySet();
		Iterator<State> statesIt = states.iterator();
		while( statesIt.hasNext() ){
			State s = statesIt.next();
			
			if( s.isAccepting() ) {
				accepting.add( s );
			}
			
			TreeMap<Character, Set<State>> transitions = stateMap.get( s );
			if( transitions != null ){
				Iterator<Character> transitionsIt = transitions.keySet().iterator();
				while( transitionsIt.hasNext() ){
					Character c = transitionsIt.next();
					Set<State> tStates = transitions.get( c );
					Iterator<State> tStatesIt = tStates.iterator();
					while( tStatesIt.hasNext() ){
						State n = tStatesIt.next();
						if( n.isAccepting() ){
							accepting.add( n );
						}
					}
				}
			}
		}
		*/
		return accepting;
	}
	
	private void addSimpleClosure( State s )
	{
		if( epsilon.get( s ) == null ){
			Set<State> t = new TreeSet<State>();
			t.add(s);
			epsilon.put( s,t );
		}
	}
	
	
};

