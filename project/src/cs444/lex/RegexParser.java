package cs444.lex;

import cs444.parser.*;

public class RegexParser
{
	private static final String LR1_FILE = "gen/res/regex.lr1";
	private Parser parser;
	
	public RegexParser()
	{
		parser = LR1Parser.create( LR1_FILE );
	}
	
	public ParseTree parse( String regex )
	{
		parser.reset();
		for( int i = 0; i < regex.length(); i++){
			char regexChar = regex.charAt(i);
			String type;
			Symbol s;
			if( Character.isWhitespace( regexChar ) ) {
				// Allow whitespace to be used as a seperator for improved readability of complex regular expressions
				continue;
			} else if(regexChar == '*' ) {
				type = "STAR";
			}else if(regexChar == '('){
				type = "LPAREN";
			}else if(regexChar == ')'){
				type = "RPAREN";
			}else if(regexChar == '|'){
				type = "OR";
			}else if(regexChar == '['){
				type = "LBRACE";
			}else if(regexChar == ']'){
				type = "RBRACE";
			}else if(regexChar == '\\'){
				i++;
				regexChar = regex.charAt(i);
				if( regexChar == 'e'){
					type = "EPSILON";
				}else if( regexChar == 's'){
					type = "SYMBOL";
					regexChar = ' ';
				}else if( regexChar == 't'){
					type = "SYMBOL";
					regexChar = '\t';
				}else if( regexChar == 'n' ){
					type = "SYMBOL";
					regexChar = '\n';
				}else{
					type = "SYMBOL";
				}
			}else{
				type = "SYMBOL";
			}
			if( !type.equals( "EPSILON" ) ) {
				String val = String.valueOf( regexChar );
				s = new Symbol( type, val );
			}else{
				s = new Symbol( type, "" );
			}
			parser.enqueue( s );
		}
		parser.markEnd();
		try {
			parser.run();
		} catch( ParserException pe ) {
			return null;
		}
		if( parser.isComplete() ) {
			return parser.getParseTree();
		}
		return null;
	}
	
	public static void main(String args[])
	{
		System.out.println( args[0]);
		RegexParser regex = new RegexParser();
		System.out.println( regex.parse( args[0] ).toString() );
	}
};

