package cs444.lex;

import cs444.lex.rule_table.*;
import cs444.shared.*;
import cs444.parser.*;
import java.util.*;

import java.io.*;
public class Lex
{
	
	protected Map<String, Integer> priorityMap = new TreeMap<String, Integer>();
	private int counter = 0;
	private RegexParser regex = new RegexParser();
	
	public NFARuleTableImpl getNFA( String line )
	{
		String[] parts = line.split( ": ", 2 );
		ParseTree tree = regex.parse( parts[1] );
		NFABuilder nfa = new NFABuilder( tree , parts[0] );
		if( !priorityMap.containsKey( parts[0] ) ){
			priorityMap.put( parts[0], new Integer( counter ++ ) );
		}
		return nfa.build();
	}
	
	public DFARuleTable build( String fileName )
	{
		FileInputStream fReader;
		NFARuleTableImpl nfa = null;
		try{
			fReader = new FileInputStream( fileName );
		} catch ( FileNotFoundException e ){
			return null;
		}
		Scanner scan = new Scanner( fReader );
		while( scan.hasNextLine() ){
			String nextLine = scan.nextLine().trim();
			if( !nextLine.equals( "" ) ){
				
				if( nfa == null ){
					nfa = getNFA( nextLine );
				} else {
					nfa = NFARuleTableImpl.orMerge( nfa, getNFA( nextLine ) );
				}
			}
		}
		
		DFABuilder dfaBuilder = new DFABuilder();
		dfaBuilder.setNFARuleTable( nfa, priorityMap );
		return dfaBuilder.create();
	}
	
	public static void main(String[] args)
	{
		String fileName = args[0];
		Lex lex = new Lex();
		System.out.println( lex.build( fileName ).toString() );
	}
};
