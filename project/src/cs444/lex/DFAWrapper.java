package cs444.lex;

import cs444.shared.*;
import cs444.lex.rule_table.*;
import java.util.*;
import cs444.parser.*;

import java.io.*;

public class DFAWrapper implements DFA
{
	//TODO: change to private
	public DFARuleTable m_dfa;
	private State currentState;
	
	DFAWrapper( DFARuleTable dfa)
	{
		m_dfa = dfa;
		currentState = dfa.getStart();
	}
	
	public boolean nextChar( char next )
	{
		State newState = m_dfa.getNextState( currentState, next );
		if(newState != null ){
			currentState = newState;
		}
		return !( newState == null );
	}
	
	
	public boolean isAccepting()
	{
		return currentState.isAccepting();
	}
	
	public boolean isStartState()
	{
		return currentState == m_dfa.getStart();
	}
	
	public void reset()
	{
		currentState = m_dfa.getStart();
	}

  public Kind getKind()
  {
  	if( currentState.isAccepting() ){
  		return Kind.valueOf(currentState.getKind());
  	}
  	return null;
  }
   
  public static DFAWrapper create( String fileName )
  {
  	Lex lex = new Lex();
  	return new DFAWrapper( lex.build( fileName ) );
  }
  
  public static DFAWrapper createFromDFAFile( String fileName )
  {
  	DFARuleTableImpl dfa = DFARuleTableImpl.createFromDFAFile( fileName );
  	DFAWrapper dfaWrapper = new DFAWrapper( dfa );
  	return dfaWrapper; 	
  }
  
  public static void main( String[] args )
  {
  	DFAWrapper dfaWrapper = createFromDFAFile( args[0] );
  }
}
