package cs444.parser;

import java.util.*;


public class ParseTree
{
	private ParseTreeNode m_root;

	public ParseTree( ParseTreeNode root )
	{
		m_root = root;
	}
	
	public ParseTreeNode getRoot()
	{
		return m_root;
	}
	public String toString()
	{
		return inspect();
	}
	/*
	public String toString()
	{
		Stack<ParseTreeNode> stack = new Stack<ParseTreeNode>();
		Stack<Integer> indices = new Stack<Integer>();
		StringBuilder buf = new StringBuilder();
		
		stack.push( m_root );
		indices.push( Integer.valueOf( 0 ) );
		buf.append( nodeName( m_root ) );
		buf.append( "\n" );
		
		while( stack.size() > 0 ) {
			ParseTreeNode node = stack.peek();
			int idx = indices.pop();
			ParseTreeNode[] children = node.getChildren();
			if( children == null || idx >= children.length ) {
				stack.pop();
			} else {
				node = children[idx];
				indices.push( Integer.valueOf( idx + 1 ) );
				
				for( int i = 0; i < stack.size(); i++ ) {
					buf.append( "  " );
				}
				buf.append( nodeName( node ) );
				buf.append( "\n" );
				
				stack.push( node );
				indices.push( Integer.valueOf( 0 ) );
			}
		}
		return buf.toString();
	}
	public String nodeName( ParseTreeNode node )
	{
		String ret = node.getType();
		if( node.isLeaf() ) {
			ret += ":  " + node.getValue();
		}
		return ret;
	}
	*/
	public String inspect()
	{
		if( getRoot() == null ) {
			return "<empty tree>";
		}
		return getRoot().inspect();
	}
	
	// Convenience method
	public void traverseWith( NodeHandler handler ) throws HandlerException
	{
		ParseTreeTraverser traverser = new ParseTreeTraverser( handler );
		traverser.traverse( this );
	}
	
	/**
	 * Handles visitting of nodes during tree traversal.
	 */
	public static interface NodeHandler
	{
		/**
		 * Returns true if the children of the node should be visited and false otherwise.
		 */
		public boolean onEnterNode( ParseTreeNode node ) throws HandlerException;
		public void onExitNode( ParseTreeNode node ) throws HandlerException;
	}
	public static class HandlerException extends Exception
	{
		public HandlerException()
		{
			super();
		}
		
		public HandlerException( String message )
		{
			super( message );
		}
		
		public HandlerException( String message, Throwable cause )
		{
			super( message, cause );
		}
		
		public HandlerException( Throwable cause )
		{
			super( cause );
		}
	}
	public static class ParseTreeTraverser
	{
		private NodeHandler m_handler;
		
		public ParseTreeTraverser( NodeHandler handler )
		{
			m_handler = handler;
		}
	
		public void traverse( ParseTree tree ) throws HandlerException
		{
			traverse( tree.getRoot() );
		}
		public void traverse( ParseTreeNode root ) throws HandlerException
		{
			if( m_handler.onEnterNode( root ) && !root.isLeaf() ) {
				ParseTreeNode[] children = root.getChildren();
				for( ParseTreeNode child : children ) {
					traverse( child );
				}
			}
			m_handler.onExitNode( root );
		}
	}
	public static class Printer implements NodeHandler
	{
		private Appendable m_out;
		private ParseTreeTraverser m_traverser;
		private int m_depth;
		
		public Printer( Appendable out )
		{
			m_out = out;
			m_traverser = new ParseTreeTraverser( this );
			m_depth = 0;
		}
		public boolean onEnterNode( ParseTreeNode node )
		{
			try {
				if( m_depth != 0 ) {
					m_out.append( "\n" );
				}
				for( int i = 0; i < m_depth; i++ ) {
					m_out.append( "  " );
				}
				m_out.append( node.typeAndValue() );
			} catch( java.io.IOException ioe ) {
				return false;
			} finally {
				m_depth += 1;
			}
			return true;
		}
		public void onExitNode( ParseTreeNode node )
		{
			m_depth -= 1;
		}
		public void traverse( ParseTree tree )
		{
			traverse( tree.getRoot() );
		}
		public void traverse( ParseTreeNode node )
		{
			try {
				m_traverser.traverse( node );
			} catch( HandlerException h ) {
				// This should never happen
				throw new RuntimeException( "Unexpected HandlerException in ParseTree.Printer" );
			}
		}
	}
}


