package cs444.parser;

import cs444.shared.Token;
import cs444.shared.Kind;


public class Symbol implements Comparable<Symbol>
{
	protected String m_type;
	protected String m_value;
	
	public static final Symbol BEGIN = new Symbol( ":START", "" );
	public static final Symbol END = new Symbol( ":END", "" );
	
	public Symbol( String type )
	{
		this( type, null );
	}
	public Symbol( String type, String value )
	{
		m_type = type;
		m_value = value;
	}
	
	
	public boolean isTerminal()
	{
		return m_value != null;
	}
	public String getType()
	{
		return m_type;
	}
	public String getValue()
	{
		return m_value;
	}
	public ParseTreeNode toNode()
	{
		return ParseTreeNode.makeLeaf( m_type, m_value );
	}
	
	
	// Overridden Object methods
	public int hashCode()
	{
		return m_type.hashCode();
	}
	public boolean equals( Object other )
	{
		if( other instanceof Symbol )
		{
			Symbol sym = (Symbol)other;
			return m_type.equals( sym.m_type );
		}
		return false;
	}
	public String toString()
	{
		return getType();
	}
	
	// Comparable methods
	public int compareTo( Symbol other )
	{
		return m_type.compareTo( other.m_type );
	}
	
	
	// Static utility functions
	public static Symbol wrap( Token token )
	{
		String type = token.kind.toString();
		String value = token.lexeme;
		return new Symbol( type, value );
	}
}


