package cs444.parser;


public class ParserException extends Exception
{
	public ParserException()
	{
		super( "Generic Parser Exception" );
	}
	public ParserException( String msg )
	{
		super( msg );
	}
}

