package cs444.parser.rules;


public class ReductionRule<T> implements Rule<T>
{
	private T m_result;
	private int m_reductionCount;
	
	
	public ReductionRule( T result, int reductionCount )
	{
		m_result = result;
		m_reductionCount = reductionCount;
	}
	
	public boolean isReduction()
	{
		return true;
	}
	
	
	public T getReduction()
	{
		return m_result;
	}
	public int getReductionCount()
	{
		return m_reductionCount;
	}


	public State getState()
	{
		return null;
	}
}


