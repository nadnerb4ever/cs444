package cs444.parser.rules;

import java.util.*;
import java.io.*;


/**
 * A basic parse table
 */
public class SimpleParseTable implements ParseTable<String>
{
	private State m_start;
	private Map<State, Map<String, Rule<String>>> m_table;
	
	public SimpleParseTable( State start )
	{
		m_start = start;
		m_table = new HashMap<State, Map<String, Rule<String>>>();
	}

	public void addRule( State state, String symbol, Rule<String> rule )
	{
		Map<String, Rule<String>> map = m_table.get( state );
		if( map == null ) {
			map = new TreeMap<String, Rule<String>>();
			m_table.put( state, map );
		}
		map.put( symbol, rule );
	}


	// Interface methods
	public Rule<String> getRule( State state, String next )
	{
		if( m_table.containsKey( state ) ) {
			return m_table.get( state ).get( next );
		}
		return null;
	}
	public State getStartState()
	{
		return m_start;
	}



	public static SimpleParseTable create( String fileName )
	{
		SimpleParseTable ret = null;
		try {
			InputStream in = new FileInputStream( fileName );
			ret = create( in );
			in.close();
		} catch( FileNotFoundException fnfe ) {
			System.err.println( "Error: Could not find " + fileName );
		} catch( IOException ioe ) {
			// Do nothing
		}
		return ret;
	}
	public static SimpleParseTable create( InputStream input )
	{
		Scanner in = new Scanner( input );
		// Read all of the header data
		// Skip terminals
		readSymbols( in );
		// Skip non-terminals
		readSymbols( in );
		// Skip over the start symbol
		in.nextLine();
		// Read in our rules and states
		List<Rule<String>> reductions = readReductions( in );
		State[] states = readStates( in );
		List<Rule<String>> generations = makeRules( states );
		
		// Construct a table using the starting state
		SimpleParseTable table = new SimpleParseTable( states[0] );
		
		// Finally fill in the table
		int remaining = in.nextInt();
		in.nextLine();
		while( remaining > 0 ) {
			// Get the initial state
			int idx1 = in.nextInt();
			State state = states[idx1];
			// Get the transition symbol
			String symbol = in.next();
			// Get the action type to perform
			String action = in.next();
			// Get the index of the action to perform
			int idx2 = in.nextInt();
			in.nextLine();
			// Get the appropriate rule based on the action type
			Rule<String> rule = null;
			if( action.equals( "reduce" ) ) {
				rule = reductions.get( idx2 );
			} else if( action.equals( "shift" ) ) {
				rule = generations.get( idx2 );
			}
			// Put the rule into the rule table
			table.addRule( state, symbol, rule );
			remaining -= 1;
		}
		return table;
	}
	
	public static void readSymbols( Scanner in )
	{
		int terminals = in.nextInt();
		in.nextLine();
		for( int i = 0; i < terminals; i++ ) {
			in.nextLine();
		}
	}
	public static List<Rule<String>> readReductions( Scanner in )
	{
		int numRules = in.nextInt();
		in.nextLine();
		List<Rule<String>> rules = new ArrayList<Rule<String>>( numRules );
		for( int i = 0; i < numRules; i++ ) {
			String symbol = in.next();
			String rest = in.nextLine();
			int reductions = countWords( rest );
			Rule<String> rule = new ReductionRule<String>( symbol, reductions );
			rules.add( rule );
		}
		return rules;
	}
	public static State[] readStates( Scanner in )
	{
		int numStates = in.nextInt();
		in.nextLine();
		State[] states = new State[numStates];
		for( int i = 0; i < numStates; i++ ) {
			states[i] = new State( i );
		}
		return states;
	}
	public static List<Rule<String>> makeRules( State[] states )
	{
		List<Rule<String>> ret = new ArrayList<Rule<String>>( states.length );
		for( int i = 0; i < states.length; i++ )
		{
			Rule<String> rule = new GenerationRule<String>( states[i] );
			ret.add( rule );
		}
		return ret;
	}
	public static int countWords( String str )
	{
		int count = 0;
		boolean space = true;
		for( int i = 0; i < str.length(); i++ ) {
			char c = str.charAt( i );
			switch( c ) {
				case ' ':
				case '\t':
				case '\n':
					space = true;
					break;
				default:
					if( space ) {
						count += 1;
						space = false;
					}
			}
		}
		return count;
	}
}

