package cs444.parser.rules;


public class GenerationRule<T> implements Rule<T>
{
	private State m_state;
	
	
	public GenerationRule( State state )
	{
		m_state = state;
	}
	
	public boolean isReduction()
	{
		return false;
	}
	
	
	public T getReduction()
	{
		return null;
	}
	public int getReductionCount()
	{
		return 0;
	}


	public State getState()
	{
		return m_state;
	}
}

