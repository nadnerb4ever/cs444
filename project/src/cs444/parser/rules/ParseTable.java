package cs444.parser.rules;

/**
 * The interface for an object which is used to lookup generation and reduction rules for a parser
 */
public interface ParseTable<T>
{
	/**
	 * Returns the rule for what to do under a certain state and token
	 */
	public Rule<T> getRule( State state, T next );
	/**
	 * Returns the starting state
	 */
	public State getStartState();
}



