package cs444.parser.rules;


public class State
{
	protected int m_id;
	
	public State( int id )
	{
		m_id = id;
	}
	
	// Overridden Object methods
	public int hashCode()
	{
		return m_id;
	}
	public boolean equals( Object other )
	{
		if( other instanceof State )
		{
			return m_id == ((State)other).m_id;
		}
		return false;
	}
	public String toString()
	{
		return "<State: " + m_id + ">";
	}
}
