package cs444.parser.rules;


public interface Rule<T>
{
	public boolean isReduction();
	/*
	 * For reduction rules
	 */
	public T getReduction();
	public int getReductionCount();
	
	/*
	 * For shift rules
	 */
	public State getState();
}


