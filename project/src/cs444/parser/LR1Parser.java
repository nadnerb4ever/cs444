package cs444.parser;

import cs444.shared.*;
import cs444.parser.rules.*;

import java.util.*;


public class LR1Parser implements Parser
{
	private ParseTable<String> m_table;
	private SymbolStream m_stream;
	
	private Stack<State> m_states;
	private Stack<Symbol> m_symbols;
	
	private ParseTree m_result;
	

	public LR1Parser( ParseTable<String> table )
	{
		m_table = table;
		m_stream = new SymbolStream();
		
		m_states = new Stack<State>();
		m_symbols = new Stack<Symbol>();
		
		reset();
	}
	
	
	// Interface methods
	public void enqueue( Symbol next )
	{
		m_stream.append( next );
	}
	public void enqueue( Collection<Symbol> next )
	{
		for( Symbol symbol : next ) {
			enqueue( symbol );
		}
	}
	public void markEnd()
	{
		m_stream.pushEOS();
	}
	public boolean isComplete()
	{
		return m_symbols.peek().equals( Symbol.END );
	}
	public void reset()
	{
		m_result = null;
		m_stream.clear();
		m_states.clear();
		m_symbols.clear();
		m_states.push( m_table.getStartState() );
	}
	public ParseTree getParseTree()
	{
		ParseTree ret = m_result;
		if( m_result == null && isComplete() ) {
			ret = m_result = new ParseTree( m_symbols.get( 1 ).toNode() );
		}
		return ret;
	}
	
	
	public void run() throws ParserException
	{
		while( !m_stream.isEmpty() )
		{
			runOnce();
		}
	}
	public void runOnce() throws ParserException
	{
		// Get the rule for the current state and symbol
		State currState = m_states.peek();
		Symbol next = m_stream.next();
		Rule<String> rule = m_table.getRule( currState, next.getType() );
		
		if( rule == null ) {
			throw new ParserException( "Invalid symbol: " + next.toString() );
		}
		
		if( rule.isReduction() ) {
			// Put the symbol we just read back onto the stream, so we can read it again later
			m_stream.pushBack( next );
			
			// Pop an appropriate number of entries off of our symbol and state stacks
			int count = rule.getReductionCount();
			ParseTreeNode[] children = new ParseTreeNode[count];
			while( count > 0 ) {
				count -= 1;
				m_states.pop();
				children[count] = m_symbols.pop().toNode();
			}
			
			// Reduces the symbols to a new ParseTreeNode (Symbol) and put into our symbol stream
			String type = rule.getReduction();
			Symbol result = ParseTreeNode.makeInternal( type, children );
			m_stream.pushBack( result );
			
		} else {
			// Add our symbol to the symbol stack
			m_symbols.push( next );
			// Add the associated state to the state stack
			m_states.push( rule.getState() );
		}
	}	
	
	public static LR1Parser create( String rulesFileName )
	{
		LR1Parser ret = null;
		SimpleParseTable parseTable = SimpleParseTable.create( rulesFileName );
		if( parseTable != null ) {
			ret = new LR1Parser( parseTable );
		}
		return ret;
	}
}
