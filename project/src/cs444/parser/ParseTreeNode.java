package cs444.parser;

import java.util.*;


public class ParseTreeNode extends Symbol
{
	private ParseTreeNode[] m_children;
	private Object m_annotation = null;

	private ParseTreeNode( String type, String value, ParseTreeNode[] children )
	{
		super( type, value );
		m_children = children;
	}


	public boolean isLeaf()
	{
		return isTerminal();
	}
	public ParseTreeNode[] getChildren()
	{
		return m_children;
	}
	public int numChildren()
	{
		return (m_children == null ? 0 : m_children.length);
	}
	public ParseTreeNode child( int idx )
	{
		return m_children[idx];
	}
	public ParseTreeNode toNode()
	{
		return this;
	}
	public boolean matches( String type, String[] children )
	{
		if( !type.equals( m_type ) ) {
			return false;
		}
		if( children.length != m_children.length ) {
			return false;
		}
		for( int i = 0; i < children.length; i++ ) {
			if( !children[i].equals( m_children[i].getType() ) ) {
				return false;
			}
		}
		return true;
	}
	
	public void annotate( Object annotation )
	{
		m_annotation = annotation;
	}
	public Object annotation()
	{
		return m_annotation;
	}
	
	public String toString()
	{
		if( isLeaf() ) {
			return getValue();
		}
		StringBuilder builder = new StringBuilder();
		for( ParseTreeNode child : getChildren() ) {
			builder.append( child.toString() );
		}
		return builder.toString();
	}
	public Set<String> asSet( int idx )
	{
		Set<String> ret = new TreeSet<String>();
		ParseTreeNode current = this;
		while( current != null && !current.isLeaf() ) {
			ParseTreeNode[] children = current.getChildren();
			ParseTreeNode prev = current;
			current = null;
			if( children.length > idx ) {
				ret.add( children[idx].toString() );
			}
			for( ParseTreeNode node : children ) {
				if( node.getType().equals( prev.getType() ) ) {
					current = node;
				}
			}
		}
		return ret;
	}
	public String typeAndValue()
	{
		String ret = getType();
		if( isLeaf() ) {
			ret += ":  " + getValue();
		}
		return ret;
	}
	public String inspect()
	{
		StringBuilder builder = new StringBuilder();
		ParseTree.Printer printer = new ParseTree.Printer( builder );
		printer.traverse( this );
		return builder.toString();
	}
	
	
	public static ParseTreeNode makeLeaf( String type, String value )
	{
		return new ParseTreeNode( type, value, null );
	}
	public static ParseTreeNode makeInternal( String type, ParseTreeNode[] children )
	{
		return new ParseTreeNode( type, null, children );
	}
}


