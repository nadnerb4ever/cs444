package cs444.parser;

import java.util.*;


/**
 * Class for turning collection of tokens into modifiable stream of symbols.
 */
public class SymbolStream
{
	private Deque<Symbol> m_stream;
	private boolean m_complete;


	public SymbolStream()
	{
		m_stream = new LinkedList<Symbol>();
		clear();
	}

	/**
	 * Adds a collection of symbols to the end of the stream
	 */
	public void append( Collection<Symbol> symbols )
	{
		for( Symbol symbol : symbols )
		{
			append( symbol );
		}
	}
	/**
	 * Adds a single symbol to the end of the stream
	 */
	public void append( Symbol symbol )
	{
		if( !m_complete ) {
			m_stream.addLast( symbol );
		}
	}
	/**
	 * Places a symbol at the front of the stream
	 */
	public void pushBack( Symbol symbol )
	{
		m_stream.addFirst( symbol );
	}
	/**
	 * Gets the first symbol from the front of the stream
	 */
	public Symbol next()
	{
		return m_stream.pollFirst();
	}
	/**
	 * Returns whether the stream is empty
	 */
	public boolean isEmpty()
	{
		return m_stream.isEmpty();
	}
	/**
	 * Returns whether the stream has been finished
	 */
	public boolean isComplete()
	{
		return m_complete;
	}
	/**
	 * Finalizes the stream by appending the END symbol
	 */
	public void pushEOS()
	{
		append( Symbol.END );
		m_complete = true;
	}
	/**
	 * Clears the contents of the stream
	 */
	public void clear()
	{
	 	m_stream.clear();
	 	pushBack( Symbol.BEGIN );
	 	m_complete = false;
	}
}


