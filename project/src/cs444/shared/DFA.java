package cs444.shared;


/**
 * The interface for a deterministic finite automoton
 */
public interface DFA
{
	/**
	 * Returns whether it successfully accepted the token
	 */
	public boolean nextChar( char next );
	
	/**
	 * Returns whether the DFA is in an accepting state
	 */
	public boolean isAccepting();
	
	/**
	 * Returns whether the DFA is in its start state
	 */
	public boolean isStartState();
	
	/**
	 * Resets the DFA back to its start state
	 */
	public void reset();
	
	/**
     * Returns Kind if it is in an accepting state
     * null otherwise.
     */
    public Kind getKind();
    
}


