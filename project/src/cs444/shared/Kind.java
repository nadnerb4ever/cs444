package cs444.shared;


/** The various kinds of tokens. */
public enum Kind {
    /* 3.3 Unicode escape */
   
    /* 3.4 Line terminator */

    /* 3.6 White space */
    WHITESPACE, // Whitespace
    
    /* 3.7 Comments */
    TRADITIONAL,    // /* */
    SINGLELINE,    // //
    DOCUMENTATION, // /** */
    
    /* 3.8 Identifier */
    ID, // identifier
    
    /* 3.9 Keywords */
    ABSTRACT,     // abstract 
    BOOLEAN,      // boolean 
    BREAK,        // break 
    BYTE,         // byte 
    CASE,         // case 
    CATCH,        // catch 
    CHAR,         // char 
    CLASS,        // class
    CONST,        // const
    CONTINUE,     // continue
    DEFAULT,      // default 
    DO,           // do 
    DOUBLE,       // double 
    ELSE,         // else 
    EXTENDS,      // extends
    FINAL,        // final 
    FINALLY,      // finally 
    FLOAT,        // float 
    FOR,          // for 
    GOTO,         // goto
    IF,           // if 
    IMPLEMENTS,   // implements
    IMPORT,       // import 
    INSTANCEOF,   // instanceof 
    INT,          // int 
    INTERFACE,    // interface
    LONG,         // long 
    NATIVE,       // native
    NEW,          // new 
    PACKAGE,      // package 
    PRIVATE,      // private
    PROTECTED,    // protected
    PUBLIC,       // public 
    RETURN,       // return 
    SHORT,        // short 
    STATIC,       // static 
    SUPER,        // super 
    SWITCH,       // switch
    SYNCHRONIZED, // synchronized 
    THIS,         // this
    THROW,        // throw 
    THROWS,       // throws 
    TRANSIENT,    // transient 
    TRY,          // try
    VOID,         // void 
    VOLATILE,     // volatile 
    WHILE,        // while 
    NULL,         // null
    TRUE,         // true
    FALSE,        // false
    
    
    /* 3.10 Literals */
    DECIMAL,         // Base10 int
    LONGDECIMAL,     // Base10 long
    HEXADECIMAL,     // Base16 int
    LONGHEXADECIMAL, // Base10 long
    OCTAL,           // Base8 int
    LONGOCTAL,       // Base8 long
    FLOATINGPOINT,   // Floating point
    DOUBLELITERAL,	 // Double
    STRINGLITERAL,   // String literal
    CHARLITERAL,     // Charcter literal
    
    /* 3.11 Separators */
    LPAREN,      // (
    RPAREN,      // )
    LBRACE,      // {
    RBRACE,      // }
    LSQBRACE,    // [
    RSQBRACE,    // ]
    SEMI,        // ;
    COMMA,       // ,
    DOT,         // .
    
    
    /* 3.12 Operators */
    ASSIGN,        // =
    EQ,            // ==
    LT,            // <
    LE,            // <=
    LSHIFT,        // <<
    LSHIFTASSIGN,  // <<=
    GT,            // >
    GE,            // >=
    RSHIFT,        // >>
    RSHIFTASSIGN,  // >>=
    URSHIFT,       // >>>
    URSHIFTASSIGN, // >>>=
    NOT,           // !
    ORASSIGN,      // |=
    NE,            // != 
    BITCOMPL,      // ~
    QUESTION,      // ?
    COLON,         // :
    PLUS,          // +
    INC,           // ++
    PLUSASSIGN,    // +=
    MINUS,         // -
    DEC,           // --
    MINUSASSIGN,   // -=
    STAR,          // *
    STARASSIGN,    // *=
    SLASH,         // /
    SLASHASSIGN,   // /=
    BITAND,        // &
    LOGICALAND,    // &&
    ANDASSIGN,     // &=
    BITOR,         // |
    LOGICALOR,     // ||
    BITXOR,        // ^
    XORASSIGN,     // ^=
    REMAIN,        // %
    REMAINASSIGN,  // %= 

}
