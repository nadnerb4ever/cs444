package cs444.shared;


/** Representation of a token. */
public class Token {
    public Kind kind;     // The kind of token.
    public String lexeme; // String representation of the actual token in the source code.

    public Token(Kind kind, String lexeme) {
        this.kind = kind;
        this.lexeme = lexeme;
    }
    public String toString() {
        return kind + " {" + lexeme + "}";
    }
    
    /** Returns an integer representation of the token. For tokens of kind
     * INT (decimal integer constant) and HEXINT (hexadecimal integer
     * constant), returns the integer constant. For tokens of kind
     * REGISTER, returns the register number.
     */
/*    public int toInt() {
        if(kind == Kind.INT) return parseLiteral(lexeme, 10, 32);
        else if(kind == Kind.HEXINT) return parseLiteral(lexeme.substring(2), 16, 32);
        else if(kind == Kind.REGISTER) return parseLiteral(lexeme.substring(1), 10, 5);
        else {
            System.err.println("ERROR in to-int conversion.");
            //System.exit(1);
            return 0;
        }
    }
    private int parseLiteral(String s, int base, int bits) {
        BigInteger x = new BigInteger(s, base);
        if(x.signum() > 0) {
            if(x.bitLength() > bits) {
                System.err.println("ERROR in parsing: constant out of range: "+s);
                //System.exit(1);
            }
        } else if(x.signum() < 0) {
            if(x.negate().bitLength() > bits-1
            && x.negate().subtract(new BigInteger("1")).bitLength() > bits-1) {
                System.err.println("ERROR in parsing: constant out of range: "+s);
                //System.exit(1);
            }
        }
        return (int) (x.longValue() & ((1L << bits) - 1));
    }*/
}
