package cs444.utils;

import java.util.*;
import cs444.typeLinker.*;
import cs444.codeGen.*;


public class Code
{
	private static String m_entryPoint = null;
	private static Map<String, Collection<String>> m_stringLiterals = new TreeMap<String, Collection<String>>();


	public static String toHex( int i )
	{
		String padding;
		if( i < 0 ) {
			padding = "f";
		} else {
			padding = "0";
		}
		StringBuilder sb = new StringBuilder();
		String s = Integer.toHexString( i );
		int len = s.length();
		sb.append( "0x" );
		while( len < 8 ) {
			sb.append( padding );
			len++;
		}
		sb.append( s );
		return sb.toString();
	}
	
	public static String makeLabel( String prefix, JoosObjectType type )
	{
		return prefix + type.fullName();
	}
	public static String makeLabel( JoosMethod method )
	{
		String label;
		if( method.isNative() ) {
			JoosObjectType origin = method.getSource();
			label = "NATIVE" + origin.fullName() + "." + method.getName();
		} else {
			label = "__method" + method.getIdentifier() + "__" + method.getName();
		}
		if( method.isStatic() && method.getName().equals( "test" ) && method.getParameters().size() == 0 ) {
			m_entryPoint = label;
		}
		return label;
	}
	public static String uniqueLabel( String name )
	{
		return "__" + name + Label.getLabelIndex() + "__";
	}
	public static String getEntryPoint()
	{
		return m_entryPoint;
	}
	public static String getStringLiteral( String literal )
	{
		String label = uniqueLabel( "StringLiteral" );
		Collection<String> labels = m_stringLiterals.get( literal );
		if( labels == null ) {
			labels = new ArrayList<String>();
			m_stringLiterals.put( literal, labels );
		}
		labels.add( label );
		return label;
	}
	public static void genCode( Appendable out ) throws java.io.IOException
	{
		out.append( "extern " + getEntryPoint() + "\n\n" );
		out.append( ";; ENTRY POINT\n" );
		out.append( "global _start\n" );
		out.append( "_start:\n" );
		out.append( "call " + getEntryPoint() + "\n" );
		out.append( "mov ebx, eax\n" );
		out.append( "mov eax, 0x1\n" );
		out.append( "int 0x80\n" );
		out.append( "\n\n" );
		
		// TODO add the entry point
		for( String literal : m_stringLiterals.keySet() ) {
			for( String label : m_stringLiterals.get( literal ) ) {
				out.append( "global " + label + "\n" );
				out.append( label + ":\n" );
			}
			out.append( "dd 0x00000000\n" );
			// TODO put string object here
		}
	}
}

