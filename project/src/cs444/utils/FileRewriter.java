package cs444.utils;

import java.io.*;
import java.util.*;
import cs444.parser.Symbol;


public class FileRewriter
{
	public static void main( String[] args )
	{
		FileRewriter rw = new FileRewriter();
		rw.rewriteCFG( System.in, System.out );
	}


	public void rewriteCFG( InputStream in, PrintStream out )
	{
		rewriteCFG( new Scanner( in ), out );
	}
	public void rewriteCFG( Scanner in, PrintStream out )
	{
		String start = null;
		Set<String> terminals = new TreeSet<String>();
		Set<String> nonTerminals = new TreeSet<String>();
		List<String> rules = new ArrayList<String>();
		
		// Read in our cfg file
		while( in.hasNextLine() ) {
			String line = in.nextLine().trim();
			if( line.equals( "" ) || line.charAt( 0 ) == '#' ) {
				// Then we have an empty line or a comment
				continue;
			}
			String[] parts = line.split( " +" );
			if( start == null ) {
				start = parts[0];
			}
			terminals.remove( parts[0] );
			nonTerminals.add( parts[0] );
			for( int i = 1; i < parts.length; i++ ) {
				if( !nonTerminals.contains( parts[i] ) ) {
					terminals.add( parts[i] );
				}
			}
			rules.add( line );
		}
		
		// Augment our grammar
		String state = start;
		start = "(Start)";
		rules.add( start + " " + Symbol.BEGIN + " " + state + " " + Symbol.END );
		nonTerminals.add( start );
		terminals.add( Symbol.BEGIN.toString() );
		terminals.add( Symbol.END.toString() );
		
		// Write out the CFG file in the expected format
		print( out, terminals );
		print( out, nonTerminals );
		out.println( start );
		print( out, rules );
	}
	public void print( PrintStream out, Collection<String> lines )
	{
		out.println( lines.size() );
		for( String line : lines ) {
		    out.println( line );
		}
	}
}


