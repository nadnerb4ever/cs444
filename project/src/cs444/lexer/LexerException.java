package cs444.lexer;

public class LexerException extends Exception
{
    public LexerException()
    {
        super( "Generic Lexer Exception" );
    }
    public LexerException( String msg )
    {
        super( msg );
    }
}

