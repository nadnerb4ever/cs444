package cs444.lexer;

import java.util.List;
import cs444.shared.Token;
import java.io.InputStream;

public interface Lexer {
    
    /**
     * Returns a list of Tokens
     */
    public List<Token> scan(String jooscCode) throws LexerException;
    public List<Token> scan( InputStream in ) throws LexerException;
}
