/**
 * LexicalScanner Class
 * 
 * @author htanaka
 *
 */

package cs444.lexer;

import cs444.shared.DFA;
import cs444.shared.Kind;
import cs444.shared.Token;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.InputStream;

public class LexicalScanner implements Lexer{
    private DFA dfa;
    int lineNum;
    private final static String WHITE_SPACE = "[\n\t ]";
    private final static Kind[] KINDS_TO_BE_IGNORED = new Kind[]{ Kind.SINGLELINE,
                                                                  Kind.TRADITIONAL,
                                                                  Kind.DOCUMENTATION,
                                                                  Kind.WHITESPACE
                                                                };
    
    
    /**
     * LexicalScanner constructor
     * @param dfa: DFA to be used for scanning 
     */
    public LexicalScanner(DFA dfa){
        this.dfa = dfa;
        this.dfa.reset();
    }

    @Override
    public List<Token> scan(InputStream in) throws LexerException {
        return scan(new Scanner(in));
    }
    
    /**
     * scan method
     * @param input: joosc code to be scanned
     * @return List of tokens recognized in input using DFA
     */
    public List<Token> scan(Scanner in) throws LexerException{
        String str = "";
        while (in.hasNext()) {
            str += in.nextLine() + "\n";
        }
        return scan(str);
    }  // scan(String input)
    
    
    public List<Token> scan(String input) throws LexerException{
        //  Return immediately if input is empty
        if(input.isEmpty()) return new ArrayList<Token>(0);

        /*
         *  Initialization Phase
         *  tokens - List of tokens recognized
         *  lexemeBeginIndex - Start position of a lexeme
         *  curIndex - Current index
         *  lastAcceptedIndex - Last index accepted by dfa
         *  lastAcceptedKind - Kind given at lastAcceptedIndex
         *  eof - End of input identifier 
         *        true if curIndex >= input size
         */
        lineNum = 1;
        int lastNewLineIndex = 0;
        List<Token> tokens = new ArrayList<Token>();
        int lexemeBeginIndex = 0;
        int curIndex = 0;
        int lastAcceptedIndex = 0;
        char c = ' ';
        Kind lastAcceptedKind = null;
        boolean eof = false;

        // loop and scan symbol by symbol until curIndex reaches the end of input.  
        while(true){
            
            /*
             *   Every time DFA accepts a character, scanner checks if DFA is 
             *  in the accepting(Final) state. If it is in the accepting state, 
             *  record the Kind and index for backtracking.
             *  Side note: eof is checked so that charAt(curIndex) does not throw
             *             out-of-range exception.
             */
            if(!eof && dfa.nextChar(c = input.charAt(curIndex))){
                if(dfa.isAccepting()){
                    lastAcceptedIndex = curIndex;
                    lastAcceptedKind = dfa.getKind();
                }
                curIndex++;
            }
            
            /*
             *   If character is rejected by the DFA, create a token from the 
             *  last state accepted and add it to the list of tokens.
             *   If there is no state accepted, it means either;
             *   - we finished feeding all characters of input to DFA, or 
             *   - DFA is at the start state and we have an invalid 
             *     character(i.e WHITESPACE, \n\t).
             */
            else{
                if(lastAcceptedKind != null){
                    if(isValidKind(lastAcceptedKind)){
                        String lexeme = input.substring(lexemeBeginIndex, lastAcceptedIndex + 1);
                        Token t = new Token(lastAcceptedKind, lexeme);
                        tokens.add( t );
                    }
                }else if(eof){
                	
                    if(curIndex > lexemeBeginIndex){
                        error(input.substring(lexemeBeginIndex));
                    }
                    break;
                }else if(!("" + c).matches(WHITE_SPACE)){ // Ignore white spaces
                    error("" + c);
                }
                                
                // Reset DFA and indexes.
                curIndex = lexemeBeginIndex = ++lastAcceptedIndex;
                dfa.reset();
                lastAcceptedKind = null;
            }
            eof = curIndex >= input.length();
            
            if( c == '\n' && curIndex > lastNewLineIndex ){ 
                lastNewLineIndex = curIndex;
                lineNum++; 
            }
        } // while(true)
        return tokens;
    }
    
    /*
     * returns true if k is not in the KINDS_TO_BE_IGONORED
     */
    private boolean isValidKind(Kind k){
        for(int i = 0; i < KINDS_TO_BE_IGNORED.length; i++)
            if(k.equals(KINDS_TO_BE_IGNORED[i])) return false;
        return true;
    }

    private void error(String str) throws LexerException{
        throw new LexerException("Error while reading '" + str + "' on line " + lineNum);
    }


} // class LexicalScanner
