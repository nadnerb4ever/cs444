package cs444.lexer;

import cs444.shared.DFA;

public class LexerFactory {
    public static Lexer getLexer(DFA dfa){
        return new LexicalScanner(dfa);
    }
}
