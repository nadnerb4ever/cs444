package cs444.ast;

import cs444.ast.expression.*;
import cs444.ast.statement.*;
import cs444.parser.ParseTreeNode;
import cs444.parser.ParseTree.HandlerException;
import cs444.parser.ParseTree.NodeHandler;
import cs444.parser.ParseTree.ParseTreeTraverser;
import cs444.typeLinker.*;

/**
 * FieldDeclaration MemberModifiers Type OptSQBRACES VariableDeclarator SEMI
 * VariableDeclarator ID
 * VariableDeclarator ID ASSIGN Expression
 */

public class FieldExpressionBuilder implements NodeHandler
{
	private Environment m_env;
	private JoosClass m_class;
	private Expression m_expr;
	
	public FieldExpressionBuilder( Environment env, JoosClass jClass )
	{
		m_env = env;
		m_class = jClass;
		m_expr = null;
	}
	
	public Expression getExpression()
	{
		return m_expr;
	}

	@Override
	public boolean onEnterNode( ParseTreeNode node ) throws HandlerException
	{
		if( node.getType().equals( "FieldDeclaration" ) ) { // &&  ) {
			boolean isStatic = node.child( 0 ).asSet( 0 ).contains( "static" );
			node = node.child( 3 );
			if( node.numChildren() == 3 ) {
				Block initializer;
				JoosScope scope;
				if( !isStatic ) {
					initializer = m_class.getInitializer();
					scope = m_class.getClassScope();
				} else {
					initializer = m_class.getStaticInitializer();
					scope = m_class.getStaticScope();
				}
				ExpressionBuilder exprBuilder = new ExpressionBuilder( m_env, scope, m_class );
				ParseTreeTraverser traverser = new ParseTreeTraverser( exprBuilder );
				traverser.traverse( node );
				initializer.addStatement( new ExpressionStatement( exprBuilder.getResult() ) );
			}
			return false;
		}
		return true;
	}

	public void onExitNode(ParseTreeNode node) throws HandlerException {

	}
	
	/**
	 * A NodeHandler for using at the top level of the tree. It creates and FieldExpressionBuilder for each field.
	 */
	 /*
	public static class Helper implements NodeHandler
	{
		private Environment m_env;
		
		public Helper( Environment env )
		{
			m_env = env;
		}
		
		public boolean onEnterNode( ParseTreeNode node ) throws HandlerException
		{
			if( node.getType().equals( "FieldDeclaration" ) ) {
				JoosClass jClass = (JoosClass)node.annotation();
				FieldExpressionBuilder builder = new FieldExpressionBuilder( m_env, jClass );
				ParseTreeTraverser traverser = new ParseTreeTraverser( builder );
				traverser.traverse( node );
				
				return false;
			}
			return true;
		}
		public void onExitNode( ParseTreeNode node ) throws HandlerException { }
	}
	*/
}
