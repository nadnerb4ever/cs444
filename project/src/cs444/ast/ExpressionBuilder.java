package cs444.ast;

import java.util.*;

import cs444.ast.expression.*;
import cs444.ast.NoTypeException;
import cs444.typeLinker.*;

import cs444.parser.ParseTreeNode;
import cs444.parser.ParseTree.HandlerException;
import cs444.parser.ParseTree.NodeHandler;


public class ExpressionBuilder implements NodeHandler
{
	private Environment m_env;
	private JoosClass m_class;
	private JoosMethod m_method;
	private JoosScope m_scope;

	private Stack<List<Expression>> m_exprLists;
	private Expression m_result;
	
	private boolean m_inCastExpression = false;
	//private ParseTreeNode m_methodNode = null;
	private boolean m_skippingNode = false;
	private boolean m_isInMethod = false;


	public ExpressionBuilder( Environment env, JoosScope scope, JoosClass jClass )
	{
		this( env, scope, jClass, null );
	}
	public ExpressionBuilder( Environment env, JoosScope scope, JoosClass jClass, JoosMethod method )
	{
		m_env = env;
		m_scope = scope;
		m_method = method;
		m_class = jClass;
		m_exprLists = new Stack<List<Expression>>();
		m_result = null;
	}
	
	public boolean onEnterNode( ParseTreeNode node ) throws HandlerException
	{
		String type = node.getType();
		if( node.isLeaf() || (m_skippingNode = skipNode( node )) ) {
			return false;
		}
		m_inCastExpression = type.equals( "CastExpression" );
		m_isInMethod = type.equals( "MethodInvocation" );
		
		m_exprLists.push( new ArrayList<Expression>() );
		return true;
	}
	public void onExitNode( ParseTreeNode node ) throws HandlerException
	{
		String type = node.getType();
		Expression result;
		if( m_skippingNode ) {
			m_skippingNode = false;
			result = null;
		} else if( node.isLeaf() ) {
			result = makeExpressionFromLeaf( node );
		} else if( node.getChildren().length == 1 && !type.equals( "MemberAccess" ) ) {
			List<Expression> results = m_exprLists.pop();
			if( results.size() == 0 ) {
			 result = null;
			} else {
				result = results.get( 0 );
			}
		} else {
			result = makeExpression( node, m_exprLists.pop() );
		}
		
		//System.err.println( "size: " + m_exprLists.size() );
		if( result == null ) {
			if( m_exprLists.size() == 0 ) {
				System.err.println( node.inspect() );
			}
			return;
		} else if( m_exprLists.size() == 0 ) {
			m_result = result;
		} else {
			m_exprLists.peek().add( result );
		}
	}
	
	private Expression makeExpressionFromLeaf( ParseTreeNode node ) throws HandlerException
	{
		String type = node.getType();
		String val = node.getValue();
		Expression ret = null;
		
		if( type.equals( "THIS" ) ) {
			ret = new This( m_class );
		} else if( type.equals( "NULL" ) ) {
			ret = new ConstantExpression.NullLiteral();
		} else if( type.equals( "TRUE" ) ) {
			ret = new ConstantExpression.TrueLiteral();
		} else if( type.equals( "FALSE" ) ) {
			ret = new ConstantExpression.FalseLiteral();
		} else if( type.equals( "DECIMAL" ) ) {
			ret = new ConstantExpression.IntLiteral( val );
		} /*else if( type.equals( "LONGDECIMAL" ) ) {
			ret = new ConstantExpression.LongLiteral( val );
		}*/ else if( type.equals( "DOUBLELITERAL" ) ) {
			ret = new ConstantExpression.DoubleLiteral( val );
		} else if( type.equals( "STRINGLITERAL" ) ) {
			ret = new ConstantExpression.StringLiteral( val );
		} else if( type.equals( "CHARLITERAL" ) ) {
			ret = new ConstantExpression.CharLiteral( val );
		}
		return ret;
	}
	private Expression makeExpression( ParseTreeNode node, List<Expression> subExpressions ) throws HandlerException
	{
		String type = node.getType();
		ParseTreeNode[] children = node.getChildren();
		Expression ret = null;
		
		try {
			if( type.equals( "Assignment" ) || type.equals( "AssignmentExpression" ) ) {
				ret = new BinaryExpression.Assign( subExpressions.get( 0 ), subExpressions.get( 1 ) );
			} else if( type.equals( "DeclStmtAssignmentExpression" ) || type.equals( "VariableDeclarator" ) ) {
				Expression lhs = new VariableReference( m_scope.lookup( children[0].getValue() ) );
				ret = new BinaryExpression.Assign( lhs, subExpressions.get( 0 ) );
			} else if( type.equals( "LogicalOR" ) ) {
				ret = new BinaryExpression.Or( subExpressions.get( 0 ), subExpressions.get( 1 ) );
			} else if( type.equals( "LogicalAND" ) ) {
				ret = new BinaryExpression.And( subExpressions.get( 0 ), subExpressions.get( 1 ) );
			} else if( type.equals( "Equality" ) ) {
				if( children[1].getType().equals( "EQ" ) ) {
					ret = new BinaryExpression.Equal( subExpressions.get( 0 ), subExpressions.get( 1 ) );
				} else if( children[1].getType().equals( "NE" ) ) {
					ret = new BinaryExpression.NotEqual( subExpressions.get( 0 ), subExpressions.get( 1 ) );
				}
			} else if( type.equals( "Relational" ) ) {
				if( children[1].getType().equals( "LT" ) ) {
					ret = new BinaryExpression.LessThan( subExpressions.get( 0 ), subExpressions.get( 1 ) );
				} else if( children[1].getType().equals( "GT" ) ) {
					ret = new BinaryExpression.GreaterThan( subExpressions.get( 0 ), subExpressions.get( 1 ) );
				} else if( children[1].getType().equals( "LE" ) ) {
					ret = new BinaryExpression.LessEqual( subExpressions.get( 0 ), subExpressions.get( 1 ) );
				} else if( children[1].getType().equals( "GE" ) ) {
					ret = new BinaryExpression.GreaterEqual( subExpressions.get( 0 ), subExpressions.get( 1 ) );
				} else if( children[1].getType().equals( "INSTANCEOF" ) ) {
					JoosType checkType = m_env.lookup( children[2].toString() );
					if( children[3].toString().equals( "[]" ) ) {
						checkType = checkType.arrayOf();
					}
					ret = new InstanceOf( subExpressions.get( 0 ), checkType );
				}
			} else if( type.equals( "Additive" ) ) {
				if( children[1].getType().equals( "PLUS" ) ) {
					ret = new BinaryExpression.Plus( subExpressions.get( 0 ), subExpressions.get( 1 ) );
				} else if( children[1].getType().equals( "MINUS" ) ) {
					ret = new BinaryExpression.Minus( subExpressions.get( 0 ), subExpressions.get( 1 ) );
				}
			} else if( type.equals( "Multiplicative" ) ) {
				if( children[1].getType().equals( "STAR" ) ) {
					ret = new BinaryExpression.Mult( subExpressions.get( 0 ), subExpressions.get( 1 ) );
				} else if( children[1].getType().equals( "SLASH" ) ) {
					ret = new BinaryExpression.Div( subExpressions.get( 0 ), subExpressions.get( 1 ) );
				} else if( children[1].getType().equals( "REMAIN" ) ) {
					ret = new BinaryExpression.Remainder( subExpressions.get( 0 ), subExpressions.get( 1 ) );
				}
			} else if( type.equals( "Unary" ) ) {
				ret = new UnaryExpression.Negative( subExpressions.get( 0 ) );
			} else if( type.equals( "UnaryNotPlusMinus" ) ) {
				ret = new UnaryExpression.Not( subExpressions.get( 0 ) );
			} else if( type.equals( "CastExpression" ) ) {
				JoosType castType = m_env.lookup( children[1].toString() );
				if( children[2].toString().startsWith( "[" ) ) {
					castType = castType.arrayOf();
				}
				ret = new Cast( castType, subExpressions.get( 0 ) );
			} else if( type.equals( "PrimaryNoNewArray" ) ) {
				ret = subExpressions.get( 0 );
			} else if( type.equals( "MethodInvocation" ) ) {
				InvocationExpression.FieldInvocation sub = (InvocationExpression.FieldInvocation)subExpressions.get( 0 );
				JoosClass context = sub.getContext();
				Expression instance = sub.getSubExpression();
				String methodName = sub.getFieldName();
				List<Expression> args = new ArrayList<Expression>();
				
				for( int i = 1; i < subExpressions.size(); i++ ) {
					args.add( subExpressions.get( i ) );
				}
				ret = new InvocationExpression.MethodInvocation( context, instance, methodName, args );
			} else if( type.equals( "ArrayAccess" ) ) {
				ret = new InvocationExpression.ArrayInvocation( subExpressions.get( 0 ), subExpressions.get( 1 ) );
			} else if( type.equals( "MemberAccess" ) ) {
				Expression base = null;
				String rest = null;
				if( children.length == 1 ) {
					String qualifiedID = children[0].toString();
					String[] parts = qualifiedID.split( "\\.", 2 );
					try {
						base = new VariableReference( m_scope.lookup( parts[0] ) );
						if( parts.length == 2 ) {
							rest = parts[1];
						}
					} catch( Exception e ) {
						int idx = qualifiedID.indexOf( '.' );
						while( idx > -1 ) {
							JoosType staticType;
							try {
								staticType = m_env.lookup( qualifiedID.substring( 0, idx ) );
							} catch( Exception useless ) {
								idx = qualifiedID.indexOf( '.', idx + 1 );
								continue;
							}
							JoosClass jClass = staticType.asObjectType().asJoosClass();
							base = new StaticInstance( jClass );
							rest = qualifiedID.substring( idx+1, qualifiedID.length() );
							break;
						}
						if( base == null ) {
							throw new Exception( "Could not find static type matching type. " + e.getMessage(), e );
						}
					}
				} else {
					base = subExpressions.get( 0 );
					rest = children[2].toString();
				}
				ret = recursiveFieldAccess( base, rest );
			} else if( type.equals( "MemberAccessInstance" ) ) {
				ret = subExpressions.get( 0 );
			} else if( type.equals( "ClassInstanceCreationExpression" ) ) {
				JoosType jClass = m_env.lookup( children[1].toString());
				if( jClass.asObjectType() != null &&jClass.asObjectType().asJoosClass()!=null ){
					ret = new NewObject( m_class, jClass.asObjectType().asJoosClass(), subExpressions );
				} else {
					throw new NoTypeException( "type" + jClass + " is not an Object type or a Class" );
				}
			} else if( type.equals( "ArrayCreationExpression" ) ) {
				JoosType arrayType = m_env.lookup( children[1].toString() );
				if( children.length == 5 ) {
					ret = new NewExpression.NewArray( arrayType, subExpressions.get( 0 ) );
				} else {
					ret = new NewExpression.NewArray( arrayType, subExpressions );
				}
			} else if( type.equals( "OptArguments" ) || type.equals( "Arguments" ) ) {
				// Hack to allow multiple expressions to propagate up (flattening of lists)
				m_exprLists.peek().addAll( subExpressions );
			}
		} catch( Exception e ) {
			throw new HandlerException( "CompilationError in: " + m_method + "\nIn expression: " + node , e );
		}
		return ret;
	}
	
	public Expression getResult()
	{
		if( m_result == null ) {
			throw new RuntimeException( "ExpressionBuilder returned result of null for expression" );
		}
		return m_result;
	}
	
	// For skipping nodes which were hacked into the cfg
	private boolean skipNode( ParseTreeNode node )
	{
		String type = node.getType();
		if( m_inCastExpression && (type.equals( "Expression" ) || type.equals( "MemberAccess" )) ) {
			return true;
		} else if( m_isInMethod && type.equals( "MemberAccess" ) && node.toString().indexOf( '.' ) == -1 ) {
			m_isInMethod = false;
			m_exprLists.peek().add( new InvocationExpression.FieldInvocation( m_class, new This( m_class ), node.toString() ) );
			return true;
		}
		return false;
	}
	
	private Expression recursiveFieldAccess( Expression base, String nameString )
	{
		if( nameString != null ) {
			String[] names = nameString.split( "\\." );
			for( String name : names ) {
				base = new InvocationExpression.FieldInvocation( m_class, base, name );
			}
		}
		return base;
	}
	
	private Expression getStaticInstance( String qualifiedID )
	{
		return null;
	}
}


