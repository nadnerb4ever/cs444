package cs444.ast;

import java.util.*;

import cs444.ast.expression.*;
import cs444.ast.statement.*;
import cs444.parser.ParseTreeNode;
import cs444.parser.ParseTree.HandlerException;
import cs444.parser.ParseTree.NodeHandler;
import cs444.parser.ParseTree.ParseTreeTraverser;
import cs444.typeLinker.*;

public class ASTBuilder implements NodeHandler
{
	public static boolean gm_debug = true;

	private Environment m_env;
	private JoosMethod m_method;
	
	private Stack<Block> m_blocks;
	
	public ASTBuilder( Environment env, JoosMethod method ){
		m_env = env;
		m_method = method;
		
		m_blocks = new Stack<Block>();
		// m_blocks.add( new Block( m_method.getScope() ) ); // root DO not put this here ... it is taken care of in addBlock()
	}
	
	public Statement getTree(){
		return m_blocks.peek();
	}

	@Override
	public boolean onEnterNode( ParseTreeNode node ) throws HandlerException
	{
		if( node.getType().equals( "Block" ) 
		 || node.getType().startsWith( "IfThen" )
		 || node.getType().startsWith( "WhileStatement" )
		 || node.getType().startsWith( "ForStatement" )) {
			addBlock( node );
		} else if( node.getType().equals( "Expression" )
				|| node.getType().equals( "ExpressionOpt" )
				|| node.getType().equals( "OptForInit" )
				|| node.getType().equals( "OptExpression" )
				|| node.getType().equals( "OptForUpdate" )
				|| node.getType().equals( "OptArguments" ) ) {
			return false; // traverse manually on these node
		}
		return true;
	}

	@Override
	public void onExitNode(ParseTreeNode node) throws HandlerException {
		/**
		 * Block LBRACE BlockStatementsOpt RBRACE
		 * BlockStatements BlockStatement
		 * BlockStatements BlockStatement BlockStatements
		 * IfThenStatement              IF LPAREN Expression RPAREN Statement
		 * IfThenElseStatement          IF LPAREN Expression RPAREN StatementNoShortIf ELSE Statement
		 * IfThenElseStatementNoShortIf IF LPAREN Expression RPAREN StatementNoShortIf ELSE StatementNoShortIf
		 * WhileStatement          WHILE LPAREN Expression RPAREN Statement
		 * WhileStatementNoShortIf WHILE LPAREN Expression RPAREN StatementNoShortIf
		 * ForStatement          FOR LPAREN OptForInit SEMI OptExpression SEMI OptForUpdate RPAREN Statement
		 * ForStatementNoShortIf FOR LPAREN OptForInit SEMI OptExpression SEMI OptForUpdate RPAREN StatementNoShortIf
		 * ReturnStatement RETURN ExpressionOpt SEMI
		 * EmptyStatement SEMI
		 * LocalVariableDeclarationStatement LocalVariableDeclaration SEMI
		 * ExpressionStatement StatementExpression SEMI
		 */
		Statement newStmt = null;
		ExpressionBuilder exprBuilder = new ExpressionBuilder( m_env, m_blocks.peek().getScope(), m_method.getSource().asJoosClass(), m_method );
		ParseTreeTraverser traverser = new ParseTreeTraverser( exprBuilder );

		try {
			if( node.getType().equals( "Block" )) {
				newStmt = m_blocks.pop();

			} else if( node.getType().equals( "IfThenStatement" )) {
				traverser.traverse( node.getChildren()[2] );
				newStmt = new IfStatement( exprBuilder.getResult(), m_blocks.pop() );

			} else if( node.getType().startsWith( "IfThenElseStatement" )) {
				traverser.traverse( node.getChildren()[2] );
				List<Statement> stmtList = m_blocks.pop().getStatementList();
				newStmt = new IfElseStatement( exprBuilder.getResult(), 
												stmtList.get(0),
												stmtList.get(1)	);

			} else if( node.getType().startsWith( "WhileStatement" )) {
				traverser.traverse( node.getChildren()[2] );
				newStmt = new WhileStatement( exprBuilder.getResult(), m_blocks.pop() );

			} else if ( node.getType().startsWith( "ForStatement" )) {
				ParseTreeNode[] children = node.getChildren();
				Expression init = getExpression( children[2], 2 );
				Expression expr = getExpression( children[4], 1 );
				Expression update = getExpression( children[6], 1 );
				/*
				traverser.traverse( children[2] );
				Expression init = exprBuilder.getResult();
				traverser.traverse( node.getChildren()[4] );
				Expression expr = exprBuilder.getResult();
				traverser.traverse( node.getChildren()[6] );
				Expression update = exprBuilder.getResult();
				newStmt = new ForStatement( (init != null? init : new EmptyExpression()), 
											(expr != null? expr : new EmptyExpression()), 
											(update != null? update : new EmptyExpression()), 
											m_blocks.pop() );
				*/
				newStmt = new ForStatement( init, expr, update, m_blocks.pop() );

			} else if( node.getType().startsWith( "ReturnStatement" ) ) {
				Expression retExpr = getExpression( node.child( 1 ), 1 );
				newStmt = new ReturnStatement( retExpr, m_method );

			} else if( node.getType().startsWith( "EmptyStatement" )) {
				newStmt = new EmptyStatement();

			} else if( node.getType().equals("ExpressionStatement") ) {
				traverser.traverse( node.getChildren()[0] );
				newStmt = new ExpressionStatement( exprBuilder.getResult() );
			} else if( node.getType().equals( "DeclStmtAssignmentExpression" ) ) {
				traverser.traverse( node );
				newStmt = new ExpressionStatement( exprBuilder.getResult() );
			} else {
				return;
			}
		
			if( m_blocks.size() != 0 ) {
				m_blocks.peek().addStatement( newStmt );
			} else {
				m_method.setBody( (Block)newStmt );
			}
		} catch( HandlerException he ) {
			if( gm_debug ) {
				System.err.println( "Exception caused by:\n" + node.inspect() );
			}
			throw he;
		}
	}

	private void addBlock( ParseTreeNode node )
	{
		Block block;
		if( m_blocks.size() == 0 ) {
			block = new Block( m_method.getScope() );
		} else {
			block = new Block( (JoosScope)node.annotation() );
		}
		m_blocks.push( block );
	}
	private Expression getExpression( ParseTreeNode node, int depth ) throws HandlerException
	{
		while( depth-- > 0 ) {
			ParseTreeNode[] children = node.getChildren();
			if( children.length == 0 ) {
				return new EmptyExpression();
			}
			node = children[0];
		}
		if( node.getType().equals( "LocalVariableDeclaration" ) ) {
			ParseTreeNode[] children = node.getChildren();
			node = children[children.length - 1];
		}
		ExpressionBuilder exprBuilder = new ExpressionBuilder( m_env, m_blocks.peek().getScope(), m_method.getSource().asJoosClass(), m_method );
		ParseTreeTraverser traverser = new ParseTreeTraverser( exprBuilder );
		traverser.traverse( node );
		return exprBuilder.getResult();
	}
	
	
	/**
	 * A NodeHandler for using at the top level of the tree. It creates and ASTBuilder for each method body encountered.
	 */
	public static class Helper implements NodeHandler
	{
		private Environment m_env;
		
		public Helper( Environment env )
		{
			m_env = env;
		}
		
		public boolean onEnterNode( ParseTreeNode node ) throws HandlerException
		{
			if( node.getType().equals( "Block" ) ) {
				JoosMethod method = (JoosMethod)node.annotation();
				ASTBuilder builder = new ASTBuilder( m_env, method );
				ParseTreeTraverser traverser = new ParseTreeTraverser( builder );
				traverser.traverse( node );
				return false;
			}
			return true;
		}
		public void onExitNode( ParseTreeNode node ) throws HandlerException { }
	}
}
