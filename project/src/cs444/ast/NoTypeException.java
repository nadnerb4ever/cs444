package cs444.ast;

import cs444.typeLinker.*;

public class NoTypeException extends Exception
{
	public NoTypeException()
	{
		super( "No Type Exception" );
	}
	
	public NoTypeException( String msg )
	{
		super( msg );
	}
	
	public NoTypeException(String msg, Throwable cause)
	{
		super( msg, cause );
	}

	public NoTypeException(Throwable cause)
	{
		super( cause );
	}
}

