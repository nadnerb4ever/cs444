package cs444.ast.expression;

import java.util.List;
import cs444.ast.NoTypeException;

import cs444.staticAnalyzer.*;
import cs444.typeLinker.JoosType;


public abstract class NewExpression implements Expression
{
	protected JoosType m_type;
	
	public NewExpression( JoosType type )
	{
		m_type = type;
	}
	public static class NewArray extends NewExpression
	{
		private Expression m_size;
		private List<Expression> m_exprList;
		
		public NewArray( JoosType type, List<Expression> exprList )
		{
			super( type );
			m_size = new ConstantExpression.IntLiteral( "" + exprList.size() );
			m_exprList = exprList;
		}
		public NewArray( JoosType type, Expression size )
		{
			super( type );
			m_size = size;
			m_exprList = null;
		}
		public JoosType getJoosType() throws NoTypeException
		{
			if( m_exprList != null ) {
				for( Expression expr : m_exprList ) {
					expr.getJoosType();
					// TODO check that the types match
				}
			}
			return m_type.arrayOf(); 
		}
		public String inspect()
		{
			return "new " + m_type.fullName() + "[" + m_size + "] { ... }";
		}
		@Override
		public void staticCheck( StaticContext context ) throws StaticAnalyzerException
		{
			if( m_exprList != null ) {
				for( Expression expr: m_exprList ){
					expr.staticCheck( context );
				}
			}
			m_size.staticCheck( context );
		}
		
	}
	
	@Override
	public void setInitialized( StaticContext context ) { /* Illegal initialization */ }
	
	public Object eval()
	{
		return UNDEFINED;
	}
	public String code(){
		return "";
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
	}
}
