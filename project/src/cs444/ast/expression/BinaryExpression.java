package cs444.ast.expression;

import cs444.staticAnalyzer.*;
import cs444.typeLinker.*;
import cs444.typeLinker.JoosPrimitiveType.JoosNumericType;
import cs444.typeLinker.JoosPrimitiveType.JoosIntegralType;
import cs444.ast.NoTypeException;
import cs444.ast.expression.ConstantExpression.FalseLiteral;
import cs444.ast.expression.ConstantExpression.TrueLiteral;
import cs444.codeGen.Label;


public abstract class BinaryExpression implements Expression
{
	protected Expression m_lvalue;
	protected Expression m_rvalue;
	
	protected JoosType m_ltype;
	protected JoosType m_rtype;

	public BinaryExpression(Expression lvalue, Expression rvalue)
	{
		m_lvalue = lvalue;
		m_rvalue = rvalue;
		m_ltype = null;
		m_rtype = null;
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
		m_lvalue.genCode( out );
		out.append( "push eax\n" );
		m_rvalue.genCode( out );
		out.append( "pop ebx\n" );
		genCombineCode( out );
	}
	
	public abstract void genCombineCode( Appendable out ) throws java.io.IOException;
	
	public JoosType ltype() throws NoTypeException
	{
		if( m_ltype == null ) {
			m_ltype = m_lvalue.getJoosType();
		}
		return m_ltype;
	}
	public JoosType rtype() throws NoTypeException
	{
		if( m_rtype == null ) {
			m_rtype = m_rvalue.getJoosType();
		}
		return m_rtype;
	}
	
	public void typeError() throws NoTypeException
	{
		throw new NoTypeException( "Invalid arguments to " + opName() + " operator: " + ltype() + ", " + rtype() );
	}
	public String inspect()
	{
		return m_lvalue.inspect() + " " + opName() + " " + m_rvalue.inspect();
	}
	public abstract String opName();
	
	// Arithmetic
	public static abstract class Numeric extends BinaryExpression
	{
		public Numeric( Expression operandA, Expression operandB )
		{
			super( operandA, operandB );
		}
		public JoosType getJoosType() throws NoTypeException
		{
			JoosPrimitiveType ljpt = ltype().asPrimitiveType();
			JoosPrimitiveType rjpt = rtype().asPrimitiveType();
			if( ljpt == null || rjpt == null ) {
				typeError();
			}
			JoosNumericType ljnt = ljpt.asNumericType();
			JoosNumericType rjnt = rjpt.asNumericType();
			if( ljnt == null || rjnt == null ) {
				typeError();
			}
			JoosIntegralType ljit = ljnt.asIntegralType();
			JoosIntegralType rjit = rjnt.asIntegralType();
			if( ljit == null || rjit == null ) {
				return JoosPrimitiveType.DOUBLE_TYPE;
			} else {
				return JoosPrimitiveType.INT_TYPE;
			}
		}
		public Object eval( Object left, Object right )
		{
			Number nleft = (Number)left;
			Number nright = (Number)right;
			try {
				if( (left instanceof Double) || (right instanceof Double) ) {
					return eval( nleft.doubleValue(), nright.doubleValue() );
				}
				return eval( nleft.intValue(), nright.intValue() );
			} catch( RuntimeException re ) {
				return UNDEFINED;
			}
		}
		public abstract Double eval( Double left, Double right );
		public abstract Integer eval( Integer left, Integer right );
	}

	public static class Plus extends Numeric
	{
		public Plus(Expression lvalue, Expression rvalue)
		{
			super(lvalue, rvalue);
		}
		public JoosType getJoosType() throws NoTypeException
		{
			if( ltype().equals( JoosClass.STRING_TYPE ) && !rtype().equals( JoosPrimitiveType.VOID_TYPE ) ||
			    rtype().equals( JoosClass.STRING_TYPE ) && !ltype().equals( JoosPrimitiveType.VOID_TYPE ) ) {
				return JoosClass.STRING_TYPE;
			} else {
				return super.getJoosType();
			}
		}
		public String opName()
		{
			return "+";
		}
		public Double eval( Double left, Double right )
		{
			return left + right;
		}
		public Integer eval( Integer left, Integer right )
		{
			return left + right;
		}
		public String code(){
			// TODO: String and character concatenation
			return m_lvalue.code() +
					"push eax\n" +
					m_rvalue.code() +
					"pop ebx\n" +
					"add eax, ebx\n";
		}
		// TODO: String and character concatenation
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			out.append( "add eax, ebx\n" );
		}
	}
	public static class Minus extends Numeric
	{
		public Minus(Expression lvalue, Expression rvalue)
		{
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return "-";
		}
		public Double eval( Double left, Double right )
		{
			return left - right;
		}
		public Integer eval( Integer left, Integer right )
		{
			return left - right;
		}
		public String code(){
			 return m_lvalue.code() +
					 "push eax\n" +
					 m_rvalue.code() +
					 "pop ebx\n" +
					 "sub ebx, eax\n" + 
					 "mov eax, ebx\n";
		}
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			out.append( "sub ebx, eax\n" );
			out.append( "mov eax, ebx\n" );
		}
	}
	public static class Mult extends Numeric
	{
		public Mult(Expression lvalue, Expression rvalue)
		{
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return "*";
		}
		public Double eval( Double left, Double right )
		{
			return left * right;
		}
		public Integer eval( Integer left, Integer right )
		{
			return left * right;
		}
		public String code(){
			// Since joosc does not support long type,
			// we ignore the "high part" of the result.
			
			return m_lvalue.code() +
					 "push eax\n" +
					 m_rvalue.code() +
					 "pop ebx\n" +
					 "imul eax, ebx\n";
		}
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			out.append( "imul eax, ebx\n" );
		}
	}
	public static class Div extends Numeric
	{
		public Div(Expression lvalue, Expression rvalue)
		{
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return "/";
		}
		public Double eval( Double left, Double right )
		{
			return left / right;
		}
		public Integer eval( Integer left, Integer right )
		{
			return left / right;
		}
		public String code(){
			// Since joosc does not support long type,
			// we ignore the "high part" of the result.
			int labelIndex = Label.getLabelIndex();
			return m_lvalue.code() +
					// Sign check
					// Set edx to 0 if eax is positive, -1 otherwise
					"mov edx, 0\n" +
					"cmp eax, 0\n" +
					"jge isPositive" + labelIndex + "\n" +
					"mov edx, -1\n" +
					"isPositive"+ labelIndex + ":\n" +
					// done sigh check
					"push eax\n" +
					m_rvalue.code() +
					"pop ebx\n" +
					"idiv ebx\n";
		}
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			int labelIndex = Label.getLabelIndex();
			// Sign check
			// Set edx to 0 if eax is positive, -1 otherwise
			out.append( "mov edx, 0\n" );
			out.append( "cmp ebx, 0\n" );
			out.append( "jge isPositive" + labelIndex + "\n" );
			out.append( "mov edx, -1\n" );
			out.append( "isPositive"+ labelIndex + ":\n" );
			// done sign check
			out.append( "idiv ebx\n" );
		}
	}
	public static class Remainder extends Numeric
	{
		public Remainder(Expression lvalue, Expression rvalue)
		{
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return "%";
		}
		public Double eval( Double left, Double right )
		{
			return left % right;
		}
		public Integer eval( Integer left, Integer right )
		{
			return left % right;
		}
		public String code(){
			// Since joosc does not support long type,
			// we ignore the "high part" of the result.
			int labelIndex = Label.getLabelIndex();
			return m_lvalue.code() +
					// Sign check
					// Set edx to 0 if eax is positive, -1 otherwise
					"mov edx, 0\n" +
					"cmp eax, 0\n" +
					"jge isPositive" + labelIndex + "\n" +
					"mov edx, -1\n" +
					"isPositive"+ labelIndex + ":\n" +
					// done sigh check
					"push eax\n" +
					m_rvalue.code() +
					"pop ebx\n" +
					"idiv ebx\n" +
					"mov eax, edx\n";
		}
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			int labelIndex = Label.getLabelIndex();
			// Sign check
			// Set edx to 0 if eax is positive, -1 otherwise
			out.append( "mov edx, 0\n" );
			out.append( "cmp ebx, 0\n" );
			out.append( "jge isPositive" + labelIndex + "\n" );
			out.append( "mov edx, -1\n" );
			out.append( "isPositive" + labelIndex + ":\n" );
			// done sign check
			out.append( "idiv ebx\n" );
			out.append( "mov eax, edx\n" );
		}
	}
	
	// Relational
	public static abstract class Comparison  extends BinaryExpression
	{
		public Comparison( Expression operandA, Expression operandB )
		{
			super( operandA, operandB );
		}
		
		public JoosType getJoosType() throws NoTypeException
		{
			JoosPrimitiveType ljpt = ltype().asPrimitiveType();
			JoosPrimitiveType rjpt = rtype().asPrimitiveType();
			if( ljpt == null || rjpt == null ) {
				typeError();
			}
			JoosNumericType ljnt = ljpt.asNumericType();
			JoosNumericType rjnt = rjpt.asNumericType();
			if( ljnt == null || rjnt == null ) {
				typeError();
			}
			return JoosPrimitiveType.BOOLEAN_TYPE;
		}
		public Boolean eval( Object left, Object right )
		{
			Number nleft = (Number)left;
			Number nright = (Number)right;
			if( (left instanceof Double) || (right instanceof Double) ) {
				return eval( nleft.doubleValue(), nright.doubleValue() );
			}
			return eval( nleft.intValue(), nright.intValue() );
		}
		public abstract Boolean eval( Double left, Double right );
		public abstract Boolean eval( Integer left, Integer right );
	}
	public static abstract class EqualityComparison extends BinaryExpression
	{
		public EqualityComparison( Expression operandA, Expression operandB )
		{
			super( operandA, operandB );
		}
		public JoosType getJoosType() throws NoTypeException
		{
			if( !ltype().isSuperType( rtype() ) && !rtype().isSuperType( ltype() ) ) {
				typeError();
			}
			return JoosPrimitiveType.BOOLEAN_TYPE;
		}

	}
	
	public static class LessThan extends Comparison {
		public LessThan(Expression lvalue, Expression rvalue){
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return "<";
		}
		public Boolean eval( Double left, Double right )
		{
			return left < right;
		}
		public Boolean eval( Integer left, Integer right )
		{
			return left < right;
		}
		public String code(){
			int labelIndex = Label.getLabelIndex();
			return m_lvalue.code() +
					 "push eax\n" +
					 m_rvalue.code() +
					 "pop ebx\n" +
					 // true if eax > ebx
					 "cmp eax, ebx\n" +
					 "mov eax, 1\n" +
					 "jg isTrue" + labelIndex + "\n" +
					 "mov eax, 0\n" +
					 "isTrue" + labelIndex + ":\n";
		}
		
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			int labelIndex = Label.getLabelIndex();
			// Sign check
			// Set edx to 0 if eax is positive, -1 otherwise
			out.append( "cmp eax, ebx\n" );
			out.append( "mov eax, 1\n" );
			out.append( "jge isTrue" + labelIndex + "\n" );
			out.append( "mov edx, 0\n" );
			out.append( "isTrue"+ labelIndex + ":\n" );
		}
	}
	public static class LessEqual extends Comparison {
		public LessEqual(Expression lvalue, Expression rvalue) {
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return "<=";
		}
		public Boolean eval( Double left, Double right )
		{
			return left <= right;
		}
		public Boolean eval( Integer left, Integer right )
		{
			return left <= right;
		}
		public String code(){
			int labelIndex = Label.getLabelIndex();
			return m_lvalue.code() +
					 "push eax\n" +
					 m_rvalue.code() +
					 "pop ebx\n" +
					 // true if eax >= ebx
					 "cmp eax, ebx\n" +
					 "mov eax, 1\n" +
					 "jge isTrue" + labelIndex + "\n" +
					 "mov eax, 0\n" +
					 "isTrue" + labelIndex + ":\n";
		}
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			int labelIndex = Label.getLabelIndex();
			// Sign check
			// Set edx to 0 if eax is positive, -1 otherwise
			out.append( "cmp eax, ebx\n" );
			out.append( "mov eax, 1\n" );
			out.append( "jge isTrue" + labelIndex + "\n" );
			out.append( "mov edx, 0\n" );
			out.append( "isTrue"+ labelIndex + ":\n" );
		}
	}
	public static class GreaterThan extends Comparison {
		public GreaterThan(Expression lvalue, Expression rvalue) {
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return ">";
		}
		public Boolean eval( Double left, Double right )
		{
			return left > right;
		}
		public Boolean eval( Integer left, Integer right )
		{
			return left > right;
		}
		public String code(){
			int labelIndex = Label.getLabelIndex();
			return m_lvalue.code() +
					 "push eax\n" +
					 m_rvalue.code() +
					 "pop ebx\n" +
					 // true if eax < ebx
					 "cmp ebx, eax\n" +
					 "mov eax, 1\n" +
					 "jg isTrue" + labelIndex + "\n" +
					 "mov eax, 0\n" +
					 "isTrue" + labelIndex + ":\n";
		}
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			int labelIndex = Label.getLabelIndex();
			// Sign check
			// Set edx to 0 if eax is positive, -1 otherwise
			out.append( "cmp ebx, eax\n" );
			out.append( "mov eax, 1\n" );
			out.append( "jge isTrue" + labelIndex + "\n" );
			out.append( "mov eax, 0\n" );
			out.append( "isTrue"+ labelIndex + ":\n" );
		}
	}
	public static class GreaterEqual extends Comparison {
		public GreaterEqual(Expression lvalue, Expression rvalue) {
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return ">=";
		}
		public Boolean eval( Double left, Double right )
		{
			return left >= right;
		}
		public Boolean eval( Integer left, Integer right )
		{
			return left >= right;
		}
		public String code(){
			int labelIndex = Label.getLabelIndex();
			return m_lvalue.code() +
					 "push eax\n" +
					 m_rvalue.code() +
					 "pop ebx\n" +
					 // true if eax <= ebx
					 "cmp ebx, eax\n" +
					 "mov eax, 1\n" +
					 "jge isTrue" + labelIndex + "\n" +
					 "mov eax, 0\n" +
					 "isTrue" + labelIndex + ":\n";
		}
		
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			int labelIndex = Label.getLabelIndex();
			// Sign check
			// Set edx to 0 if eax is positive, -1 otherwise
			out.append( "cmp ebx, eax\n" );
			out.append( "mov eax, 1\n" );
			out.append( "jge isTrue" + labelIndex + "\n" );
			out.append( "mov eax, 0\n" );
			out.append( "isTrue"+ labelIndex + ":\n" );
		}
	}
	public static class Equal extends EqualityComparison {
		public Equal(Expression lvalue, Expression rvalue) {
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return "==";
		}
		public Boolean eval( Object left, Object right )
		{
			return left.equals( right );
		}
		public String code(){
			int labelIndex = Label.getLabelIndex();
			return m_lvalue.code() +
					 "push eax\n" +
					 m_rvalue.code() +
					 "pop ebx\n" +
					 "cmp eax, ebx\n" +
					 "mov eax, 1\n" +
					 "je isTrue" + labelIndex + "\n" +
					 "mov eax, 0\n" +
					 "isTrue" + labelIndex + ":\n";
		}
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			int labelIndex = Label.getLabelIndex();
			// Sign check
			// Set edx to 0 if eax is positive, -1 otherwise
			out.append( "cmp eax, ebx\n" );
			out.append( "mov eax, 1\n" );
			out.append( "je isTrue" + labelIndex + "\n" );
			out.append( "mov eax, 0\n" );
			out.append( "isTrue"+ labelIndex + ":\n" );
		}
	}
	public static class NotEqual extends EqualityComparison {
		public NotEqual(Expression lvalue, Expression rvalue) {
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return "!=";
		}
		public Boolean eval( Object left, Object right )
		{
			return !left.equals( right );
		}
		public String code(){
			int labelIndex = Label.getLabelIndex();
			return m_lvalue.code() +
					 "push eax\n" +
					 m_rvalue.code() +
					 "pop ebx\n" +
					 "cmp eax, ebx\n" +
					 "mov eax, 1\n" +
					 "jne isTrue" + labelIndex + "\n" +
					 "mov eax, 0\n" +
					 "isTrue" + labelIndex + ":\n";
		}
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			int labelIndex = Label.getLabelIndex();
			// Sign check
			// Set edx to 0 if eax is positive, -1 otherwise
			out.append( "cmp eax, ebx\n" );
			out.append( "mov eax, 1\n" );
			out.append( "jne isTrue" + labelIndex + "\n" );
			out.append( "mov eax, 0\n" );
			out.append( "isTrue"+ labelIndex + ":\n" );
		}
	}
	
	// boolean
	
	public static abstract class BooleanExpr extends BinaryExpression
	{
		public BooleanExpr( Expression operandA, Expression operandB )
		{
			super( operandA, operandB );
		}
		public JoosType getJoosType() throws NoTypeException
		{
			if( !ltype().equals( JoosPrimitiveType.BOOLEAN_TYPE ) ||
			    !rtype().equals( JoosPrimitiveType.BOOLEAN_TYPE ) ) {
			  typeError();
			}
			return JoosPrimitiveType.BOOLEAN_TYPE;
		}
		public Boolean eval( Object left, Object right )
		{
			return eval( (Boolean)left, (Boolean)right );
		}
		public abstract Boolean eval( Boolean left, Boolean right );
	}
	public static class And extends BooleanExpr {
		public And(Expression lvalue, Expression rvalue) {
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return "&&";
		}
		public Boolean eval( Boolean left, Boolean right )
		{
			return left && right;
		}
		public String code(){
			return m_lvalue.code() +
					 "push eax\n" +
					 m_rvalue.code() +
					 "pop ebx\n" +
					 "and eax, ebx\n";
		}
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			out.append( "and eax, ebx\n" );
		}
	}
	public static class Or extends BooleanExpr {
		public Or(Expression lvalue, Expression rvalue) {
			super(lvalue, rvalue);
		}
		public String opName()
		{
			return "||";
		}
		/*
		public Expression eval(){
			Expression lvalue = m_lvalue.eval();
			Expression rvalue = m_rvalue.eval();
			if( lvalue instanceof TrueLiteral || lvalue instanceof FalseLiteral 
			 && rvalue instanceof TrueLiteral || rvalue instanceof FalseLiteral ){
				if( lvalue instanceof TrueLiteral || rvalue instanceof TrueLiteral ){
					return new TrueLiteral();
				}
				return new FalseLiteral();
			}
			return this;
		}
		*/
		public Boolean eval( Boolean left, Boolean right )
		{
			return left || right;
		}
		public String code(){
			return m_lvalue.code() +
					 "push eax\n" +
					 m_rvalue.code() +
					 "pop ebx\n" +
					 "or eax, ebx\n";
		}
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			out.append( "or eax, ebx\n" );
		}
	}
	// Assign
	public static class Assign extends BinaryExpression {
		public Assign(Expression lvalue, Expression rvalue) {
			super(lvalue, rvalue);
		}
		public JoosType getJoosType() throws NoTypeException
		{
			if( !ltype().isSuperType( rtype() ) ) {
				typeError();
			}
			return ltype();
		}
		public String opName()
		{
			return "=";
		}
		public void staticCheck( StaticContext context ) throws StaticAnalyzerException
		{
			m_rvalue.staticCheck( context );
			m_lvalue.setInitialized( context );
			m_lvalue.staticCheck( context );
		}
		/*
		public void setInitialized() {
			m_lvalue.setInitialized();
		}
		*/
		public Object eval( Object left, Object right )
		{
			throw new RuntimeException( "eval( Object, Object ) should never be called for BinaryExpression.Assign" );
		}
		public String code()
		{
			return null;
		/*
			return ((InvocationExpression)m_lvalue).addr() +
					"push eax\n" +
					m_rvalue.code() +
					"pop ebx\n" +
					"mov eax, [ebx]\n";
		*/
		}
		public void genCombineCode( Appendable out ) throws java.io.IOException
		{
			out.append( "mov eax, [ebx]\n" );
		}
	}
	
	public void staticCheck( StaticContext context ) throws StaticAnalyzerException
	{
		m_lvalue.staticCheck( context );
		m_rvalue.staticCheck( context );
	}
	public void setInitialized( StaticContext context ) {
		/* Illegal initialization and prohibited by the grammar
		 * except for Assign class
		 */
	}
	
	public Object eval()
	{
		Object lresult = m_lvalue.eval();
		Object rresult = m_rvalue.eval();
		if( lresult.equals( UNDEFINED ) || rresult.equals( UNDEFINED ) ) {
			return UNDEFINED;
		}
		return eval( lresult, rresult );
	}
	public abstract Object eval( Object left, Object right );
	public abstract String code();
	
}
