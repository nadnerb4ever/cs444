package cs444.ast.expression;

import java.util.ArrayList;
import java.util.List;

import cs444.ast.NoTypeException;
import cs444.ast.expression.InvocationExpression.MethodInvocation;
import cs444.staticAnalyzer.*;
import cs444.typeLinker.*;

public class NewObject implements Expression 
{
	MethodInvocation m_method;
	JoosClass m_type;
	public NewObject( JoosClass context, JoosClass type, List<Expression> parameters )
	{
		m_type = type;
		Expression id = new NewInstance( m_type  );
		m_method = new MethodInvocation( context, id, m_type.shortName(), parameters );
		
	}
	public JoosType getJoosType() throws NoTypeException
	{ 
		m_method.getJoosType();
		return m_type;
	}
	public String inspect()
	{
		return "new " + m_method.inspect();
	}
	
	public class NewInstance implements Expression
	{
		private JoosClass m_class;

		public NewInstance( JoosClass jClass )
		{
			m_class = jClass;
		}
		public JoosType getJoosType()
		{
			return m_class; 
		}
		public String inspect()
		{
			return m_class.fullName();
		}
		@Override
		public void staticCheck( StaticContext context ) throws StaticAnalyzerException {}
		@Override
		public void setInitialized( StaticContext context ) {}
		@Override
		public Expression eval(){
			return null;
		}
		public String code(){
			return "";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
		}
	}
	
	@Override
	public void staticCheck( StaticContext context ) throws StaticAnalyzerException {
			m_method.staticCheck( context );
	}
	public void setInitialized( StaticContext context ) {}
	
	public Object eval()
	{
		return UNDEFINED;
	}
	public String code(){
		return "";
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
	}
}
