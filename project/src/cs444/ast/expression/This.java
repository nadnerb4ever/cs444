package cs444.ast.expression;

import cs444.staticAnalyzer.*;
import cs444.typeLinker.JoosType;
import cs444.typeLinker.JoosClass;


public class This implements Expression
{
	private JoosClass m_class;

	public This( JoosClass jClass )
	{
		m_class = jClass;
	}
	public JoosType getJoosType()
	{
		return m_class; 
	}
	public String inspect()
	{
		return "this";
	}
	public void staticCheck( StaticContext context ) throws StaticAnalyzerException {}
	public void setInitialized( StaticContext context ) {}
	
	public Object eval()
	{
		return UNDEFINED;
	}
	public String code()
	{
		return "";
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
		out.append( "mov eax, [ebp + 8]\n" );
	}
}
