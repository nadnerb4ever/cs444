package cs444.ast.expression;

import java.util.ArrayList;
import java.util.List;
import cs444.ast.NoTypeException;
import cs444.staticAnalyzer.*;
import cs444.typeLinker.*;
import java.util.*;

import cs444.typeLinker.JoosType;
import cs444.typeLinker.JoosMember;
import cs444.utils.Code;


public abstract class InvocationExpression implements Expression
{
	protected Expression m_id;
	
	public InvocationExpression( Expression id )
	{
		m_id = id;
	}
	
	public Expression getSubExpression()
	{
		return m_id;
	}
	
	public void genCode( Appendable out ) throws java.io.IOException
	{
		String label = Code.uniqueLabel( "NoError" );
		m_id.genCode( out );
		out.append( "cmp eax, 0x0\n" );
		out.append( "jne " + label + "\n" );
		out.append( "call __exception\n" );
		out.append( label + ":\n" );
	}
	
	public static class ArrayInvocation extends InvocationExpression
	{
		private Expression m_index;
		public ArrayInvocation( Expression id, Expression index )
		{
			super( id );
			m_index = index;
		}
		public JoosType getJoosType() throws NoTypeException
		{
			m_index.getJoosType(); // TODO check that this is an integral type
			if( m_id.getJoosType().asArrayType() != null ){
				return m_id.getJoosType().asArrayType().getInnerType();
			} else {
				throw new NoTypeException( m_id.getJoosType() + " is not of ArrayType" );
			}
		}
		public String inspect()
		{
			return m_id.inspect() + "[" + m_index.inspect() + "]";
		}
		public void staticCheck( StaticContext context ) throws StaticAnalyzerException{
			super.staticCheck( context );
			m_index.staticCheck( context );
		}
		public String code(){
			return null;
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			String noErrorLabel = Code.uniqueLabel( "NoError" );
			// Get the array pointer
			super.genCode( out );
			out.append( "push eax\n" );
			// Get the index
			m_index.genCode( out );
			// Restore the array pointer
			out.append( "pop ebx\n" );
			// Length check
			out.append( "mov ecx, [ebx]\n" );
			out.append( "cmp eax, ecx\n" );
			out.append( "jl " + noErrorLabel + "\n" );
			out.append( "call __exception\n" );
			out.append( noErrorLabel + ":\n" );
			// Get the value from the array
			out.append( "add eax, ebx\n" );
			out.append( "mov eax, [eax]\n" );
		}
	}
	public static abstract class MemberInvocation extends InvocationExpression
	{
		protected JoosClass m_context;
		protected String m_name;
		
		protected MemberInvocation( JoosClass context, Expression instance, String name )
		{
			super( instance );
			m_context = context;
			m_name = name;
		}
		public JoosClass getContext()
		{
			return m_context;
		}
		public void filter( List<? extends JoosMember> members )
		{
			Iterator<? extends JoosMember> it = members.iterator();
			while( it.hasNext() ) {
				JoosMember member = it.next();
				if( !isAccessible( member ) ) {
					it.remove();
				}
			}
		}
		public boolean isAccessible( JoosMember member )
		{
			if( member.getPermission() == JoosMember.PERMISSION_PRIVATE ) {
				// private is only accessible from same class
				if( !member.getSource().equals( m_context ) ) {
					return false;
				}
			} else if( member.getPermission() == JoosMember.PERMISSION_PROTECTED ) {
				// protected is accessible from child class or same package
				JoosObjectType source = member.getSource();
				if( source.isSuperType( m_context ) ) {
					return true;
				} else if( source.packageName() == null ) {
					return m_context.packageName() == null;
				} else {
					return source.packageName().equals( m_context.packageName() );
				}
			}
			// public is accessible everywhere
			return true;
		}
	}
	public static class MethodInvocation extends MemberInvocation
	{
		private List<Expression> m_args;
		
		private List<JoosType> m_argTypes;
		private JoosMethod m_joosMethod;
		
		public MethodInvocation( JoosClass context, Expression id, String name, List<Expression> args ) {
			super( context, id, name );
			m_args = args;
			m_argTypes = null;
			m_joosMethod = null;
		}
		public JoosType getJoosType() throws NoTypeException
		{
			if( m_joosMethod == null ) {
				JoosObjectType objectType = m_id.getJoosType().asObjectType();
				JoosStaticType staticType = m_id.getJoosType().asStaticType();
				
				
				if( objectType != null ) {
					computeJoosMethod( objectType, false );
				} else if( staticType != null ) {
					objectType = staticType.getInnerType();
					computeJoosMethod( objectType, true );
				} else {
					throw new NoTypeException( "expression " + m_id + " is not an object");
				}
			}
			return getJoosMethod().getReturnType();
		}
		private void computeJoosMethod( JoosObjectType objectType, boolean staticMethods ) throws NoTypeException
		{
			List<JoosMethod> candidates = objectType.getMethods( m_name, getArgumentTypes(), staticMethods );
			// Filter out the candidates which are not accessible
			filter( candidates );
			// Now get the most specific candidate (we could do this in O(n) time but the O(n^2) algorithm is more robust and easier to report errors with
			List<JoosMethod> optimal = new ArrayList<JoosMethod>();
outerLoop:
			for( JoosMethod candidate : candidates ) {
				for( JoosMethod other : candidates ) {
					if( other.isMoreSpecific( candidate ) ) {
						continue outerLoop;
					}
				}
				optimal.add( candidate );
			}
			if( optimal.size() == 0 ) {
				throw new NoTypeException( "There were no " + (staticMethods ? "static " : "") + "methods matching the signature " +
				                           m_name + " " + getArgumentTypes() + " in " + objectType.fullName() );
			} else if( optimal.size() > 1 ) {
				throw new NoTypeException( "There were " + optimal.size() + (staticMethods ? " static" : "") +
				                           " methods matching the signature " + m_name + " " + getArgumentTypes() +
				                           " in " + objectType.fullName() + ":\n\t" + optimal );
			}
			m_joosMethod = optimal.get( 0 );
			if( m_joosMethod == null ) {
				throw new RuntimeException( "m_joosMethod is null" );
			}
		}
		
		public JoosMethod getJoosMethod()
		{
			return m_joosMethod;
		}
		
		private Collection<JoosType> getArgumentTypes() throws NoTypeException
		{
			if( m_argTypes == null ) {
				m_argTypes = new ArrayList<JoosType>();
				for( Expression expr : m_args ) {
					m_argTypes.add( expr.getJoosType() );
				}
			}
			return m_argTypes;
		}
		public String inspect()
		{
			StringBuilder builder = new StringBuilder();
			builder.append( m_id.inspect() + "." + m_name + "(" );
			if( m_args.size() != 0 ) {
				builder.append( " " + m_args.get( 0 ).inspect() );
				for( int i = 1; i < m_args.size(); i++ ) {
					builder.append( ", " + m_args.get( i ).inspect() );
				}
				builder.append( " " );
			}
			builder.append( ")" );
			return builder.toString();
		}
		
		public void staticCheck( StaticContext context ) throws StaticAnalyzerException{
			super.staticCheck( context );
			for( Expression expr: m_args ){
				expr.staticCheck( context );
			} 
		}
		public String code(){
			return null;
			/*
			String code = addr() + // get the address to the method implementation
					"push eax\n";
			
			// push the arguments to the stack
			int offset = 0;
			for( Expression arg: m_args ){
				offset += 4;
				code += arg.code() +
						"push eax\n";
			}
			
			// Get the address to the object back from the stack
			code += "mov eax, [esp + "+ offset +"]\n" +
					"call eax\n" +
					"add esp, " + (offset + 4) + "\n";
			return code;
			*/	
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			for( Expression arg : m_args ) {
				arg.genCode( out );
				out.append( "push eax\n" );
			}
			
			if( m_joosMethod.isStatic() ) {
				out.append( "call " + Code.makeLabel( m_joosMethod ) + "\n" );
			} else {
				super.genCode( out );
				// Push our object instance
				out.append( "push eax\n" );
				
				// Find the correct jump table in our vtable
				JoosObjectType caller = m_joosMethod.getSource();
				out.append( "mov ebx, " + Code.toHex( caller.getIdentifier() ) + "\n" );
				out.append( "imul ebx, 4\n" );
				out.append( "add ebx, eax\n" );
				out.append( "mov ebx, [ebx + 4]\n" );
				
				// Find the correct function address in our jump table
				out.append( "mov ecx, " + Code.toHex( caller.cardinality( m_joosMethod ) ) + "\n" );
				out.append( "imul ecx, 4\n" );
				out.append( "add ecx, ebx\n" );
				out.append( "mov ecx, [ecx]\n" );
				
				// Jump to the given address
				out.append( "call ecx\n" );
				
				/*
				//-- Temp work --//
				// Push the bottom of our binary search (+12)
				out.append( "push 0\n" );
				// Push what will be the middle of our binary search (+8)
				out.append( "push 0\n" );
				// Push the top of our binary search (+4)
				out.append( "mov ebx, [eax + 4]\n" );
				out.append( "push ebx\n" );
				// Push the identifier of the class that we are declared as (+0)
				JoosObjectType caller = m_joosMethod.getSource();
				out.append( "mov ebx, " + Code.toHex( caller.getIdentifier() ) + "\n" );
				out.append( "push ebx\n" );
				
				// Perform the binary search to find a value matching ebx
				String topLabel = Code.uniqueLabel( "loopTop" );
				String endLabel = Code.uniqueLabel( "loopEnd" );
				String lessThanLabel = Code.uniqueLabel( "ifLessThan" );
				
				out.append( topLabel + ":\n" );
				// Get our current location to look at: (bot + top)/2
				out.append( "mov ecx, [esp + 12]\n" ); // Bot (smaller numbers)
				out.append( "mov edx, [esp + 4]\n" ); // Top (larger numbers)
				out.append( "add ecx, edx\n" );
				out.append( "idiv ecx, 2\n" );
				out.append( "mov [esp + 8], ecx\n" );
				// Turn the current index into an address
				out.append( "imul ecx, 8\n" );
				out.append( "add ecx, eax\n" );
				// Compare
				out.append( "cmp [ecx], [esp]\n" );
				out.append( "je " + endLabel + "\n" );
				out.append( "jl " + lessThanLabel + "\n" );
				
				out.append( "mov [esp + 4], [esp + 8]\n" );
				out.append( "jmp " + topLabel + "\n" );
				
				out.append( lessThanLabel + ":\n" );
				out.append( "mov [esp + 12], [esp + 8]\n" );
				out.append( "jmp " + topLabel + "\n" );
				
				out.append( endLabel + ":\n" );
				
				// Now we have the location of the jump table
				*/
			}
			
			
			/*
			out.append( addr() + // get the address to the method implementation
					"push eax\n" );
					
			int offset = 0;
			for( Expression arg: m_args ){
				offset += 4;
				out.append( arg.code() +
						"push eax\n" );
			}
			
			// Get the address to the object back from the stack
			out.append( "mov eax, [esp + "+ offset +"]\n" +
					"call eax\n" +
					"add esp, " + (offset + 4) + "\n");
			*/
		}
	}
	
	public static class FieldInvocation extends MemberInvocation
	{
		public FieldInvocation( JoosClass context, Expression instance, String fieldName )
		{
			super( context, instance, fieldName );
		}
		public JoosType getJoosType() throws NoTypeException
		{
			JoosObjectType objectType = m_id.getJoosType().asObjectType();
			JoosStaticType staticType = m_id.getJoosType().asStaticType();
			JoosArrayType arrayType = m_id.getJoosType().asArrayType();
			JoosType ret = null;
			
			if( objectType != null ){
				JoosField field = objectType.asJoosClass().getField( m_name );
				if( !isAccessible( field ) ) {
					throw new NoTypeException( "That field cannot be accessed from this context" );
				}
				ret = field.getType();
			} else if( staticType != null ) {
				JoosField field = staticType.getInnerType().asJoosClass().getField( m_name );
				if( !isAccessible( field ) ) {
					throw new NoTypeException( "That field cannot be accessed from this context" );
				} else {
					System.err.println( field + " is accessible from static context in " + m_context );
				}
				ret = field.getType();
			} else if( arrayType != null ) {
				if( m_name.equals( "length" ) ){
					ret = JoosPrimitiveType.INT_TYPE; 
				} else {
				throw new NoTypeException( "invalid field name for ArrayType" );
				}
			} else {
				throw new NoTypeException( "invalid JoosType" );
			}
			if( ret != null ) {
				return ret;
			} else {
				throw new NoTypeException( "no field found" );
			}
		}
		
		public String getFieldName()
		{
			return m_name;
		}
		public String inspect()
		{
			return m_id.inspect() + "." + m_name;
		}
	}
	public void staticCheck( StaticContext context ) throws StaticAnalyzerException
	{
		m_id.staticCheck( context );
	}
	public void setInitialized( StaticContext context ) { }
	
	public Object eval()
	{
		return UNDEFINED;
	}
	public String code(){
		return null;
	}
}
