package cs444.ast.expression;

import cs444.ast.NoTypeException;

import cs444.staticAnalyzer.*;
import cs444.typeLinker.JoosType;
import cs444.typeLinker.JoosClass;


public class StaticInstance implements Expression
{
	JoosClass m_class;
	
	public StaticInstance( JoosClass jClass )
	{
		m_class = jClass;
	}
	
	public JoosType getJoosType() throws NoTypeException
	{
		return m_class.staticTypeOf();
	}
	
	public String inspect()
	{
		return m_class.fullName();
	}

	public void staticCheck( StaticContext context ) throws StaticAnalyzerException {}
	public void setInitialized( StaticContext context ) {}
	public Object eval()
	{
		return UNDEFINED;
	}
	public String code(){
		return "";
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
	}
}


