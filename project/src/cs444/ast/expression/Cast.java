package cs444.ast.expression;

import cs444.staticAnalyzer.*;
import cs444.typeLinker.*;
import cs444.ast.NoTypeException;

public class Cast implements Expression
{
	JoosType m_type;
	Expression m_expr;
	JoosType m_exprType = null;
	
	public Cast( JoosType type, Expression expr ){
		m_type = type;
		m_expr = expr;
	}
	public JoosType getType(){ return m_type; }
	public Expression getExpr(){ return m_expr; }
	boolean isUpcast() throws NoTypeException
	{
		JoosType exprType = expressionType();
		return m_type.isSuperType( exprType );
	}
	
	private JoosType expressionType() throws NoTypeException
	{
		if( m_exprType == null ){
			m_exprType = m_expr.getJoosType();
		}
		return m_exprType;
	}
	
	boolean isDowncast() throws NoTypeException
	{
		JoosType exprType = expressionType();
		return exprType.isSuperType( m_type );
	}
	
	boolean isFinal() throws NoTypeException
	{
		return expressionType().asObjectType().isFinal(); 
	}
	
	boolean isInterface() throws NoTypeException
	{
		return (m_type.asObjectType().asJoosInterface()!=null); // throwing NullPointerException
	}
	
	public JoosType getJoosType() throws NoTypeException
	{
		if( isDowncast() || isUpcast() ) {
			return m_type;
		} else {
			JoosObjectType joExprType = expressionType().asObjectType();
			JoosObjectType joCastType = m_type.asObjectType();
			if( joExprType == null || joCastType == null ) {
				throw new NoTypeException( "Cannot cast type " + joExprType + " to type " + joCastType );
			} else if( joExprType.asJoosInterface() != null && joCastType.isFinal()){
				return m_type;
			} else if( joCastType.asJoosInterface() != null && joExprType.isFinal()){
				return m_type;
			} else {
				throw new NoTypeException( "Cannot cast type " + joExprType + " to type " + joCastType );
			}
		}
	}
	public String inspect()
	{
		return "(" + m_type.fullName() + ")" + m_expr.inspect();
	}
	public void staticCheck( StaticContext context ) throws StaticAnalyzerException
	{
		m_expr.staticCheck( context );
	}
	public void setInitialized( StaticContext context ) {
		/* Illegal initialization */
	}
	public Object eval()
	{
		return m_expr.eval();
	}
	public String code(){
		return "";
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
	}
}
