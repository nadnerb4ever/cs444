package cs444.ast.expression;

import cs444.staticAnalyzer.*;
import cs444.typeLinker.*;
import cs444.ast.NoTypeException;
import cs444.ast.expression.ConstantExpression.TrueLiteral;
import cs444.ast.expression.ConstantExpression.FalseLiteral;

public abstract class UnaryExpression implements Expression{
	protected Expression m_operand;
	
	public UnaryExpression( Expression expr ){
		m_operand = expr;
	}
	
	public JoosType getJoosType() throws NoTypeException
	{
		JoosType type = m_operand.getJoosType();
		JoosType[] allowed = allowableTypes();
		for( int i = 0; i< allowed.length; i++){
			if( allowed[i].equals( type ) ){
				return type;
			}
		}
		throw new NoTypeException("No type " + type.fullName() + " allowed ");
	}
	
	protected abstract JoosType[] allowableTypes();
	
	protected static final JoosType[] MINUS_TYPES = {
	  JoosPrimitiveType.INT_TYPE,
	  JoosPrimitiveType.BYTE_TYPE,
	  JoosPrimitiveType.FLOAT_TYPE,
	  JoosPrimitiveType.SHORT_TYPE
	};	
	public static class Negative extends UnaryExpression
	{
		public Negative( Expression operand )
		{
			super( operand );
		}
		public JoosType[] allowableTypes()
		{
			return MINUS_TYPES;
		}
		public String inspect()
		{
			return "-" + m_operand.inspect();
		}
		public Object eval()
		{
			Object val = m_operand.eval();
			if( val.equals( UNDEFINED ) ) {
				return UNDEFINED;
			}
			Number nval = (Number)val;
			if( nval instanceof Double ) {
				return new Double( -nval.doubleValue() );
			}
			return new Integer( -nval.intValue() );
		}
		public String code(){
			return m_operand.code() +
					"neg eax\n";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			out.append( m_operand.code() +
					"neg eax\n" );
		}
	}
	
	protected static final JoosType[] NOT_TYPES = { JoosPrimitiveType.BOOLEAN_TYPE };
	public static class Not extends UnaryExpression {
		public Not( Expression operand )
		{
			super( operand );
		}
		public JoosType[] allowableTypes()
		{
			return NOT_TYPES;
		}
		public String inspect()
		{
			return "!" + m_operand.inspect();
		}
		/*
		public Expression eval(){
			Expression expr = m_operand.eval();
			if( expr instanceof TrueLiteral ){
				return new FalseLiteral();
			} else if ( expr instanceof FalseLiteral ){
				return new TrueLiteral();
			}
			return this;
		}
		*/
		public Object eval()
		{
			Object val = m_operand.eval();
			if( val.equals( UNDEFINED ) ) {
				return UNDEFINED;
			}
			return new Boolean( !(Boolean)val );
		}
		public String code(){
			return m_operand.code() +
					"not eax\n";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			out.append( m_operand.code() +
					"not eax\n" );
		}
	}
	public void staticCheck( StaticContext context ) throws StaticAnalyzerException
	{
		m_operand.staticCheck( context );
	}
	public void setInitialized( StaticContext context ) {}
}
