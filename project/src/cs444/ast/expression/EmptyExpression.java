package cs444.ast.expression;

import cs444.staticAnalyzer.*;
import cs444.typeLinker.*;

public class EmptyExpression implements Expression
{
	public JoosType getJoosType()
	{ 
		return JoosPrimitiveType.VOID_TYPE;
	}
	public String inspect()
	{
		return "";
	}

	public void staticCheck( StaticContext context ) throws StaticAnalyzerException { }
	public void setInitialized( StaticContext context ) {}
	
	public Object eval()
	{
		return UNDEFINED;
	}
	public String code(){
		return "";
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
	}
}
