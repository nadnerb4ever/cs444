package cs444.ast.expression;

import cs444.ast.NoTypeException;
import cs444.staticAnalyzer.*;
import cs444.typeLinker.JoosType;
import cs444.typeLinker.JoosPrimitiveType;


public class InstanceOf implements Expression
{
	private Expression m_subExpr;
	private JoosType m_type;

	public InstanceOf( Expression subExpr, JoosType type ) {
		m_subExpr = subExpr;
		m_type = type;
	}
	
	public JoosType getJoosType() throws NoTypeException
	{
		m_subExpr.getJoosType();
		// TODO maybe check that it is actually an Object
		return JoosPrimitiveType.BOOLEAN_TYPE;
	}
	public String inspect()
	{
		return m_subExpr.inspect() + " instanceof " + m_type.fullName();
	}

	@Override
	public void staticCheck( StaticContext context ) throws StaticAnalyzerException
	{
		m_subExpr.staticCheck( context );
	}

	@Override
	public void setInitialized( StaticContext context ) { /* Illegal initialization */ }
	
	public Object eval()
	{
		return UNDEFINED;
	}
	public String code(){
		return "";
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
	}
}
