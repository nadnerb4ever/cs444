package cs444.ast.expression;

import cs444.staticAnalyzer.*;
import cs444.typeLinker.*;
import cs444.ast.NoTypeException;

public interface Expression {
	
	public JoosType getJoosType() throws NoTypeException;
	public String inspect();
	public void staticCheck( StaticContext context ) throws StaticAnalyzerException;
	public void setInitialized( StaticContext context );
	
	// Evaluate this expression
	public Object eval();
	
	public static final String UNDEFINED = "(undefined)";
	public static final String NULL = "(null)";
	
	public String code();
	public void genCode( Appendable out ) throws java.io.IOException;
}
