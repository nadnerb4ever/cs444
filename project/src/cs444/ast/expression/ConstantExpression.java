package cs444.ast.expression;

import cs444.staticAnalyzer.*;
import cs444.typeLinker.*;
import cs444.utils.Code;


// TODO: We have to decide our data layout.
// Once we get the layout, we can store the constants with 
// .data directive and labels:
//
// .data
// [var_name] db initial_value
// str db "This is a test string"


public abstract class ConstantExpression implements Expression{
	
	public static class NullLiteral extends ConstantExpression{
		public NullLiteral() {}
		public Object getValue() { return null;	}
		public JoosType getJoosType() { 
			return JoosPrimitiveType.NULL_TYPE;
		}
		public String inspect()
		{
			return "null";
		}
		public Object eval()
		{
			return UNDEFINED;
		}
		public String code(){
			return "mov eax, 0\n";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			out.append( "mov eax, 0\n" );
		}
	} 
	public static class TrueLiteral extends ConstantExpression{
		public TrueLiteral() {}
		public boolean getValue() {	return true; }
		public JoosType getJoosType(){ 
			return JoosPrimitiveType.BOOLEAN_TYPE; 
		}
		public String inspect()
		{
			return "true";
		}
		public Object eval()
		{
			return Boolean.TRUE;
		}
		public String code(){
			return "mov eax, 1\n";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			out.append( "mov eax, 1\n" );
		}
	}	
	public static class FalseLiteral extends ConstantExpression{
		public FalseLiteral() { }
		public boolean getValue() {	return false; }
		public JoosType getJoosType(){ 
			return JoosPrimitiveType.BOOLEAN_TYPE;
		}
		public String inspect()
		{
			return "false";
		}
		public Object eval()
		{
			return Boolean.FALSE;
		}
		public String code(){
			return "mov eax, 0\n";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			out.append( "mov eax, 0\n" );
		}
	}	
	public static class IntLiteral extends ConstantExpression{
		private String m_value;
		public IntLiteral( String value ) { m_value = value; }
		public String getValue(){ return m_value; }
		public JoosType getJoosType(){ 
			return JoosPrimitiveType.INT_TYPE; 
		}
		public String inspect()
		{
			return m_value;
		}
		public Object eval()
		{
			return Integer.valueOf( m_value );
		}
		public String code(){
			return "mov eax, " + m_value + "\n";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			out.append( "mov eax," + m_value + "\n" );
		}
	}
	public static class ShortLiteral extends ConstantExpression{
		private String m_value;
		public ShortLiteral( String value ) { m_value = value; }
		public String getValue(){ return m_value; }
		public JoosType getJoosType(){ 
			return JoosPrimitiveType.SHORT_TYPE; 
		}
		public String inspect()
		{
			return m_value;
		}
		public Object eval()
		{
			return Short.valueOf( m_value );
		}
		public String code(){
			return "mov eax, " + m_value + "\n";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			out.append( "mov eax, " + m_value + "\n" );
		}
	}
	public static class ByteLiteral extends ConstantExpression{
		private String m_value;
		public ByteLiteral( String value ) { m_value = value; }
		public String getValue(){ return m_value; }
		public JoosType getJoosType(){ 
			return JoosPrimitiveType.BYTE_TYPE; 
		}
		public String inspect()
		{
			return m_value;
		}
		public Object eval()
		{
			return Byte.valueOf( m_value );
		}
		public String code(){
			return "mov eax, " + m_value + "\n";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			out.append( "mov eax, " + m_value + "\n" );
		}
	}
	public static class DoubleLiteral extends ConstantExpression{
		private String m_value;
		public DoubleLiteral( String value ) { m_value = value; }
		public String getValue(){ return m_value; }
		public JoosType getJoosType(){ 
			return JoosPrimitiveType.DOUBLE_TYPE; 
		}
		public String inspect()
		{
			return m_value;
		}
		public Object eval()
		{
			return Double.valueOf( m_value );
		}
		public String code(){
			return "mov eax, " + m_value + "\n";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			out.append( "mov eax, " + m_value + "\n" );
		}
	}
	public static class CharLiteral extends ConstantExpression{
		private String m_c;
		public CharLiteral( String c ) { m_c = c; }
		public String getValue(){ return m_c; }
		public JoosType getJoosType(){ 
			return JoosPrimitiveType.CHAR_TYPE; 
		}
		public String inspect()
		{
			return m_c;
		}
		public Object eval()
		{
			return new Character( m_c.charAt( 1 ) );
		}
		public String code(){
			return "";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			out.append( "mov eax, " + Code.toHex( (int)m_c.charAt(0) ) + "\n" );
		}
	}
	public static class StringLiteral extends ConstantExpression {
		private String m_str;
		public StringLiteral( String str ) { m_str = str; }
		public String getValue(){ return m_str;	}
		public JoosType getJoosType(){ 
			return JoosClass.STRING_TYPE;
		}
		public String inspect()
		{
			return m_str;
		}
		public Object eval()
		{
			return UNDEFINED;
		}
		public String code(){
			//TODO: See the todo above.
			return "";
		}
		public void genCode( Appendable out ) throws java.io.IOException
		{
			String pointer = Code.getStringLiteral( m_str );
			out.append( "extern " + pointer + "\n" );
			out.append( "mov eax, " + pointer + "\n" );
		}
	}
	
	public void staticCheck( StaticContext context ) throws StaticAnalyzerException
	{
		// Nothing to do here </meme>
	}
	public void setInitialized( StaticContext context ) { /* Illegal initialization */	}
}
