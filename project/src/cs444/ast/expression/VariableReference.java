package cs444.ast.expression;

import cs444.ast.NoTypeException;

import cs444.staticAnalyzer.*;
import cs444.typeLinker.JoosClass;
import cs444.typeLinker.JoosMethod;
import cs444.typeLinker.JoosType;
import cs444.typeLinker.JoosVariable;


public class VariableReference implements Expression
{
	JoosVariable m_var;
	
	public VariableReference( JoosVariable var )
	{
		m_var = var;
	}
	
	public JoosType getJoosType() throws NoTypeException
	{
		return m_var.getType();
	}
	public String inspect()
	{
		return "variable:" + m_var.getType().fullName();
	}

	public void staticCheck( StaticContext context ) throws StaticAnalyzerException
	{
		if( !context.isInit( m_var ) ) {
			// All local variable must be initialized before used.
			throw new VariableNotInitializedException( m_var.toString() + " may not be initialized." );
		}
	}

	public void setInitialized( StaticContext context )
	{
		context.init( m_var );
	}
	
	public Object eval()
	{
		return UNDEFINED;
	}
	public String code(){
		return "";
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
	}
}


