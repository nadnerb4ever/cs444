package cs444.ast.statement;

import java.util.ArrayList;
import java.util.List;

import cs444.staticAnalyzer.*;
import cs444.typeLinker.JoosType;
import cs444.typeLinker.JoosScope;
import cs444.typeLinker.JoosPrimitiveType;
import cs444.ast.NoTypeException;

public class Block implements Statement
{
	private List<Statement> m_stmtList;
	private JoosScope m_scope;
	
	public Block( JoosScope scope )
	{
		m_stmtList = new ArrayList<Statement>();
		m_scope = scope;
	}
	public List<Statement> getStatementList() {
		return m_stmtList;
	}
	
	public JoosScope getScope()
	{
		return m_scope;
	}
	public void checkJoosTypes() throws NoTypeException
	{
		for( Statement statement : m_stmtList ) {
			statement.checkJoosTypes();
		}
	}
	public void addStatement( Statement stmt )
	{
		m_stmtList.add( stmt );
	}
	public StaticContext staticCheck( StaticContext in ) throws StaticAnalyzerException {
		if( !in.reachable() ){
			throw new StatementUnreachableException( this.toString() + " is unreachable" );
		}
		StaticContext out = in;
		for( Statement stmt: m_stmtList){
			out = stmt.staticCheck( out );
		}
		return out;
	}
	public String code(){
		String code = "";
		for( Statement stmt: m_stmtList){
			code += stmt.code();
		}
		
		return code;
	}
	public void genCode( Appendable out ) throws java.io.IOException
		{
			for( Statement stmt: m_stmtList){
				stmt.genCode(out);
			}
		}
}
