package cs444.ast.statement;

import cs444.staticAnalyzer.*;
import cs444.ast.NoTypeException;


public class EmptyStatement implements Statement
{
	public EmptyStatement() { }
	public void checkJoosTypes() throws NoTypeException { }
	
	public StaticContext staticCheck( StaticContext in ) throws StaticAnalyzerException
	{
		if( !in.reachable() ) {
			throw new StatementUnreachableException( this.toString() + " is unreachable" );
		}
		return in;
	}
	public String code(){
		return "";
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
	}
}
