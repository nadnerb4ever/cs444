package cs444.ast.statement;

import cs444.ast.expression.ConstantExpression.TrueLiteral;
import cs444.ast.expression.Expression;
import cs444.ast.expression.ConstantExpression.FalseLiteral;
import cs444.staticAnalyzer.*;
import cs444.typeLinker.JoosType;
import cs444.typeLinker.JoosPrimitiveType;
import cs444.ast.NoTypeException;
import cs444.codeGen.Label;

public class WhileStatement implements Statement {
	private Expression m_expr;
	private Statement m_stmt;
	
	public WhileStatement( Expression expr, Statement stmt ){
		m_expr = expr;
		m_stmt = stmt;
	}
	
	public Expression getExpr() {
		return m_expr;
	}

	public Statement getStatement() {
		return m_stmt;
	}
	public void checkJoosTypes() throws NoTypeException
	{
		if( !m_expr.getJoosType().equals( JoosPrimitiveType.BOOLEAN_TYPE ) ) {
			throw new NoTypeException( "the check of a while statement requires a boolean return type" );
		}
		m_stmt.checkJoosTypes();
	}
	public StaticContext staticCheck( StaticContext in) throws StaticAnalyzerException
	{
		if( !in.reachable() ){
			throw new StatementUnreachableException( this.toString() + " is unreachable" );
		}
		
		StaticContext context = in.copy();
		
		m_expr.staticCheck( context );
		
		Object result = m_expr.eval();
		
		if( result.equals( Boolean.FALSE ) ) {
			context.reachable( false );
		}
		m_stmt.staticCheck( context );
		
		if( result.equals( Boolean.TRUE ) ) {
			in.reachable( false );
		}
		
		return in;
	}
	public String code(){
		int labelIndex = Label.getLabelIndex();
		return "loop" + labelIndex + ":\n" + 
				m_expr.code() +
				"cmp eax, 0\n" +
				"je end" + labelIndex + "\n" +
				m_stmt.code() +
				"jmp loop" + labelIndex + "\n" +
				"end" + labelIndex + ":\n";
	}
	public void genCode( Appendable out ) throws java.io.IOException
		{
			int labelIndex = Label.getLabelIndex();
		 	out.append( "loop" + labelIndex + ":\n");
			m_expr.genCode(out);
			out.append("cmp eax, 0\n");
			out.append("je end" + labelIndex + "\n");
			m_stmt.genCode(out);
			out.append("jmp loop" + labelIndex + "\n");
			out.append("end" + labelIndex + ":\n");
		}
}
