package cs444.ast.statement;

import cs444.ast.expression.Expression;
import cs444.staticAnalyzer.*;
import cs444.typeLinker.JoosType;
import cs444.typeLinker.JoosMethod;
import cs444.typeLinker.JoosPrimitiveType;
import cs444.ast.NoTypeException;


public class ReturnStatement implements Statement
{
	private Expression m_expr;
	private JoosMethod m_method;
	
	public ReturnStatement( Expression expr, JoosMethod method )
	{
		m_expr = expr;
		m_method = method;
	}
	
	public Expression getExpr()
	{
		return m_expr;
	}
	public void checkJoosTypes() throws NoTypeException
	{
		JoosType retType = m_method.getReturnType();
		JoosType exprType = m_expr.getJoosType();
		
		if( exprType.equals( JoosPrimitiveType.VOID_TYPE ) ) {
			if( !retType.equals( JoosPrimitiveType.VOID_TYPE ) ) {
				throw new NoTypeException( "Return statement is expected to return type " + retType.fullName() );
			}
		} else if( retType.equals( JoosPrimitiveType.VOID_TYPE ) ) {
			throw new NoTypeException( "Return statement cannot return non-void type from a void method" );
		} else if( !retType.isSuperType( exprType ) ) {
			throw new NoTypeException( "Method expected to return type " + retType.fullName() + " but instead returns " + exprType.fullName() );
		}
	}
	public StaticContext staticCheck( StaticContext in ) throws StaticAnalyzerException
	{
		if( !in.reachable() ){
			throw new StatementUnreachableException( this.toString() + " is unreachable" );
		}
		
		m_expr.staticCheck( in );
		
		in.reachable( false );
		
		return in;
	}
	public String code(){
		return m_expr.code() +
				"leave\n" +
				"ret\n";
	}
	public void genCode( Appendable out ) throws java.io.IOException
		{
			m_expr.genCode(out);
			out.append("leave\n");
			out.append("ret\n");
		}
}
