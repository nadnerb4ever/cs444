package cs444.ast.statement;

import cs444.ast.expression.Expression;
import cs444.staticAnalyzer.*;

import cs444.typeLinker.JoosPrimitiveType;
import cs444.ast.NoTypeException;
import cs444.codeGen.Label;

public class IfStatement implements Statement{
	private Expression m_expr;
	private Statement m_stmt;
	
	
	public IfStatement( Expression expr, Statement stmt ){
		m_expr = expr;
		m_stmt = stmt;
	}
	
	public Expression getExpr() {
		return m_expr;
	}

	public Statement getStatement() {
		return m_stmt;
	}
	public void checkJoosTypes() throws NoTypeException
	{
		if( !m_expr.getJoosType().equals( JoosPrimitiveType.BOOLEAN_TYPE ) ) {
			throw new NoTypeException( "the check of a if statement requires a boolean return type" );
		}
		m_stmt.checkJoosTypes();
	}
	public StaticContext staticCheck( StaticContext in ) throws StaticAnalyzerException
	{
		if( !in.reachable() ){
			throw new StatementUnreachableException( this.toString() + " is unreachable" );
		}
		StaticContext innerContext = in.copy();
		m_expr.staticCheck( innerContext );
		m_stmt.staticCheck( innerContext );
		return in;
	}
	public String code(){
		int labelIndex = Label.getLabelIndex();
		return m_expr.code() +
				"cmp eax, 0\n" +
				"je end" + labelIndex + "\n" +
				m_stmt.code() +
				"end" + labelIndex + ":\n";
	}
	public void genCode( Appendable out ) throws java.io.IOException
		{
			int labelIndex = Label.getLabelIndex();
			m_expr.genCode(out); 
			out.append("cmp eax, 0\n");
			out.append("je end" + labelIndex + "\n");
			m_stmt.genCode(out);
			out.append("end" + labelIndex + ":\n");
		}
}
