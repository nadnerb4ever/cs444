package cs444.ast.statement;

import cs444.ast.expression.Expression;
import cs444.staticAnalyzer.*;
import cs444.ast.NoTypeException;

public class ExpressionStatement implements Statement
{
	private Expression m_expr;
	
	public ExpressionStatement( Expression expr )
	{
		m_expr = expr;
	}
	
	public void checkJoosTypes() throws NoTypeException
	{
		m_expr.getJoosType();
	}
	public StaticContext staticCheck( StaticContext in ) throws StaticAnalyzerException
	{
		if( !in.reachable() ) {
			throw new StatementUnreachableException( this.toString() + " is unreachable" );
		}
		m_expr.staticCheck( in );
		return in;
	}
	public String code(){
		return m_expr.code();
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
		m_expr.genCode(out);
	}
}
