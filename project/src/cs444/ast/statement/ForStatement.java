package cs444.ast.statement;

import cs444.ast.expression.Expression;
import cs444.ast.expression.ConstantExpression.FalseLiteral;
import cs444.ast.expression.ConstantExpression.TrueLiteral;
import cs444.staticAnalyzer.*;
import cs444.typeLinker.JoosType;
import cs444.typeLinker.JoosPrimitiveType;
import cs444.ast.NoTypeException;
import cs444.codeGen.Label;


public class ForStatement implements Statement
{
	private Expression m_init, m_expr, m_update;
	private Statement m_stmt;
	public ForStatement( Expression init, Expression expr, Expression update, Statement stmt )
	{
		m_init = init;
		m_expr = expr;
		m_update = update;
		m_stmt = stmt;
	}
	
	public Expression getInit()
	{
		return m_init;
	}
	public Expression getExpr()
	{
		return m_expr;
	}
	public Expression getUpdate()
	{
		return m_update;
	}
	public Statement getStatement()
	{
		return m_stmt;
	}

	public void checkJoosTypes() throws NoTypeException
	{
		m_init.getJoosType();
		if( !m_expr.getJoosType().equals( JoosPrimitiveType.BOOLEAN_TYPE ) ) {
			throw new NoTypeException( "the check of a for statement requires a boolean return type" );
		}
		m_update.getJoosType();
		m_stmt.checkJoosTypes();
	}
	
	public StaticContext staticCheck( StaticContext in ) throws StaticAnalyzerException
	{
		if( !in.reachable() ){
			throw new StatementUnreachableException( this.toString() + " is unreachable" );
		}
		
		// TODO use context1 for m_init, m_expr, m_update
		StaticContext context1 = in.copy();
		
		m_init.staticCheck( context1 );
		m_expr.staticCheck( context1 );
		m_update.staticCheck( context1 );
		
		Object result = m_expr.eval();
		
		StaticContext context2 = context1.copy();
		
		if( result.equals( Boolean.FALSE ) ) {
			context2.reachable( false );
		}
		
		m_stmt.staticCheck( context2 );
		
		if( result.equals( Boolean.TRUE ) ) {
			in.reachable( false );
		}
		
		return in;
	}
	public String code(){
		int labelIndex = Label.getLabelIndex();

		return m_init.code() +
				"loop" + labelIndex + ":\n" + 
				m_expr.code() +
				"cmp eax, 0\n" +
				"je end" + labelIndex + "\n" +
				m_stmt.code() +
				m_update.code() +
				"jmp loop" + labelIndex + "\n" +
				"end" + labelIndex + ":\n";
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
		int labelIndex = Label.getLabelIndex();
		
		m_init.genCode(out);
		out.append("loop" + labelIndex + ":\n");
		m_expr.genCode(out);
		out.append("cmp eax, 0\n");
		out.append("je end" + labelIndex + "\n");
		m_stmt.genCode(out);
		m_update.genCode(out);
		out.append("jmp loop" + labelIndex + "\n");
		out.append("end" + labelIndex + ":\n");
	}
}
