package cs444.ast.statement;

import cs444.ast.expression.Expression;
import cs444.staticAnalyzer.*;

import cs444.typeLinker.JoosPrimitiveType;
import cs444.ast.NoTypeException;
import cs444.codeGen.Label;

public class IfElseStatement implements Statement
{
	private Expression m_expr;
	private Statement m_stmtTrue;
	private Statement m_stmtFalse;
	
	
	public IfElseStatement( Expression expr, Statement stmtTrue, Statement stmtFalse ){
		m_expr = expr;
		m_stmtTrue = stmtTrue;
		m_stmtFalse = stmtFalse;
	}
	
	public Expression getExpr() {
		return m_expr;
	}

	public Statement getStatementTrue() {
		return m_stmtTrue;
	}
	public Statement getStatementFalse() {
		return m_stmtFalse;
	}
	public void checkJoosTypes() throws NoTypeException
	{
		if( !m_expr.getJoosType().equals( JoosPrimitiveType.BOOLEAN_TYPE ) ) {
			throw new NoTypeException( "the check of a if statement requires a boolean return type" );
		}
		m_stmtTrue.checkJoosTypes();
		m_stmtFalse.checkJoosTypes();
	}
	public StaticContext staticCheck( StaticContext in ) throws StaticAnalyzerException
	{
		if( !in.reachable() ) {
			throw new StatementUnreachableException( this.toString() + " is unreachable" );
		}
		StaticContext ifContext = in.copy();
		
		m_expr.staticCheck( ifContext );
		
		StaticContext elseContext = ifContext.copy();
		
		ifContext = m_stmtFalse.staticCheck( ifContext );
		elseContext = m_stmtTrue.staticCheck( elseContext );
		
		ifContext.intersect( elseContext ).copyInto( in );
		
		return in;
	}
	public String code(){
		int labelIndex = Label.getLabelIndex();
		return m_expr.code() +
				"cmp eax, 0\n" +
				"je else" + labelIndex + "\n" +
				m_stmtTrue.code() +
				"jmp end" + labelIndex + "\n" +
				"else" + labelIndex + ":\n" +
				m_stmtFalse.code() +
				"end" + labelIndex + ":\n";
	}
	public void genCode( Appendable out ) throws java.io.IOException
		{
			int labelIndex = Label.getLabelIndex();
		  m_expr.genCode(out);
			out.append( "cmp eax, 0\n");
			out.append( "je else" + labelIndex + "\n");
			m_stmtTrue.genCode(out);
			out.append("jmp end" + labelIndex + "\n");
			out.append("else" + labelIndex + ":\n");
			m_stmtFalse.genCode(out);
			out.append("end" + labelIndex + ":\n");
		}
}
