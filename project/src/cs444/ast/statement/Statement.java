package cs444.ast.statement;

import cs444.staticAnalyzer.*;
import cs444.ast.NoTypeException;

public interface Statement {
	
	public void checkJoosTypes() throws NoTypeException;
	
	// This will do the decl-use check as well as the reachability check 
	// using the method discussed in class.
	// - in: the static context of the current block when starting the statement
	// returns the static context of the current block upon finishing the statement
	public StaticContext staticCheck( StaticContext context ) throws StaticAnalyzerException;
	
	public String code();
	public void genCode( Appendable out ) throws java.io.IOException;
}
