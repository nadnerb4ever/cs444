package cs444.typeLinker;

import java.util.*;
import cs444.ast.statement.Block;
import cs444.ast.NoTypeException;
import cs444.staticAnalyzer.*;
import cs444.utils.Code;


class JoosClassImpl extends JoosObjectTypeImpl implements JoosClass, VariableSource
{
	private JoosClass m_parent;
	private Set<JoosInterface> m_interfaces;
	private boolean m_isAbstract = false;
	private boolean m_isFinal = false;
	
	private Map<String, JoosField> m_fields;
	private JoosClassScope m_classScope = null;
	private JoosClassScope m_staticScope = null;
	private Block m_instanceInitializer = null;
	private Block m_staticInitializer = null;
	
	public JoosClassImpl( String packageName, String name, boolean isAbstract, boolean isFinal )
	{
		super( packageName, name );
		m_parent = null;
		m_interfaces = new HashSet<JoosInterface>();
		m_fields = new TreeMap<String, JoosField>();
		m_isAbstract = isAbstract;
		m_isFinal = isFinal;
	}
	
	public Map<String, JoosField> getFields()
	{
		return m_fields;
	}
	public JoosField getField( String name )
	{
		return getFields().get( name );
	}
	public void addField( String name, JoosType type, Set<String> modifiers ) throws Exception
	{
		JoosField var = m_fields.get( name );
		if( var != null ) {
			VariableSource origin = var.getOrigin();
			if( equals( origin ) ) {
				throw new Exception( "Class already contains a field '" + name + "'" );
			}
		}
		boolean isStatic = modifiers.contains( "static" );
		int permission;
		if( modifiers.contains( "private" ) ) {
			permission = JoosMember.PERMISSION_PRIVATE;
		} else if( modifiers.contains( "protected" ) ) {
			permission = JoosMember.PERMISSION_PROTECTED;
		} else {
			permission = JoosMember.PERMISSION_PUBLIC;
		}
		var = new JoosFieldImpl( name, type, this, permission, isStatic );
		m_fields.put( name, var );
	}
	
	public Set<JoosInterface> getImplements()
	{
		return m_interfaces;
	}
	public JoosClass getExtends()
	{
		return m_parent;
	}
	
	
	protected boolean isSuperType( JoosObjectType other )
	{
		return equals( OBJECT_TYPE ) || isSuperType( other.asJoosClass() );
	}
	protected boolean isSuperType( JoosClass other )
	{
		if( other == null ) {
			return false;
		}
		return equals( other ) || (other.getExtends() != null && isSuperType( other.getExtends() ));
	}
	public boolean isAbstract()
	{
		return m_isAbstract;
	}
	public boolean isFinal()
	{
		return m_isFinal;
	}
	public JoosStaticType staticTypeOf()
	{
		return new JoosStaticType( this );
	}
	
	public void addImplements( JoosInterface jInterface )
	{
		m_interfaces.add( jInterface );
	}
	public void setExtends( JoosClass jClass )
	{
		m_parent = jClass;
	}
	public void inherit() throws Exception
	{
		for( JoosInterface jInterface : getImplements() ) {
			inheritMethods( jInterface );
		}
		if( m_parent != null ) {
			inheritFields( m_parent );
			inheritMethods( m_parent );
		}
	}
	public void check() throws Exception
	{
		for( JoosMethod method : getMethods() ) {
			//System.err.println( "checking method: " + method );
			if( method.isAbstract() ) {
				if( !isAbstract() ) {
					throw new Exception( "Non-abstract class " + fullName() + " cannot have abstract method " + method.getName() );
				}
			} else {
				try {
					if( method.getBody() != null ) {
						method.getBody().checkJoosTypes();
						StaticContext context = method.getBody().staticCheck( new StaticContext( true ) );
						if( context.reachable() && !method.getReturnType().equals( JoosPrimitiveType.VOID_TYPE ) ) {
							throw new Exception( "Missing return statement at the end of non-void function" );
						}
					}
				} catch( Exception e ) {
					throw new Exception( "Error in " + method.toString() + ": " + e.getMessage(), e );
				}
			}
		}
		try {
			getInitializer().checkJoosTypes();
			getInitializer().staticCheck( new StaticContext( false ) );
			getStaticInitializer().checkJoosTypes();
			getStaticInitializer().staticCheck( new StaticContext( false ) );
		} catch( NoTypeException e ) {
			throw new Exception( "Error in " + fullName() + ": " + e.getMessage(), e );
		}
		if( getExtends() != null && getExtends().isFinal() ) {
			throw new Exception( "A class cannot inherit from a final class" );
		}
	}
	protected void inheritFields( JoosClass other ) throws Exception
	{
		for( Map.Entry<String, JoosField> entry : other.getFields().entrySet() ) {
			String name = entry.getKey();
			JoosField field = entry.getValue();
			if( !field.isStatic() && field.getPermission() != JoosMember.PERMISSION_PRIVATE ) {
				m_fields.put( name, field );
			}
		}
	}
	
	
	public JoosClass asJoosClass()
	{
		return this;
	}
	public JoosMethod asJoosMethod()
	{
		return null;
	}
	public JoosScope asJoosScope()
	{
		return null;
	}
	
	public String inspect()
	{
		StringBuilder builder = new StringBuilder();
		builder.append( "public " );
		if( isFinal() ) {
			builder.append( "final " );
		}
		builder.append( "class " );
		builder.append( fullName() );
		if( getExtends() != null ) {
			builder.append( " extends " );
			builder.append( getExtends().fullName() );
		}
		builder.append( join( getImplements(), " implements ", ", ", "", false ) );
		builder.append( "\n{" );
		for( Map.Entry<String, JoosField> entry : getFields().entrySet() ) {
			String name = entry.getKey();
			JoosVariable var = entry.getValue();
			builder.append( "\n\t" + var.getType().fullName() + " " + name + "; (defined in " + var.getOrigin().fullName() + ")" );
		}
		for( JoosMethod method : getMethods() ) {
			builder.append( "\n\t" );
			builder.append( method.toString() );
		}
		builder.append( "\n}" );
		return builder.toString();
	}
	public JoosScope getClassScope()
	{
		if( m_classScope == null ) {
			m_classScope = new JoosClassScopeImpl( this, false );
		}
		return m_classScope;
	}
	public JoosScope getStaticScope()
	{
		if( m_staticScope == null ) {
			m_staticScope = new JoosClassScopeImpl( this, true );
		}
		return m_staticScope;
	}
	public Block getInitializer()
	{
		if( m_instanceInitializer == null ) {
			m_instanceInitializer = new Block( getClassScope() );
		}
		return m_instanceInitializer;
	}
	public Block getStaticInitializer()
	{
		if( m_staticInitializer == null ) {
			m_staticInitializer = new Block( getStaticScope() );
		}
		return m_staticInitializer;
	}
	public Collection<JoosObjectType> getParents()
	{
		ArrayList<JoosObjectType> ret = new ArrayList<JoosObjectType>();
		ret.addAll( getImplements() );
		ret.add( getExtends() );
		return ret;
	}
	
	public void genCode( Appendable out, int numTypes ) throws java.io.IOException
	{
		genExterns( out );
		if( !isAbstract() ) {
			genVTable( out, numTypes );
		}
		genMethods( out );
		genInitializer( out );
		genStaticInitializer( out );
	}
	public void genExterns( Appendable out ) throws java.io.IOException
	{
		out.append( "extern __exception\n" );
		for( JoosMethod method : JoosMethod.ALL_METHODS ) {
			if( !method.isAbstract() && (!equals( method.getSource() ) || method.isNative()) ) {
				out.append( "extern " + Code.makeLabel( method ) + "\n" );
			}
		}
		out.append( "\n" );
	}
	public void genVTable( Appendable out, int numTypes ) throws java.io.IOException
	{
		out.append( ";;== VTABLE ==;;\n" );
		out.append( Code.makeLabel( "__vtable__", this ) + ":\n" );
		// Output the identifier
		out.append( "\tdd " + Code.toHex( getIdentifier() ) + "\n" );
		TreeMap<Integer, JoosObjectType> types = getParentTypes();
		// output the number of parent types
		//out.append( "\tdd " + Code.toHex( types.size() ) + "\n" );

		// output the locations of the parent types (in sorted order)
		for( int i = 0; i < numTypes; i++ ) {
			Integer key = new Integer( i );
			if( types.containsKey( key ) ) {
				JoosObjectType type = types.get( key );
				out.append( "\tdd " + Code.makeLabel( "__vtable_entry__", type ) + "\n" );
			} else {
				out.append( "\tdd 0x00000000\n" );
			}
		}
		/*
		for( Integer i : types.keySet() ) {
			JoosObjectType type = types.get( i );
			out.append( "\tdd " + Code.toHex( type.getIdentifier() ) + "\n" );
			out.append( "\tdd ." + Code.makeLabel( "__vtable_entry__", type ) + "\n" );
		}
		*/
		// output the jump table for each parent type
		for( JoosObjectType type : types.values() ) {
			out.append( Code.makeLabel( "__vtable_entry__", type ) + ":\n" );
			for( JoosMethod method : type.getMethods() ) {
				method = getMatchingMethod( method );
				/*
				if( !equals( method.getSource() ) ) {
					out.append( "\textern " + Code.makeLabel( method ) + "\n" );
				}
				*/
				out.append( "\tdd " + Code.makeLabel( method ) + "\n" );
			}
		}
		out.append( "\n\n\n" );
	}
	public void genMethods( Appendable out ) throws java.io.IOException
	{
		for( JoosMethod method : getMethods() ) {
			// Don't generate code for inherited methods
			if( equals( method.getSource() ) ) {
				method.genCode( out );
			}
		}
	}
	public void genInitializer( Appendable out ) throws java.io.IOException
	{
	}
	public void genStaticInitializer( Appendable out ) throws java.io.IOException
	{
	}
	private TreeMap<Integer, JoosObjectType> getParentTypes()
	{
		TreeMap<Integer, JoosObjectType> ret = new TreeMap<Integer, JoosObjectType>();
		Stack<JoosObjectType> worklist = new Stack<JoosObjectType>();
		worklist.push( this );
		while( !worklist.isEmpty() ) {
			JoosObjectType next = worklist.pop();
			ret.put( next.getIdentifier(), next );
			for( JoosObjectType type : next.getParents() ) {

				if( type != null && !ret.containsKey( type.getIdentifier() ) ) {
					worklist.push( type );
				}
			}
		}
		return ret;
	}
}



