package cs444.typeLinker.localVariableList;

import java.util.Stack;

import cs444.parser.ParseTree.HandlerException;
import cs444.parser.ParseTree.NodeHandler;
import cs444.parser.ParseTreeNode;
import cs444.typeLinker.Environment;
import cs444.typeLinker.JoosMethodScope;
import cs444.typeLinker.JoosNestedScope;
import cs444.typeLinker.JoosScope;
import cs444.typeLinker.JoosType;

public class LocalVariableListBuilder implements NodeHandler {
	private Stack<JoosScope> m_stack; // Keeps track of current scope at the top
	private Environment m_env;
	
	/**
	 * Constructor
	 * @param scope: has to be initialized and expected to have parameters
	 * @param env: has a list of available types
	 */
	public LocalVariableListBuilder( JoosMethodScope scope, Environment env )
	{
		m_stack = new Stack<JoosScope>();
		m_stack.push(scope);
		m_env = env;
	}
	
	/**
	 * OnEntryNode method
	 * - Overrides the interface method
	 * - Since statements can have nested scopes(i.e. block, for/while loops, if), 
	 *   this will push a new scope to the stack when such a scope is created.
	 * joos.cfg:
	 *   LocalVariableDeclaration Type ID ASSIGN Expression
	 *   LocalVariableDeclaration BasicType LSQBRACE RSQBRACE ID ASSIGN Expression
	 *   LocalVariableDeclaration MemberAccess LSQBRACE RSQBRACE ID ASSIGN Expression
	 *   
	 *   Type ObjectType // QualifiedID
	 *   Type BasicType  // BOOLEAN, BYTE, INT, CHAR, etc...
	 *   
	 *   MemberAccess // Must be QualifiedID
	 */
	@Override
	public boolean onEnterNode( ParseTreeNode node ) throws HandlerException
	{
		try{
			if( node.getType().equals( "LocalVariableDeclaration" ) ) {
			
				ParseTreeNode[] children = node.getChildren();
				/*
				 * Find out the type of the variable to be declared
				 */
				JoosType type = null;
				type = m_env.lookup( children[0].toString() );
			
				/*
				 * Get the variable name to be declared
				 */
				ParseTreeNode expr;
				if( children[0].getType().equals("Type") ) {
					expr = children[1];
				} else {
					expr = children[3];
					type = type.arrayOf();
				}
				String name = expr.getChildren()[0].getValue();
			
				/*
				 * Add the new variable to the current scope
				 */
				m_stack.peek().addJoosVariable( type, name );
			} else if( needNewScope( node ) ){
				JoosNestedScope newScope = m_stack.peek().addNestedScope();
				m_stack.push( newScope );
				node.annotate( newScope );
			} else if( node.getType().equals( "ClassInstanceCreationExpression" ) ||
				         node.getType().equals( "ArrayCreationExpression" ) ) {
				// Check that the type exists
				ParseTreeNode[] children = node.getChildren();
				JoosType type = m_env.lookup( children[1].toString() );
			}
		} catch( Exception e ) {
			throw new HandlerException( e.getMessage(), e );
		}
		return true;
	}

	/**
	 * OnExitNode method
	 * - Overrides the interface method
	 * - Similar to the onEntryNode method, this will pop the scope from the stack
	 */
	@Override
	public void onExitNode(ParseTreeNode node) throws HandlerException {
		if( needNewScope(node) ){
			m_stack.pop();
		}
	}
	
	private boolean needNewScope( ParseTreeNode node ){
		return node.getType().equals( "Block" ) 
		 || node.getType().startsWith( "IfThen" )
		 || node.getType().startsWith( "WhileStatement" )
		 || node.getType().startsWith( "ForStatement" );
	}
}
