package cs444.typeLinker;

import java.util.*;


public interface JoosObjectType extends JoosType
{
	public int getIdentifier();
	public String packageName();
	public String shortName();
	public String fullName();
	public Set<JoosMethod> getMethods();
	public Set<JoosMethod> getMethods( String name );
	public List<JoosMethod> getMethods( String name, Collection<JoosType> args, boolean staticMethods );
	public JoosMethod getMatchingMethod( JoosMethod method );
	public int cardinality( JoosMethod method );
	public Collection<JoosObjectType> getParents();
	public boolean isAbstract();
	public boolean isFinal();
	
	public JoosMethod createMethod( String name, JoosType returnType, Set<String> modifiers ) throws Exception;
	public void addMethod( JoosMethod method ) throws Exception;
	public void inherit() throws Exception;
	public void check() throws Exception;

	public JoosClass asJoosClass();
	public JoosInterface asJoosInterface();
}




