package cs444.typeLinker;

class JoosMethodScopeImpl extends JoosScopeImpl implements JoosMethodScope
{
	private JoosClass m_parent;
	private JoosMethod m_method;
	
	JoosMethodScopeImpl( JoosClass parent, JoosMethod method )
	{
		m_parent = parent;
		m_method = method;
		inherit( parent.getFields() );
		inherit( method.getVariables() );
	}
	
	
	public JoosClass getParent()
	{
		return m_parent;
	}
	public JoosMethod getMethod()
	{
		return m_method;
	}
	
	
	public JoosMethodScope asMethodScope()
	{
		return this;
	}
}


