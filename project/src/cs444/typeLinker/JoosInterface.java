package cs444.typeLinker;

import java.util.*;


public interface JoosInterface extends JoosObjectType
{
	public Set<JoosInterface> getExtends();
	
	public void addExtends( JoosInterface extend );
}



