package cs444.typeLinker;


public abstract class JoosTypeImpl implements JoosType
{
	public abstract boolean isSimilar( JoosType other );
	public abstract boolean isSuperType( JoosType other );
	public JoosArrayType arrayOf()
	{
		return new JoosArrayType( this );
	}
	public JoosObjectType asObjectType()
	{
		return null;
	}
	public JoosPrimitiveType asPrimitiveType()
	{
		return null;
	}
	public JoosArrayType asArrayType()
	{
		return null;
	}
	public JoosStaticType asStaticType()
	{
		return null;
	}
	public int compareTo( JoosType other )
	{
		return fullName().compareTo( other.fullName() );
	}
	public boolean equals( JoosType other )
	{
		return fullName().equals( other.fullName() );
	}
	public int hashCode()
	{
		return fullName().hashCode();
	}
	public String toString()
	{
		return fullName();
	}
}


