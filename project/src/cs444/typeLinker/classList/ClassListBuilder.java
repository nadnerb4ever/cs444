package cs444.typeLinker.classList;

import cs444.typeLinker.*;
import cs444.parser.*;
import cs444.parser.ParseTree.*;


public class ClassListBuilder implements NodeHandler
{
	private CompilationEnvironment m_env;
	private String m_package;
	private JoosObjectType m_type;

	public ClassListBuilder( CompilationEnvironment env )
	{
		m_env = env;
		m_package = null;
		m_type = null;
	}

	// Interface methods
	public boolean onEnterNode( ParseTreeNode node ) throws HandlerException
	{
		try {
			if( node.getType().equals( "PackageDeclarationOpt" ) ) {
				ParseTreeNode[] children = node.getChildren();
				if( children.length > 0 ) {
					m_package = children[1].toString();
				}
			} else if( node.getType().equals( "ClassDeclaration" ) ) {
				ParseTreeNode[] children = node.getChildren();
				java.util.Set<String> modifiers = children[0].asSet( 0 );
				m_type = m_env.addClass( m_package, children[2].getValue(), modifiers.contains( "abstract" ), modifiers.contains( "final" ) );
			} else if( node.getType().equals( "InterfaceDeclaration" ) ) {
				m_type = m_env.addInterface( m_package, node.getChildren()[2].getValue() );
			} else {
				return true;
			}
			return false;
		} catch( Exception e ) {
			throw new HandlerException( e.getMessage(), e );
		}
	}
	public void onExitNode( ParseTreeNode node ) throws HandlerException
	{
		if(  node.getType().equals( "CompilationUnit" ) ) {
			m_package = null;
		}
	}
	
	// Extra methods
	public JoosObjectType getType()
	{
		return m_type;
	}
	
	
	// Private helper methods
	private String getFullName( ParseTreeNode idNode )
	{
		if( m_package != null ) {
			return m_package + "." + idNode.getValue();
		}
		return idNode.getValue();
	}
}


