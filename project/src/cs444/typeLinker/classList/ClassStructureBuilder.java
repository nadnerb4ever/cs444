package cs444.typeLinker.classList;

import cs444.typeLinker.*;
import cs444.parser.*;
import cs444.parser.ParseTree.*;


public class ClassStructureBuilder implements NodeHandler
{
	private Environment m_env;
	private JoosObjectType m_jObject;
	

	public ClassStructureBuilder( Environment env, JoosObjectType jObject )
	{
		m_env = env;
		m_jObject = jObject;
	}
	
	public boolean onEnterNode( ParseTreeNode node ) throws HandlerException
	{
		try {
			if( node.getType().equals( "ClassBody" ) || node.getType().equals( "InterfaceBody" ) || node.getType().equals( "QualifiedID" ) ) {
				return false;
			} else if( node.getType().equals( "ClassExtends" ) ) {
				String name = node.getChildren()[1].toString();
				JoosType type = m_env.lookup( name );
				JoosClass jClass = type.asObjectType().asJoosClass();
				if( jClass == null ) {
					throw new HandlerException( "class " + m_jObject.fullName() + " must extend another class" );
				}
				m_jObject.asJoosClass().setExtends( jClass );
			} else if( node.getType().equals( "ImplementsList" ) ) {
				String name = node.getChildren()[0].toString();
				JoosType type = m_env.lookup( name );
				m_jObject.asJoosClass().addImplements( type.asObjectType().asJoosInterface() );
			} else if( node.getType().equals( "ExtendsList" ) ) {
				String name = node.getChildren()[0].toString();
				JoosType type = m_env.lookup( name );
				m_jObject.asJoosInterface().addExtends( type.asObjectType().asJoosInterface() );
			}
		} catch( Exception e ) {
			throw new HandlerException( e.getMessage(), e );
		}
		return true;
	}
	
	public void onExitNode( ParseTreeNode node ) throws HandlerException
	{
		try {
			// Perform imports on node exit since they are stored in a left-oriented tree
			// This will ensure that they are performed in order
			if( node.getType().equals( "ImportDeclaration" ) ) {
				node = node.getChildren()[0];
				ParseTreeNode[] children = node.getChildren();
				String id = children[1].toString();
				if( node.getType().equals( "SingleTypeImportDeclaration" ) ) {
					m_env.doImport( id );
				} else {
					m_env.doImportStar( id );
				}
			} else if( node.getType().equals( "ClassDeclaration" ) ) {
				JoosClass jClass = m_jObject.asJoosClass();
				if( jClass.getExtends() == null && !jClass.fullName().equals( "java.lang.Object" ) ) {
					jClass.setExtends( m_env.lookup( "java.lang.Object" ).asObjectType().asJoosClass() );
				}
			} else if( node.getType().equals( "InterfaceDeclaration" ) ) {
				JoosInterface jInterface = m_jObject.asJoosInterface();
				if( jInterface.getExtends().size() == 0 && !jInterface.fullName().equals( "java.lang.Object2" ) ) {
					jInterface.addExtends( m_env.lookup( "java.lang.Object2" ).asObjectType().asJoosInterface() );
				}
			}
		} catch( Exception e ) {
			throw new HandlerException( e.getMessage(), e );
		}
	}
	
}
