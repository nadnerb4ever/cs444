package cs444.typeLinker;


public interface JoosScope extends VariableSource
{
	// Returns JoosVariable matches with the name.
	// Throws an exception when not found.
	public JoosVariable lookup( String name ) throws Exception;

	// Add new variable of JoosType to this scope. 
	// Throws an exception when the variable already declared and visible
	public void addJoosVariable( JoosType type, String name ) throws Exception;
	
	// Create and return new nested scope
	public JoosNestedScope addNestedScope();
	
	public JoosClassScope asClassScope();
	public JoosMethodScope asMethodScope();
	public JoosNestedScope asNestedScope();
}
