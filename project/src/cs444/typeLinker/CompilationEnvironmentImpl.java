package cs444.typeLinker;

import java.util.*;


class CompilationEnvironmentImpl implements CompilationEnvironment
{
	private Set<JoosClass> m_classes;
	private Set<JoosInterface> m_interfaces;
	
	private Set<String> m_packages;
	private Set<String> m_objects;
	
	protected CompilationEnvironmentImpl()
	{
		m_classes = new TreeSet<JoosClass>();
		m_interfaces = new TreeSet<JoosInterface>();
		m_packages = new TreeSet<String>();
		m_objects = new TreeSet<String>();
	}
	
	// Interface methods
	public Set<JoosClass> getClasses()
	{
		return m_classes;
	}
	public Set<JoosInterface> getInterfaces()
	{
		return m_interfaces;
	}
	public Environment getInstance( JoosObjectType mainType ) throws Exception
	{
		EnvironmentImpl ret = new EnvironmentImpl( mainType );
		ret.addAll( m_classes );
		ret.addAll( m_interfaces );
		ret.doImportStar( "java.lang" );
		ret.doImportDefault( mainType.packageName() );
		return ret; 
	}
	public JoosClass addClass( String packageName, String className, boolean isAbstract, boolean isFinal ) throws Exception
	{
		JoosClass jClass;
		if( "java.lang".equals( packageName ) && "String".equals( className ) ) {
			jClass = JoosClass.STRING_TYPE;
		} else if ( "java.lang".equals( packageName ) && "Object".equals( className ) ) {
			jClass = JoosClass.OBJECT_TYPE;
		} else {
			jClass = new JoosClassImpl( packageName, className, isAbstract, isFinal );
		}
		m_classes.add( jClass );
		addPackages( packageName );
		addObject( getName( jClass ) );
		return jClass;
	}
	public JoosInterface addInterface( String packageName, String interfaceName ) throws Exception
	{
		JoosInterface jInterface = new JoosInterfaceImpl( packageName, interfaceName );
		m_interfaces.add( jInterface );
		addPackages( packageName );
		addObject( getName( jInterface ) );
		return jInterface;
	}
	
	private void addPackages( String packageName ) throws Exception
	{
		if( packageName == null ) {
			packageName = "$default";
		}
		addPackage( packageName );
		int idx = packageName.indexOf( '.' );
		while( idx != -1 ) {
			String subPackageName = packageName.substring( 0, idx );
			addPackage( subPackageName );
			idx = packageName.indexOf( '.', idx + 1 );
		}
	}
	private void addPackage( String packageName ) throws Exception
	{
		if( m_objects.contains( packageName ) ) {
			throw new Exception( "Cannot have identicle package and object names" ); 
		}
		m_packages.add( packageName );
	}
	private void addObject( String objectName ) throws Exception
	{
		if( m_packages.contains( objectName ) ) {
			throw new Exception( "Cannot have identicle package and object names" ); 
		}
		m_objects.add( objectName );
	}
	private String getName( JoosObjectType jObject )
	{
		String packageName = jObject.packageName();
		if( packageName == null ) {
			packageName = "$default";
		}
		return packageName + "." + jObject.shortName();
	}
}


