package cs444.typeLinker;

import java.util.*;


class JoosClassScopeImpl extends JoosScopeImpl implements JoosClassScope
{
	private JoosClass m_parent;
	
	JoosClassScopeImpl( JoosClass parent, boolean isStatic )
	{
		m_parent = parent;
		for( Map.Entry<String, JoosField> entry : parent.getFields().entrySet() ) {
			String name = entry.getKey();
			JoosField field = entry.getValue();
			if( field.isStatic() == isStatic ) {
				inheritVariable( name, field );
			}
		}
	}
	
	
	public JoosClass getParent()
	{
		return m_parent;
	}
	
	
	public JoosClassScope asClassScope()
	{
		return this;
	}
}


