package cs444.typeLinker;


public class JoosStaticType extends JoosTypeImpl implements JoosType
{
	private JoosClass m_inner;
	
	public JoosStaticType( JoosClass inner )
	{
		m_inner = inner;
	}
	public JoosClass getInnerType()
	{
		return m_inner;
	}
	
	// Interface methods
	public boolean isSimilar( JoosType other )
	{
		return false;
	}
	public boolean isSuperType( JoosType other )
	{
		return false;
	}
	public String fullName()
	{
		return "classof( " + m_inner.fullName() + " )";
	}
	
	public JoosStaticType asStaticType()
	{
		return this;
	}
	public JoosArrayType arrayOf()
	{
		return null;
	}
	
	// Overridden Object Methods
	public String toString()
	{
		return fullName();
	}
	public boolean equals( Object o )
	{
		if( o == null || !(o instanceof JoosStaticType) ) {
			return false;
		}
		JoosStaticType other = (JoosStaticType)o;
		return m_inner.equals( other.getInnerType() );
	}
	public int hashCode()
	{
		return m_inner.hashCode() + 13;
	}
}


