package cs444.typeLinker;

import java.util.*;
import cs444.ast.statement.Block;
import cs444.utils.Code;


class JoosMethodImpl implements JoosMethod, Cloneable
{
	private static int mg_identifierCounter = 0;
	
	private int m_identifier;
	private JoosObjectType m_parent;
	private JoosMethod m_super;
	private String m_name;
	private JoosType m_returnType;
	private JoosMethodScope m_scope;
	private Block m_body = null;
	private int m_permission = PERMISSION_PUBLIC;
	private boolean m_isFinal = false;
	
	private boolean m_isAbstract = false;
	private boolean m_isConstructor = false;
	private boolean m_isStatic = false;
	private boolean m_isNative = false;
	
	private Collection<JoosType> m_parameters;
	private Map<String, JoosVariable> m_variables;
	
	public JoosMethodImpl( JoosObjectType parent, String name, JoosType returnType, Set<String> modifiers )
	{
		m_identifier = mg_identifierCounter++;
		JoosMethod.ALL_METHODS.add( this );
		
		m_parent = parent;
		m_super = null;
		m_name = name;
		m_returnType = returnType;
		m_parameters = new ArrayList<JoosType>();
		m_variables = new TreeMap<String, JoosVariable>();
		setupScope( modifiers );
		setupPermission( modifiers );
		m_isFinal = modifiers.contains( "final" );
		m_isStatic = modifiers.contains( "static" );
		m_isNative = modifiers.contains( "native" );
	}
	private JoosMethodImpl()
	{
		m_super = null;
	}
	
	public int getIdentifier()
	{
		return m_identifier;
	}
	public String getName()
	{
		return m_name;
	}
	public JoosObjectType getSource()
	{
		return m_parent;
	}
	public boolean isConstructor()
	{
		return m_isConstructor;
	}
	public boolean isFinal()
	{
		return m_isFinal;
	}
	public boolean isStatic()
	{
		return m_isStatic;
	}
	public boolean isNative()
	{
		return m_isNative;
	}
	public String fullName()
	{
		return getName();
	}
	public JoosMethod getSuper()
	{
		return m_super;
	}
	public JoosType getReturnType()
	{
		return m_returnType;
	}
	public Collection<JoosType> getParameters()
	{
		return m_parameters;
	}
	public Map<String, JoosVariable> getVariables()
	{
		return m_variables;
	}
	public JoosMethodScope getScope()
	{
		// Lazily create the scope since not all of the parameters are defined when the method is created
		if( !isAbstract() && m_scope == null ) {
			m_scope = new JoosMethodScopeImpl( m_parent.asJoosClass(), this );
		}
		return m_scope;
	}
	public int getPermission()
	{
		return m_permission;
	}
	public boolean isAbstract()
	{
		return m_isAbstract;
	}
	public Block getBody()
	{
		return m_body;
	}
	public JoosMethod override( JoosMethod parent ) throws Exception
	{
		try {
			m_super = parent;
			if( m_super == null ) {
				return this;
			}
			if( getSource().equals( parent.getSource() ) ) {
				throw new Exception( "Cannot declare multiple methods with the same signature inside one class or interface" );
			} else if( parent.isFinal() ) {
				throw new Exception( "Cannot override a final method" );
			} else if( parent.getName().equals( "getClass" ) &&
				         parent.getSource().fullName().equals( "java.lang.Object2" ) &&
				         !getSource().fullName().equals( "java.lang.Object" ) ) {
				throw new Exception( "Cannot declare final method from java.lang.Object" );
			} else if( isStatic() && !parent.isStatic() ) {
				throw new Exception( "Static methods cannot override non-static methods" );
			} else if( !isStatic() && parent.isStatic() ) {
				throw new Exception( "Non-static methods cannot override static methods" );
			}
			if( !m_super.getReturnType().isSuperType( getReturnType() ) ) {
				throw new Exception( "Returns types do not match for overridden method " + m_name ); 
			}	else if( getPermission() < parent.getPermission() ) {
				if( isAbstract() && parent.isAbstract() && !parent.getSource().isSuperType( getSource() ) ) {
					// Then these are abstract methods takes from seperate sources so take the less restrictive permissions
					return copy( parent.getPermission() ).override( parent );
				} else {
					throw new Exception( "Cannot assign more restrictive permission to overridden method" );
				}
			}
			// TODO check throws
			if( m_super.isAbstract() ) {
				m_super = m_super.getSuper();
			}
		} catch( Exception e ) {
			throw new Exception( "Error trying to override " + parent + " with " + this, e );
		}
		return this;
	}
	/*
	 * Returns true if one or more parameters of this are more specific than parameters of method, and no parameters are less specific.
	 */
	public boolean isMoreSpecific( JoosMethod method )
	{
		Iterator<JoosType> m1It = getParameters().iterator();
		Iterator<JoosType> m2It = method.getParameters().iterator();
		boolean hasSubtype = false;
		
		while( m1It.hasNext() && m2It.hasNext() ) {
			JoosType t1 = m1It.next();
			JoosType t2 = m2It.next();
			if( t1.equals( t2 ) ) {
				continue;
			} else if( t1.isSuperType( t2 ) ) {
				return false;
			} else if( t2.isSuperType( t1 ) ) {
				hasSubtype = true;
			}
		}
		return hasSubtype;
	}

	public void setBody( Block body )
	{
		m_body = body;
	}
	public void addParameter( String name, JoosType type ) throws Exception
	{
		m_parameters.add( type );
		JoosVariable var = m_variables.get( name );
		if( var != null ) {
			JoosMethod methodSource = var.getOrigin().asJoosMethod();
			if( methodSource != null ) {
				throw new Exception( "A variable with the name '" + name + "' has already been declared." );
			}
		}
		var = new JoosVariableImpl( name, type, this );
		m_variables.put( name, var );
	}
	public void setConstructor()
	{
		m_isConstructor = true;
	}
	
	public JoosClass asJoosClass()
	{
		return null;
	}
	public JoosMethod asJoosMethod()
	{
		return this;
	}
	public JoosScope asJoosScope()
	{
		return null;
	}
	
	
	// Overridden Object Methods
	public boolean equals( Object o )
	{
		if( !(o instanceof JoosMethod) ) {
			return false;
		}
		JoosMethod other = (JoosMethod)o;
		if( !getName().equals( other.getName() ) ) {
			return false;
		} else if( isConstructor() != other.isConstructor() ) {
			return false;
		}
		return getParameters().equals( other.getParameters() );
	}
	public int hashCode()
	{
		int code = getName().hashCode() + getParameters().hashCode();
		if( isConstructor() ) {
			code += 1;
		}
		return code;
	}
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		switch( m_permission ) {
			case PERMISSION_PUBLIC:
				builder.append( "public " );
				break;
			case PERMISSION_PROTECTED:
				builder.append( "protected " );
				break;
			default:
				builder.append( "private " );
		}
		if( !isConstructor() ) {
			if( isAbstract() ) {
				builder.append( "abstract " );
			}
			builder.append( getReturnType().fullName() );
			builder.append( " " );
		}
		builder.append( getName() );
		builder.append( "(" );
		String sep = " ";
		for( Map.Entry<String, JoosVariable> parameter : getVariables().entrySet() ) {
			String name = parameter.getKey();
			JoosType type = parameter.getValue().getType();
			builder.append( sep );
			builder.append( type.fullName() );
			builder.append( " " );
			builder.append( name );
			sep = ", ";
		}
		builder.append( " ) { ... } (defined in " + m_parent.fullName() + ")" );
		return builder.toString();
	}



	private void setupScope( Set<String> modifiers )
	{
		JoosClass asClass = m_parent.asJoosClass();
		m_scope = null;
		if( modifiers.contains( "abstract" ) || asClass == null ) {
			m_isAbstract = true;
		} else {
			m_isAbstract = false;
		}
	}
	private void setupPermission( Set<String> modifiers )
	{
		if( modifiers.contains( "public" ) ) {
			m_permission = PERMISSION_PUBLIC;
		} else if( modifiers.contains( "protected" ) ) {
			m_permission = PERMISSION_PROTECTED;
		} else {
			m_permission = PERMISSION_PRIVATE;
		}
	}
	private JoosMethod copy( int permission )
	{
		JoosMethodImpl ret = null;
		try {
			ret = (JoosMethodImpl)clone();
			ret.m_permission = permission;
		} catch( CloneNotSupportedException cnse ) {
			// Should not occur
		}
		return ret;
	}
	public void genCode( Appendable out ) throws java.io.IOException
	{
		if( getBody() == null ) {
			return;
		}
		out.append( ";; " + this.toString() + "\n" );
		out.append( "global " + Code.makeLabel( this ) + "\n" );
		out.append( Code.makeLabel( this ) + ":\n" );
		
		// Set the stack pointer
		out.append( "push ebp\n" );
		out.append( "mov ebp, esp\n" );
		
		// The actual function code
		getBody().genCode( out );
		
		// Restore the stack pointers
		out.append( "mov esp, ebp\n" );
		out.append( "pop ebp\n" );
		
		// Return
		out.append( "ret\n\n" );
	}
}



