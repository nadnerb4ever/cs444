package cs444.typeLinker;


public class JoosPrimitiveType extends JoosTypeImpl implements JoosType
{
	public static JoosPrimitiveType NULL_TYPE = new JoosPrimitiveType( "nulltype" );
	public static JoosPrimitiveType VOID_TYPE = new JoosPrimitiveType( "void" )
	{
		public boolean isSuperType( JoosType other )
		{
			return false;
		}
	};
	public static JoosPrimitiveType BOOLEAN_TYPE = new JoosPrimitiveType( "boolean" );

	public static JoosPrimitiveType DOUBLE_TYPE = new JoosNumericType( "double" )
	{
		public boolean isSuperType( JoosType other )
		{
			return equals( other ) || FLOAT_TYPE.isSuperType( other );
		}
	};
		
	public static JoosPrimitiveType FLOAT_TYPE = new JoosNumericType( "float" )
	{
		public boolean isSuperType( JoosType other )
		{
			return equals( other ) || LONG_TYPE.isSuperType( other );
		}
	};
	
	public static JoosPrimitiveType LONG_TYPE = new JoosIntegralType( "long" )
	{
		public boolean isSuperType( JoosType other )
		{
			return equals( other ) || INT_TYPE.isSuperType( other );
		}
	};
	
	public static JoosPrimitiveType INT_TYPE = new JoosIntegralType( "int" )
	{
		public boolean isSuperType( JoosType other )
		{
			return equals( other ) || CHAR_TYPE.isSuperType( other ) || SHORT_TYPE.isSuperType( other );
		}
	};
	
	public static JoosPrimitiveType SHORT_TYPE = new JoosIntegralType( "short" )
	{
		public boolean isSuperType( JoosType other )
		{
			return equals( other ) || BYTE_TYPE.isSuperType( other );
		}
	};
	
	public static JoosPrimitiveType BYTE_TYPE = new JoosIntegralType( "byte" );
	
	public static JoosPrimitiveType CHAR_TYPE = new JoosIntegralType( "char" );
	
	
	
	
	public static JoosPrimitiveType[] ALL = {
		INT_TYPE,
		CHAR_TYPE,
		BOOLEAN_TYPE,
		BYTE_TYPE,
		SHORT_TYPE,
		LONG_TYPE,
		FLOAT_TYPE,
		VOID_TYPE,
		DOUBLE_TYPE,
		NULL_TYPE
	};
	
	private String m_name;
	
	private JoosPrimitiveType( String name )
	{
		m_name = name;
	}
	
	public String name()
	{
		return m_name;
	}
	public String fullName()
	{
		return name();
	}
	public boolean isSimilar( JoosType other )
	{
		return equals( other );
	}
	public boolean isSuperType( JoosType other )
	{
		return equals( other );
	}
	public JoosPrimitiveType asPrimitiveType()
	{
		return this;
	}
	public JoosNumericType asNumericType()
	{
		return null;
	}
	
	public String toString()
	{
		return fullName();
	}
	
	public static class JoosNumericType extends JoosPrimitiveType
	{
		public JoosNumericType( String name )
		{
			super( name );
		}
		public JoosNumericType asNumericType()
		{
			return this;
		}
		public JoosIntegralType asIntegralType()
		{
			return null;
		}
	}
	public static class JoosIntegralType extends JoosNumericType
	{
		public JoosIntegralType( String name )
		{
			super( name );
		}
		public JoosIntegralType asIntegralType()
		{
			return this;
		}
	}
}




