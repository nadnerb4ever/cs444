package cs444.typeLinker;

public interface JoosMethodScope extends JoosScope
{
	public JoosClass getParent();
	public JoosMethod getMethod();
}


