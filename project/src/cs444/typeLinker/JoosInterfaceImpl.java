package cs444.typeLinker;

import java.util.*;


class JoosInterfaceImpl extends JoosObjectTypeImpl implements JoosInterface
{

	private Set<JoosInterface> m_parent;
	
	protected JoosInterfaceImpl( String packageName, String name )
	{
		super( packageName, name );
		m_parent = new HashSet<JoosInterface>();
	}

	@Override
	public Set<JoosInterface> getExtends()
	{
		return m_parent;
	}

	@Override
	public void addExtends( JoosInterface extend )
	{
		m_parent.add( extend );		
	}


	@Override
	protected boolean isSuperType( JoosObjectType other )
	{
		return isSuperType( other.asJoosInterface() ) || isSuperType( other.asJoosClass() );
	}

	protected boolean isSuperType( JoosInterface other )
	{
		if( other == null ) {
			return false;
		} else if( equals( other ) ) {
			return true;
		}
		for( JoosInterface candidate : other.getExtends() ) {
			if( isSuperType( candidate ) ) {
				return true;
			}
		}
		return false;
	}
	protected boolean isSuperType( JoosClass other )
	{
		if( other == null ) {
			return false;
		}
		if( isSuperType( other.getExtends() ) ) {
			return true;
		}
		for( JoosInterface candidate : other.getImplements() ) {
			if( isSuperType( candidate ) ) {
				return true;
			}
		}
		return false;
	}
	public boolean isAbstract()
	{
		return true;
	}
	
	public void inherit() throws Exception
	{
		for( JoosInterface jInterface : getExtends() ) {
			inheritMethods( jInterface );
		}
	}
	
	public JoosInterface asJoosInterface()
	{
		return this;
	}
	public Collection<JoosObjectType> getParents()
	{
		ArrayList<JoosObjectType> ret = new ArrayList<JoosObjectType>();
		ret.addAll( getExtends() );
		return ret;
	}
	
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append( "public interface " );
		builder.append( fullName() );
		builder.append( join( getExtends(), " extends ", ", ", "", false ) );
		builder.append( "\n{" );
		for( JoosMethod method : getMethods() ) {
			builder.append( "\n\t" );
			builder.append( method.toString() );
		}
		builder.append( "\n}" );
		return builder.toString();
	}
}
