package cs444.typeLinker;


public class JoosArrayType extends JoosTypeImpl implements JoosType
{
	private JoosType m_inner;
	
	protected JoosArrayType( JoosType inner )
	{
		m_inner = inner;
	}
	public JoosType getInnerType()
	{
		return m_inner;
	}
	
	// Overridden Interface methods
	public boolean isSimilar( JoosType other )
	{
		JoosArrayType ary = other.asArrayType();
		return ary != null && m_inner.isSimilar( ary.getInnerType() );
	}
	public boolean isSuperType( JoosType other )
	{
		if( other.equals( JoosPrimitiveType.NULL_TYPE ) ) {	
			return true;
		}
		JoosArrayType ary = other.asArrayType();
		return ary != null && m_inner.isSuperType( ary.getInnerType() );
	}
	public JoosArrayType arrayOf()
	{
		return null; // Multidimensional arrays not allowed in joos
	}
	public String fullName()
	{
		return m_inner.fullName() + "[]";
	}
	public JoosArrayType asArrayType()
	{
		return this;
	}
	
	// Overridden Object Methods
	public String toString()
	{
		return fullName();
	}
	public boolean equals( Object o )
	{
		if( o == null || !(o instanceof JoosArrayType) ) {
			return false;
		}
		JoosArrayType other = (JoosArrayType)o;
		return m_inner.equals( other.getInnerType() );
	}
	public int hashCode()
	{
		return m_inner.hashCode() + 7;
	}
}



