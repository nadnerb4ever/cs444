package cs444.typeLinker;

import java.util.*;
import cs444.ast.statement.Block;


public interface JoosClass extends JoosObjectType
{
	public Set<JoosInterface> getImplements();
	public JoosClass getExtends();
	public JoosScope getClassScope();
	public JoosScope getStaticScope();
	public Block getInitializer();
	public Block getStaticInitializer();
	
	public void addField( String name, JoosType type, Set<String> modifiers ) throws Exception;
	public Map<String, JoosField> getFields();
	public JoosField getField( String name );
	public JoosStaticType staticTypeOf();
	
	public void addImplements( JoosInterface implement );
	public void setExtends( JoosClass extend );

	public static final JoosClass STRING_TYPE = new JoosClassImpl("java.lang", "String", false, true);
	public static final JoosClass OBJECT_TYPE = new JoosClassImpl("java.lang", "Object", false, false);	
	
	public void genCode( Appendable out, int numTypes ) throws java.io.IOException;
}



