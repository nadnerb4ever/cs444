package cs444.typeLinker.classBody;

import cs444.typeLinker.*;
import cs444.typeLinker.localVariableList.*;
import cs444.parser.*;
import cs444.parser.ParseTree.*;
import cs444.ast.ASTBuilder;
import java.util.*;


public class MethodListBuilder implements NodeHandler
{
	private Environment m_env;
	private JoosObjectType m_jObject;
	
	private JoosMethod m_method = null;
	

	public MethodListBuilder( Environment env, JoosObjectType jObject )
	{
		m_env = env;
		m_jObject = jObject;
	}
	
	public boolean onEnterNode( ParseTreeNode node ) throws HandlerException
	{
		try {
			if( node.getType().equals( "Block" ) ) {
				// First build the scopes
				NodeHandler scopeHandler = new LocalVariableListBuilder( m_method.getScope(), m_env );
				ParseTreeTraverser traverser = new ParseTreeTraverser( scopeHandler );
				traverser.traverse( node.getChildren()[1] );
				/*
				// Next parse the statements and expressions
				NodeHandler statementHandler = new ASTBuilder( m_env, m_method );
				traverser = new ParseTreeTraverser( statementHandler );
				traverser.traverse( node );
				*/
				node.annotate( m_method );
				return false;
			} else if( node.getType().equals( "ConcreteMethodDeclaration" ) || node.getType().equals( "AbstractMethodDeclaration" ) ) {
				ParseTreeNode[] children = node.getChildren();
				
				JoosType returnType = m_env.lookup( children[1].toString() );
				if( !children[2].toString().equals( "" ) ) {
					returnType = returnType.arrayOf();
				}
				String methodName = children[3].toString();
				Set<String> modifiers = children[0].asSet( 0 );

				m_method = m_jObject.createMethod( methodName, returnType, modifiers );
				/*
				TODO
				if( exceptionNodes.length == 2 ){
					JoosType exceptionType = m_env.lookup( exceptionNodes[1].toString() );
				}
				*/
			} else if( node.getType().equals( "ConstructorDeclaration" ) ) {
				ParseTreeNode[] children = node.getChildren();
				
				JoosType returnType = m_env.lookup( "void" );
				String methodName = m_jObject.shortName();
				Set<String> modifiers = children[0].asSet( 0 );
				
				m_method = m_jObject.createMethod( methodName, returnType, modifiers );
				m_method.setConstructor();
			} else if( node.getType().equals( "Parameter" ) ) {
				ParseTreeNode[] children = node.getChildren();
				JoosType type = m_env.lookup( children[0].toString() );
				if( !children[1].toString().equals( "" ) ) {
					type = type.arrayOf();
				}
				String name = children[2].toString();
				m_method.addParameter( name, type );
			}
		} catch( Exception e ) {
			throw new HandlerException( e.getMessage(), e );
		}
		return true;
	}
	
	public void onExitNode( ParseTreeNode node ) throws HandlerException
	{
		try {
			if( node.getType().equals( "AbstractMethodDeclaration" ) ||
			    node.getType().equals( "ConcreteMethodDeclaration" ) ||
			    node.getType().equals( "ConstructorDeclaration" ) ) {
				m_jObject.addMethod( m_method );
				m_method = null;
			}
		} catch( Exception e ) {
			throw new HandlerException( e.getMessage(), e );
		}
	}
	
}
