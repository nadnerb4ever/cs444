package cs444.typeLinker.classBody;

import cs444.typeLinker.*;
import cs444.parser.*;
import cs444.parser.ParseTree.*;

import java.util.Set;


public class FieldListBuilder implements NodeHandler
{
	private Environment m_env;
	private JoosClass m_jClass;
	

	public FieldListBuilder( Environment env, JoosClass jClass )
	{
		m_env = env;
		m_jClass = jClass;
	}
	
	public boolean onEnterNode( ParseTreeNode node ) throws HandlerException
	{
		try {
			if( node.getType().equals( "FieldDeclaration" ) ) {
				ParseTreeNode[] children = node.getChildren();
				String name = children[3].getChildren()[0].toString();
				Set<String> modifiers = children[0].asSet( 0 );
				JoosType type = m_env.lookup( children[1].toString() );
			
				if( !children[2].toString().equals( "" ) ) {
					type = type.arrayOf();
				}
				
				m_jClass.addField( name, type, modifiers );
				
				return false;
			}
		} catch( Exception e ) {
			throw new HandlerException( e.getMessage(), e );
		}
		return true;
	}
	
	public void onExitNode( ParseTreeNode node ) throws HandlerException
	{
	}
	
}
