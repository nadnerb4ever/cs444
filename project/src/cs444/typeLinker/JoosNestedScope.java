package cs444.typeLinker;



public interface JoosNestedScope extends JoosScope
{
	public JoosScope getParent();
}


