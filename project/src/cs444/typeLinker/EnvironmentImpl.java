package cs444.typeLinker;

import java.util.*;


class EnvironmentImpl implements Environment
{
	private JoosObjectType m_mainType;
	private Map<String, Set<JoosObjectType>> m_packages;
	private Map<String, JoosPrimitiveType> m_primitives;
	private Map<String, JoosObjectType> m_types;
	private Map<String, JoosObjectType> m_imports;
	private Map<String, JoosObjectType> m_defaultImports;
	private Map<String, JoosObjectType> m_wildImports;
	private Set<String> m_wildImportDuplicates;
	
	protected EnvironmentImpl( JoosObjectType mainType )
	{
		m_mainType = mainType;
		m_packages = new TreeMap<String, Set<JoosObjectType>>();
		m_primitives = new TreeMap<String, JoosPrimitiveType>();
		m_types = new TreeMap<String, JoosObjectType>();
		m_imports = new TreeMap<String, JoosObjectType>();
		m_defaultImports = new TreeMap<String, JoosObjectType>();
		m_wildImports = new TreeMap<String, JoosObjectType>();
		m_wildImportDuplicates = new TreeSet<String>();
		addPrimitiveTypes();
	}



	// Interface methods
	public JoosType lookup( String name ) throws Exception
	{
		JoosType ret = null;
		if( m_primitives.containsKey( name ) ) {
			ret = m_primitives.get( name );
		} else if( m_imports.containsKey( name ) ) {
			ret = m_imports.get( name );
		} else if( m_types.containsKey( name ) ) {
			// Check that fully qualified names do not contain prefixes which are also fully qualified type names.
			int idx = name.indexOf( '.' );
			while( idx != -1 ) {
				String prefix = name.substring( 0, idx );
				JoosType other = null;
				try {
					other = lookup( prefix );
				} catch( Exception e ) {
				}
				if( other != null ) {
					throw new Exception( "The prefix '" + prefix + "' of type name '" + name + "' is also a fully qualified type name" );
				}
				idx = name.indexOf( '.', idx + 1 );
			}
			// Get the actual type
			ret = m_types.get( name );
		} else if( m_defaultImports.containsKey( name ) ) {
			ret = m_defaultImports.get( name );
		} else if( m_wildImports.containsKey( name ) ) {
			if( m_wildImportDuplicates.contains( name ) ) {
				throw new Exception( "Class name '" + name + "' is ambiguous" );
			}
			ret = m_wildImports.get( name );
		} else {
			throw new Exception( "There is no matching type with name '" + name + "'" );
		}
		return ret;
	}
	public void doImport( String canonicalName ) throws Exception
	{
		if( !m_types.containsKey( canonicalName ) ) {
			throw new Exception( "Invalid class name specified in import statement: " + canonicalName );
		}
		JoosObjectType jObject = m_types.get( canonicalName );
		if( jObject.shortName().equals( m_mainType.shortName() ) && !jObject.fullName().equals( m_mainType.fullName() ) ) {
			throw new Exception( "Import type clashes with type defined in file: " + m_mainType.fullName() );
		}
		if( m_imports.containsKey( jObject.shortName() ) ) {
			JoosObjectType other = m_imports.get( jObject.shortName() );
			if( !other.equals( jObject ) ) {
				throw new Exception( "Class with name '" + jObject.shortName() + "' has already been imported" );
			}
		}
		m_imports.put( jObject.shortName(), jObject );
	}
	public void doImportStar( String packageName ) throws Exception
	{
		if( !m_packages.containsKey( packageName ) ) {
			throw new Exception( "Invalid package name specified in import statement: " + packageName );
		}
		Set<JoosObjectType> set = m_packages.get( packageName );
		for( JoosObjectType jObject : set ) {
			String name = jObject.shortName();
			JoosObjectType other = m_wildImports.get( name );
			if( other != null ) {
				// It is ok to import the same package twice (for instance specifying "import java.lang.*;")
				if( !jObject.fullName().equals( other.fullName() ) ) {
					m_wildImportDuplicates.add( name );
				}
			} else {
				m_wildImports.put( name, jObject );
			}
		}
	}
	public void doImportDefault( String packageName ) throws Exception
	{
		if( packageName == null ) {
			packageName = "$default";
		} else if( !m_packages.containsKey( packageName ) ) {
			throw new Exception( "Invalid package name specified in import statement: " + packageName );
		}
		Set<JoosObjectType> set = m_packages.get( packageName );
		for( JoosObjectType jObject : set ) {
			String name = jObject.shortName();
			m_defaultImports.put( name, jObject );
		}
	}
	
	
	// Package methods
	protected void add( String packageName, JoosObjectType jObject )
	{
		Set<JoosObjectType> set = m_packages.get( packageName );
		if( set == null ) {
			set = new TreeSet<JoosObjectType>();
			m_packages.put( packageName, set );
		}
		if( jObject != null ) {
			set.add( jObject );
		}
	}
	protected void add( JoosObjectType jObject )
	{
		String packageName = jObject.packageName();
		if( packageName == null ) {
			packageName = "$default";
		}
		m_types.put( packageName + "." + jObject.shortName(), jObject );
		add( packageName, jObject );
		// Create the subpackages if they don't already exist
		int idx = packageName.indexOf( '.' );
		while( idx >= 0 ) {
			String subPackageName = packageName.substring( 0, idx );
			add( subPackageName, null );
			idx = packageName.indexOf( '.', idx + 1 );
		}
	}
	protected void addAll( Collection<? extends JoosObjectType> collection )
	{
		for( JoosObjectType jObject : collection ) {
			add( jObject );
		}
	}
	
	// Private helper methods
	private void addPrimitiveTypes()
	{
		for( JoosPrimitiveType jPrimType : JoosPrimitiveType.ALL ) {
			m_primitives.put( jPrimType.name(), jPrimType );
		}
	}
}


