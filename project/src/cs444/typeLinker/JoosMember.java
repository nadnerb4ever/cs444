package cs444.typeLinker;


public interface JoosMember
{
	public static final int PERMISSION_PRIVATE = 0;
	public static final int PERMISSION_PROTECTED = 1;
	public static final int PERMISSION_PUBLIC = 2;
	
	public int getPermission();
	public boolean isStatic();
	public JoosObjectType getSource();
}

