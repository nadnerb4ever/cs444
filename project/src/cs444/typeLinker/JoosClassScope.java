package cs444.typeLinker;


public interface JoosClassScope extends JoosScope
{
	public JoosClass getParent();
}


