package cs444.typeLinker;


public interface JoosVariable
{
	public JoosType getType();
	public VariableSource getOrigin();
	public boolean isInitialized();
	public void setInitialized();
}


