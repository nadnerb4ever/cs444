package cs444.typeLinker;

import java.util.*;


public interface CompilationEnvironment
{
	public Set<JoosClass> getClasses();
	public Set<JoosInterface> getInterfaces();
	public Environment getInstance( JoosObjectType mainType ) throws Exception;

	public JoosClass addClass( String packageName, String className, boolean isAbstract, boolean isFinal ) throws Exception;
	public JoosInterface addInterface( String packageName, String interfaceName ) throws Exception;
}


