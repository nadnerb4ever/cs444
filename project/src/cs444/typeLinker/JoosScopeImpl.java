package cs444.typeLinker;

import java.util.*;


abstract class JoosScopeImpl implements JoosScope
{
	private Map<String, JoosVariable> m_variables;
	private Set<JoosNestedScope> m_nestedScopes;
	
	JoosScopeImpl()
	{
		m_variables = new TreeMap<String, JoosVariable>();
		m_nestedScopes = new HashSet<JoosNestedScope>();
	}
	
	
	public JoosClass asJoosClass()
	{
		return null;
	}
	public JoosMethod asJoosMethod()
	{
		return null;
	}
	public JoosScope asJoosScope()
	{
		return this;
	}
	
	public JoosVariable lookup( String name ) throws Exception
	{
		if( !m_variables.containsKey( name ) ) {
			throw new Exception( "there is no variable with the name '" + name + "' in this scope" );
		}
		return m_variables.get( name );
	}

	public void inherit( Map<String, ? extends JoosVariable> variables )
	{
		m_variables.putAll( variables );
	}
	// Only should be used when inheriting selectively from other sources (static fields)
	protected void inheritVariable( String name, JoosVariable var )
	{
		m_variables.put( name, var );
	}
	public void addJoosVariable( JoosType type, String name ) throws Exception
	{
		JoosVariable var = m_variables.get( name );
		if( var != null ) {
			VariableSource origin = var.getOrigin();
			if( origin.asJoosScope() != null || origin.asJoosMethod() != null ) {
				throw new Exception( "A variable by the name '" + name + "' already exists in this scope" );
			}
		}
		var = new JoosVariableImpl( name, type, this );
		m_variables.put( name, var );
	}
	public JoosNestedScope addNestedScope()
	{
		JoosNestedScope scope = new JoosNestedScopeImpl( this, m_variables );
		return scope;
	}
	
	public String fullName()
	{
		return "scope";
	}
	
	
	public JoosClassScope asClassScope()
	{
		return null;
	}
	public JoosMethodScope asMethodScope()
	{
		return null;
	}
	public JoosNestedScope asNestedScope()
	{
		return null;
	}
}


