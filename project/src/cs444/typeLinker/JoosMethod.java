package cs444.typeLinker;

import java.util.*;
import cs444.ast.statement.Block;


public interface JoosMethod extends VariableSource, JoosMember
{
	public static Collection<JoosMethod> ALL_METHODS = new ArrayList<JoosMethod>();

	public int getIdentifier();
	public String getName();
	public JoosMethod getSuper();
	public JoosType getReturnType();
	public Collection<JoosType> getParameters();
	public Map<String, JoosVariable> getVariables();
	public JoosMethodScope getScope();
	public int getPermission();
	public boolean isConstructor();
	public boolean isFinal();
	public boolean isNative();
	public Block getBody();
	/*
	 * Returns true if one or more parameters of this are more specific than parameters of method, and no parameters are less specific.
	 */
	public boolean isMoreSpecific( JoosMethod method );
	
	public boolean isAbstract();
	public JoosMethod override( JoosMethod parent ) throws Exception;
	public void setConstructor();

	public void setBody( Block body );
	public void addParameter( String name, JoosType type ) throws Exception;
	public void genCode( Appendable out ) throws java.io.IOException;
}


