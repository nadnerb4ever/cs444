package cs444.typeLinker;


public interface JoosType extends Comparable<JoosType>
{
	public boolean isSimilar( JoosType other );
	public boolean isSuperType( JoosType other );
	public JoosArrayType arrayOf();
	public String fullName();
	
	public JoosObjectType asObjectType();
	public JoosPrimitiveType asPrimitiveType();
	public JoosArrayType asArrayType();
	public JoosStaticType asStaticType();
}



