package cs444.typeLinker;

import java.util.Map;


class JoosNestedScopeImpl extends JoosScopeImpl implements JoosNestedScope
{
	private JoosScope m_parent;

	JoosNestedScopeImpl( JoosScope parent, Map<String, JoosVariable> variables )
	{
		m_parent = parent;
		inherit( variables );
	}
	
	public JoosScope getParent()
	{
		return m_parent;
	}
	
	public JoosNestedScope asNestedScope()
	{
		return this;
	}
}
