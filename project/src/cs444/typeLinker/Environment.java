package cs444.typeLinker;


public interface Environment
{
	public JoosType lookup( String name ) throws Exception;
	public void doImport( String canonicalName ) throws Exception;
	public void doImportStar( String packageName ) throws Exception;
	public void doImportDefault( String packageName ) throws Exception;
}


