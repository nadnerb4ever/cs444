package cs444.typeLinker;


public interface VariableSource
{
	public JoosClass asJoosClass();
	public JoosMethod asJoosMethod();
	public JoosScope asJoosScope();
	
	public String fullName();
}


