package cs444.typeLinker;


class JoosFieldImpl extends JoosVariableImpl implements JoosField
{
	protected int m_permission;
	protected boolean m_isStatic;
	
	JoosFieldImpl( String name, JoosType type, VariableSource origin, int permission, boolean isStatic )
	{
		super( name, type, origin );
		m_permission = permission;
		m_isStatic = isStatic;
	}
	public int getPermission()
	{
		return m_permission;
	}
	public boolean isStatic()
	{
		return m_isStatic;
	}
	public JoosObjectType getSource()
	{
		return getOrigin().asJoosClass();
	}
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append( permissionAsString( getPermission() ) );
		if( isStatic() ) {
			builder.append( " static" );
		}
		builder.append( " " );
		builder.append( getType().toString() );
		builder.append( " " );
		builder.append( m_name );
		builder.append( "; (defined in " );
		builder.append( getSource() );
		builder.append( ")" );
		return builder.toString();
	}
	public static String permissionAsString( int permission )
	{
		String ret;
		switch( permission ) {
			case PERMISSION_PRIVATE:
				ret = "private";
				break;
			case PERMISSION_PROTECTED:
				ret = "protected";
				break;
			default:
				ret = "public";
		}
		return ret;
	}
}


