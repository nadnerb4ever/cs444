package cs444.typeLinker;

import java.util.*;


public abstract class JoosObjectTypeImpl extends JoosTypeImpl implements JoosObjectType
{
	private static int mg_identifierCounter = 0;

	protected String m_packageName;
	protected String m_name;
	protected String m_fullName;
	
	protected int m_identifier;
	protected Set<JoosMethod> m_methods;
	protected Map<String, Set<JoosMethod>> m_methodNames;
	protected Map<JoosMethod, JoosMethod> m_signatures;
	protected Map<JoosMethod, Integer> m_methodCardinality;

	protected JoosObjectTypeImpl( String packageName, String name )
	{
		m_identifier = mg_identifierCounter++;
		m_packageName = packageName;
		m_name = name;
		if( packageName != null ) {
			m_fullName = packageName + "." + name;
		} else {
			m_fullName = m_name;
		}
		m_methods = new HashSet<JoosMethod>();
		m_methodNames = new TreeMap<String, Set<JoosMethod>>();
		m_signatures = new HashMap<JoosMethod, JoosMethod>();
		m_methodCardinality = new HashMap<JoosMethod, Integer>();
	}
	
	// Interface Methods
	public int getIdentifier()
	{
		return m_identifier;
	}
	public String packageName()
	{
		return m_packageName;
	}
	public String shortName()
	{
		return m_name;
	}
	public String fullName()
	{
		return m_fullName;
	}
	public JoosMethod getMatchingMethod( JoosMethod method )
	{
		return m_signatures.get( method );
	}
	public Set<JoosMethod> getMethods()
	{
		return m_methods;
	}
	public Set<JoosMethod> getMethods( String name )
	{
		return m_methodNames.get( name );
	}
	public List<JoosMethod> getMethods( String name, Collection<JoosType> args, boolean staticMethods )
	{
		List<JoosMethod> ret = new ArrayList<JoosMethod>();

topLoop:
		for( JoosMethod method : getMethods( name ) ) {
			if( method.isStatic() != staticMethods ) {
				continue;
			}
			Iterator<JoosType> paramsIt = method.getParameters().iterator();
			Iterator<JoosType> argsIt = args.iterator();
			
			while( paramsIt.hasNext() && argsIt.hasNext() ) {
				if( !paramsIt.next().isSuperType( argsIt.next() ) ) {
					continue topLoop;
				}
			}
			if( paramsIt.hasNext() || argsIt.hasNext() ) {
				continue;
			}
			ret.add( method );
		}
		return ret;
	}
	public JoosMethod createMethod( String name, JoosType returnType, Set<String> modifiers ) throws Exception
	{
		JoosMethod method = new JoosMethodImpl( this, name, returnType, modifiers );
		//addMethod( method );
		return method;
	}
	public void addMethod( JoosMethod method ) throws Exception
	{
		if( m_methods.contains( method ) ) {
			JoosMethod original = m_signatures.get( method );
			// If it is just a multiply defined interface method then ignore it
			if( equals( method.getSource() ) || method.getSource().asJoosClass() != null ) {
				method = method.override( original );
			} else if( original.getReturnType().isSuperType( method.getReturnType() ) ) {
				return;
			} else if( !method.getReturnType().isSuperType( original.getReturnType() ) ) {
				throw new Exception( "An Object Type cannot inherit two identicle methods with unrelated return types" );
			}
		}
		
		m_methods.remove( method );
		m_methods.add( method );
		Set<JoosMethod> set = m_methodNames.get( method.getName() );
		if( set == null ) {
			set = new HashSet<JoosMethod>();
			m_methodNames.put( method.getName(), set );
		}
		set.remove( method );
		set.add( method );
		m_signatures.put( method, method );
		if( !m_methodCardinality.containsKey( method ) ) {
			m_methodCardinality.put( method, m_methods.size() );
		}
	}
	public int cardinality( JoosMethod method )
	{
		return m_methodCardinality.get( method );
	}
	public void check() throws Exception { }
	protected void inheritMethods( JoosObjectType other ) throws Exception
	{
		for( JoosMethod method : other.getMethods() ) {
			// Skip over private methods
			if( method.getPermission() != 0 ) {
				addMethod( method );
			}
		}
	}
	
	public boolean isSimilar( JoosType other )
	{
		return isSuperType( other ) || other.isSuperType( this );
	}
	
	public boolean isSuperType( JoosType other )
	{
		if( other.equals( JoosPrimitiveType.NULL_TYPE ) ) {
			return true;
		} else if( other.asArrayType() != null ) {
			if( fullName().equals( "java.lang.Object" ) ||
			    fullName().equals( "java.lang.Cloneable" ) ||
			    fullName().equals( "java.io.Serializable" ) ) {
				return true;
			}
		}
		JoosObjectType asObject = other.asObjectType();
		return asObject != null && isSuperType( asObject );
	}
	
	public boolean isFinal(){
		return false;
	}
	
	public JoosObjectType asObjectType()
	{
		return this;
	}
	
	public JoosClass asJoosClass()
	{
		return null;
	}
	public JoosInterface asJoosInterface()
	{
		return null;
	}
	
	// Unimplemented methods
	protected abstract boolean isSuperType( JoosObjectType other );
	
	
	protected static String join( Collection<? extends JoosType> c, String pre, String sep, String post, boolean showEmpty )
	{
		if( !showEmpty && c.size() == 0 ) {
			return "";
		}
		String cSep = pre;
		StringBuilder builder = new StringBuilder();
		for( JoosType type : c ) {
			builder.append( cSep );
			builder.append( type.fullName() );
			cSep = sep;
		}
		builder.append( post );
		return builder.toString();
	}
}




