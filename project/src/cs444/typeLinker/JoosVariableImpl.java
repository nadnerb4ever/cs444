package cs444.typeLinker;


class JoosVariableImpl implements JoosVariable
{
	protected String m_name;
	protected JoosType m_type;
	protected VariableSource m_origin;
	protected boolean m_isInitialized;

	JoosVariableImpl( String name, JoosType type, VariableSource origin )
	{
		m_name = name;
		m_type = type;
		m_origin = origin;
		m_isInitialized = false;
	}

	public JoosType getType()
	{
		return m_type;
	}
	public VariableSource getOrigin()
	{
		return m_origin;
	}

	@Override
	public boolean isInitialized() {
		return m_isInitialized;
	}

	@Override
	public void setInitialized() {
		m_isInitialized = true;
	}
}


