package cs444.typeLinker;

import java.util.*;
import cs444.parser.*;
import cs444.parser.ParseTree.*;

import cs444.typeLinker.classList.*;
import cs444.typeLinker.classBody.*;

import cs444.ast.ASTBuilder;
import cs444.ast.FieldExpressionBuilder;


public class Linker
{
	Collection<ParseTree> m_trees;
	Map<String, JoosFile> m_joosFiles;
	CompilationEnvironment m_env;
	
	public Linker( Collection<ParseTree> trees )
	{
		m_trees = trees;
		m_joosFiles = new TreeMap<String, JoosFile>();
		m_env = new CompilationEnvironmentImpl();
	}
	
	public void link()
	{
		try {
			buildEnv();
			buildInheritance();
			buildStructure();
			buildMethods();
			buildFields();
			checkObjects();
		} catch( Exception e ) {
			Throwable t = e;
			while( t != null ) {
				//System.err.println( t.getMessage() );
				t.printStackTrace();
				t = t.getCause();
			}
			System.exit( 42 );
		}
	}
	
	public Set<JoosClass> getClasses()
	{
		return m_env.getClasses();
	}
	public CompilationEnvironment getEnv()
	{
		return m_env;
	}
	private void buildEnv() throws Exception
	{
		ClassListBuilder handler = new ClassListBuilder( m_env );
		ParseTreeTraverser traverser = new ParseTreeTraverser( handler );
		for( ParseTree tree : m_trees ) {
			traverser.traverse( tree );
			JoosObjectType type = handler.getType();
			JoosFile file = new JoosFile( tree, type );
			if( m_joosFiles.containsKey( type.fullName() ) ) {
				throw new Exception( "Cannot define multiple types with the same name" );
			}
			m_joosFiles.put( type.fullName(), file );
		}
	}
	private void buildInheritance() throws Exception
	{
		for( JoosFile file : m_joosFiles.values() ) {
			Environment env = m_env.getInstance( file.type() );
			file.env( env );
			ClassStructureBuilder handler = new ClassStructureBuilder( env, file.type() );
			ParseTreeTraverser traverser = new ParseTreeTraverser( handler );
			traverser.traverse( file.tree() );
		}
	}
	private void buildStructure() throws Exception
	{
		Set<JoosObjectType> complete = new TreeSet<JoosObjectType>();
		Set<JoosObjectType> pending = new TreeSet<JoosObjectType>();
		Stack<JoosObjectType> stack = new Stack<JoosObjectType>();
		for( JoosInterface jInterface : m_env.getInterfaces() ) {
			if( complete.contains( jInterface ) ) {
				continue;
			}
			stack.push( jInterface );
iLoop:			
			while( stack.size() > 0 ) {
				JoosInterface current = stack.peek().asJoosInterface();
				pending.remove( current );
				for( JoosInterface depend : current.getExtends() ) {
					if( depend != null && !complete.contains( depend ) ) {
						pending.add( current );
						if( pending.contains( depend ) ) {
							throw new Exception( "Cannot have circular dependencies in interfaces" );
						}
						stack.push( depend );
						continue iLoop;
					}
				}
				buildStructure( current );
				complete.add( current );
				stack.pop();
			}
		}
		for( JoosClass jClass : m_env.getClasses() ) {
			if( complete.contains( jClass ) ) {
				continue;
			}
			stack.push( jClass );
cLoop:			
			while( stack.size() > 0 ) {
				JoosClass current = stack.peek().asJoosClass();
				pending.remove( current );
				JoosClass depend = current.getExtends();
				if( depend != null && !complete.contains( depend ) ) {
					pending.add( current );
					if( pending.contains( depend ) ) {
						throw new Exception( "Cannot have circular dependencies in classes" );
					}
					stack.push( depend );
					continue cLoop;
				}
				buildStructure( current );
				complete.add( current );
				stack.pop();
			}
		}
	}
	private void buildStructure( JoosObjectType jObject ) throws Exception
	{
		jObject.inherit();
		JoosFile file = m_joosFiles.get( jObject.fullName() );
		ParseTree tree = file.tree();
		Environment env = file.env();
		JoosClass jClass = jObject.asJoosClass();
		if( jClass != null ) {
			tree.traverseWith( new FieldListBuilder( env, jClass ) );
		}
		tree.traverseWith( new MethodListBuilder( env, jObject ) );
		
		//System.err.println( "-- Completed parsing of: --\n" + jObject.toString() );
	}
	private void buildMethods() throws HandlerException
	{
		for( JoosFile file : m_joosFiles.values() ) {
			NodeHandler handler = new ASTBuilder.Helper( file.env() );
			ParseTreeTraverser traverser = new ParseTreeTraverser( handler );
			traverser.traverse( file.tree() );
		}
	}
	private void buildFields() throws HandlerException
	{
		for( JoosFile file : m_joosFiles.values() ) {
			JoosClass jClass = file.type().asJoosClass();
			if( jClass == null ) {
				continue;
			}
			NodeHandler handler = new FieldExpressionBuilder( file.env(), jClass );
			ParseTreeTraverser traverser = new ParseTreeTraverser( handler );
			traverser.traverse( file.tree() );
		}
	}
	private void checkObjects() throws Exception
	{
		for( JoosFile file : m_joosFiles.values() ) {
			file.type().check();
		}
	}
}



class JoosFile
{
	private ParseTree m_tree;
	private Environment m_env;
	private JoosObjectType m_type;
	
	protected JoosFile( ParseTree tree, JoosObjectType type )
	{
		m_tree = tree;
		m_env = null;
		m_type = type;
	}
	
	protected ParseTree tree()
	{
		return m_tree;
	}
	protected Environment env()
	{
		return m_env;
	}
	protected void env( Environment env )
	{
		m_env = env;
	}
	protected JoosObjectType type()
	{
		return m_type;
	}
}

