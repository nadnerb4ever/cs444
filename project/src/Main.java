import java.util.*;
import java.io.*;


import cs444.lex.*;
import cs444.lexer.*;
import cs444.parser.*;
import cs444.shared.*;
import cs444.weeder.*;
import cs444.typeLinker.*;
import cs444.codeGen.*;



public class Main
{
	public static final String DFA_FILE = "gen/res/joos.dfa";
	public static final String LR1_FILE = "gen/res/joos.lr1";
	
	public static void main( String[] args ) throws java.io.IOException
	{
		Collection<String> filenames = new ArrayList<String>();
		Collection<ParseTree> trees = new ArrayList<ParseTree>();
		boolean printTokens = false;
		boolean printParseTree = false;
		boolean parseOnly = false;
		
		for( String arg : args ) {
			if( arg.charAt( 0 ) == '-' ) {
				if( filenames.size() != 0 ) {
					System.err.println( "Cannot specify options after filenames: " + arg );
				} else if( arg.equals( "-pt" ) ) {
					printTokens = true;
				} else if( arg.equals( "-pp" ) ) {
					printParseTree = true;
				} else if( arg.equals( "-p" ) ) {
					parseOnly = true;
				} else {
					System.err.println( "Unrecognized option: " + arg );
					System.exit( 31 );
				}
			} else {
				filenames.add( arg );
			}
		}
		
		if( filenames.size() == 0 ) {
			System.err.println( "Please specify the names of the files to parse" );
			System.exit( 31 );
		}
		
		
		// Parse each file
		for( String filename : filenames ) {
			// Read the input file and tokenize
			DFAWrapper dfa = DFAWrapper.createFromDFAFile( DFA_FILE );
			Lexer scanner = LexerFactory.getLexer( dfa );
			List<Token> tokens = scanFile( scanner, filename );
	
			if( printTokens ) {
				System.out.println( tokens );
			}
			
			// Parse the token list
			Parser parser = LR1Parser.create( LR1_FILE );
			for( Token token : tokens ) {
				parser.enqueue( Symbol.wrap( token ) );
			}
			parser.markEnd();
			try {
				parser.run();
			} catch( ParserException pe ) {
				System.err.println( "ParserException in " + filename );
				System.err.println( pe.toString() );
				System.exit( 42 );
			}
			if( !parser.isComplete() ) {
				System.err.println( "Joos file is incomplete: " + filename );
				System.exit( 42 );
			}
			ParseTree tree = parser.getParseTree();
			if( printParseTree ) {
				System.out.println( tree );
			}
		
			// Weed out any errors that the parser does not account for
			Weeder weeder = new Weeder( filename );
			if( !weeder.check( tree ) ) {
				System.err.println( weeder.getError() );
				System.exit( 42 );
			}
			trees.add( tree );
		}
		if( parseOnly ) {
			System.exit( 0 );
		}
		
		// Link the contents together
		Linker linker = new Linker( trees );
		linker.link();
		CodeGenerator codeGen = new CodeGenerator( linker.getEnv() );
		codeGen.genCode();
	}
	
	/*public static void main( String[] args )
	{
		Lex lex = new Lex();
		DFA dfa = lex.build( fileName );
		Lexer lexer = LexerFactory.getLexer( dfa );
	}*/
	
	public static List<Token> scanFile( Lexer scanner, String fileName )
	{
		List<Token> ret = null;
		try {
			FileInputStream in = new FileInputStream( fileName );
			ret = scanner.scan( in );
			in.close();
		} catch( LexerException le ){
			System.err.println( le.getMessage() );
			System.exit( 42 );
		} catch( FileNotFoundException fnfe ) {
			System.err.println( "Invalid file name: " + fileName );
			System.exit( 32 );
		} catch( IOException ioe ) {
			System.exit( 42 );
		} 
		return ret;
	}
}
