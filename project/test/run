#!/bin/bash 
pushd `dirname "${BASH_SOURCE[0]}"`
TEST=`pwd`
ROOT="${TEST}/.."
popd

if [[ "$#" -eq 0 ]]
then
	echo -e "Usage:\n  test/run assignment [testcase]\n\nExamples:\n  test/run 2\n  test/run 1 all\n  test/run 3 TestCaseName"
	exit 1
fi

ASSIGNMENT="${1}"
TESTCASE="${2}"
DIR="${TEST}/a${ASSIGNMENT}"
DST="${DIR}/output"
OUTPUT="${ROOT}/output"
NASM="nasm"
STDLIBDIR="${ROOT}/res/stdlib${ASSIGNMENT}"
STDLIB=`find "${STDLIBDIR}" -name '*.java'`
realResults="${DST}/results.txt"
realFailures="${DST}/failures.txt"
results="${realResults}.tmp"
failures="${realFailures}.tmp"

if [ ! -d "${DIR}" ]
then
	echo "Invalid assignment number: ${ASSIGNMENT}"
	exit 1
fi

if [ ! -d "${DST}" ]
then
	mkdir "${DST}"
fi


if [[ "${TESTCASE}" == "all" || ("${TESTCASE}" == "" && ! -f "${failures}") ]]
then
	FILES="${DIR}/*"
elif [[ "${TESTCASE}" != "" ]]
then
	FILES="${DIR}/${TESTCASE}"
	results="/dev/null"
	failures="/dev/null"
	realResults="/dev/null"
	realFailures="/dev/null"
else
	FILES="`cat "${realFailures}"`"
fi


: > "${results}"
: > "${failures}"



for f in ${FILES}
do
	if [[ -f "${f}" ]]
	then
		targets=${f}
		base=`basename "${f}" '.java'`
	elif [[ -d "${f}" && !( "${f}" == '.' || "${f}" == '..' || "${f}" == "output" ) ]]
	then
		targets=`find "${f}" -name '*.java'`
		base=`basename "${f}"`
	else
		continue
	fi

	if [[ "${ASSIGNMENT}" -eq "5" ]]
	then
		for old in `ls "${OUTPUT}"`
		do
			rm "${OUTPUT}/${old}"
		done
	fi

	dst="${DST}/${base}"
	"${ROOT}/joosc" ${STDLIB} ${targets} > "${dst}.out" 2> "${dst}.err"
	result=$?

	if [[ "${result}" -ne "0" ]]
	then
		if [[ "`expr "${base}" : J1`" -ne  "0" ]]
		then
			echo "${f} => ${result}: `cat "${dst}.err"`" >> "${results}"
			echo "${f}" >> "${failures}"
			echo FAIL
		else
			echo SUCCESS
		fi
	else
		if [[ "`expr "${base}" : Je`" -ne "0" ]]
		then
			echo "${f} => ${result}: `cat "${dst}.err"`" >> "${results}"
			echo "${f}" >> "${failures}"
			echo FAIL
		else
			echo SUCCESS
			if [[ "${ASSIGNMENT}" -eq "5" ]]
			then
				find "${STDLIBDIR}" -name '*.s' | xargs cp -t "${OUTPUT}"
				success="true"
				for asm in `ls "${OUTPUT}/"`
				do
					"${NASM}" -O1 -f elf -g -F dwarf "${OUTPUT}/${asm}" >> "${dst}.out" 2>> "${dst}.err"
					if [[ "$?" -ne "0" ]]
					then
						success="false"
					fi
				done
				if [[ "${success}" == "false" ]]
				then
					echo "(but nasm FAILED)"
				else
					find "${OUTPUT}" -name '*.o' | xargs ld -melf_i386 -o main 2>> "${dst}.err"
					if [[ "$?" -ne "0" ]]
					then
						echo "(but linking FAILED)"
					fi
				fi
			fi
		fi
	fi
done

cat "${results}" > "${realResults}"
cat "${failures}" > "${realFailures}"



