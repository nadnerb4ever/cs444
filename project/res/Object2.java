package java.lang;


public interface Object2
{
	public boolean equals( Object other );
	public String toString();
	public int hashCode();
	protected Object clone();
	public Class getClass();
}

